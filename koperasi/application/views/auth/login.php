<?php
$this->load->view('auth/header');
?>
    <div class="login-box" style="background-color: white;margin:0;height: 100%">
      <div class="login-logo"><img src="<?php echo base_url('assets/img/logo.png');?>" width="200px" height="auto"/>
        <span>KOPERASI</span>
      </div>
      <div class="login-box-body">
        <p class="login-box-msg">
          <div class="error"><?php echo $message;?></div>
        </p>
        <?php echo form_open('auth/login',array("name"=>"login","onSubmit"=>"return validate(this)","autocomplete"=>"off")); ?>
          <div class="form-group has-feedback">
            <?php echo form_input($identity);?>
            <span class="help-block"></span>
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <?php echo form_input($password);?>
            <span class="help-block"></span>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-6">
              <div class="checkbox icheck">
                <label>
                  <input type="checkbox"> Remember Me
                </label>
              </div>
            </div>
            <div class="col-xs-6">
              <div class="checkbox icheck">
                <label>
                  <?php echo anchor('auth/forgot_password','Lost Password'); ?>
                </label>
              </div>
            </div>
          </div>
        <div class="social-auth-links text-center">
          <button type="submit" class="btn btn-success btn-block btn-flat">Login</button>
          <a href="<?php echo site_url('register') ?>" class="btn btn-primary btn-block btn-flat">Register</a>
        </div>
        <?php echo form_close(); ?>
      </div>
    </div>

<?php
$this->load->view('auth/footer');
?>
  