</body>
  <script src="<?php echo base_url('assets/plugins/jquery/dist/jquery.min.js');?>"></script>
  <script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js');?>"></script>
  <script src="<?php echo base_url('assets/plugins/iCheck/icheck.min.js');?>"></script>
  <script type="text/javascript">
    $(function () {
      $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' /* optional */
      });
      /*$('input[type="text"]').change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
      });*/
      $('.form-group').removeClass('has-error');
      $('.help-block').empty();
    });
    function validate(form){
      $('.form-group').removeClass('has-error');
      $('.help-block').empty();
      if (form.identity.value == ""){
        //$('.error').html('Mohon Isikan').addClass('alert alert-danger');
        form.identity.focus();
        $('input[name="identity"]').parent().addClass('has-error'); 
        $('input[name="identity"]').next().text('Wajib Diisi');
        return false;
      }
      if (form.password.value == ""){
        //$('.error').html('Mohon Isikan').addClass('alert alert-danger');
        form.password.focus();
        $('input[name="password"]').parent().addClass('has-error'); 
        $('input[name="password"]').next().text('Wajib Diisi');
        return false;
      }
      return (true);
    }
  </script>
</html>