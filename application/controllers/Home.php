<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	private $reqTokenData = array( 
			"user"=>"admin@admin.com",
			"secret"=>"123456789");
	public function __construct(){
		parent::__construct();
		if (!$this->ion_auth->logged_in()){
			redirect('auth');
		}
		$this->load->model('Dashboard_model', 'dashboard');
		$this->load->library(array('curl','Bcrypt','incrypt','custom_library'));
		$this->user = $this->ion_auth->user()->row();
	}


	function index(){
		$user = $this->user;
		
		
		$data = [
			'user' 		=> $user,
			'judul'		=> 'Dashboard',
			'subjudul'	=> 'Data Aplikasi',
			'parent'	=> $this->config->item('parent_module'),
			'logData' =>array($this->session->userdata('userLogin'),$this->session->userdata('passLogin')),
			'berita' => $this->db->get_where('tbl_berita',array('date(datetime)'=>date('y-m-d')))->result_array(),
			'notifikasi'=>$this->custom_library->loadJadwalSholat(682,date('Y-m-d'))
		];
		 
	 	
		$enCryptData  = $this->incrypt->encrypt($data);
		$data['title'] = "INTEGRAL SMART SCHOOL";  
		$data['saldo'] = $this->custom_library->cekSaldo($user->email);
		$data['sendData'] = urlencode(base64_encode($enCryptData));
		$data['top_user'] = $this->db->select('first_name,last_name')
							->join('users_groups','users_groups.user_id = users.id')
							->where('users_groups.group_id','3')
							->order_by('created_on')->get('users')->result_array();
		$data['modul'] =  $this->config->item('module');
		
		$this->load->view('home',$data);
	}


	function reqToken($return=1){
		$data = array(
        	"user"=>"admin@admin.com",
        	"data"=>base64_encode(json_encode($this->reqTokenData))
        );
        //var_dump($data);die;
		$Data = $this->custom_library->loadDataAPI(
					$this->config->item('server_wallet'),
					'integral',
					'1nt3gr4l', 
					'requestToken',
					$data
					);

		if(isset($Data['error']) && $Data['error']!=""){
			return false;
		}else{
			return $Data; 
		}
         
	}

	

}