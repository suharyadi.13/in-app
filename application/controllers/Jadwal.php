<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Segmen extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model(array('Konsumen_model','User_model','kunjungan_model'));
        $this->load->library(array('form_validation','session'));
		$this->load->library('custom_library');
		if($this->session->userdata('logged_in') != TRUE){
			$data['message'] = "Akses ditolak, silahkan login terlebih dahulu. !!";
			redirect('login');
		}
    }

    function index(){
        $this->list_segmen();
    }

    function list_segmen(){
   
        $data['list_data'] = $this->db->select("* ")
                            ->from("tbl_segmen")
                            ->get()
                            ->result_array();
        $data['title'] = "Data Segmen";
        $data['back_url']=site_url("segmen/");
        $data['add_url']=site_url("segmen/form_segmen");
        $data['edit_url']=site_url("segmen/edit_segmen");
        //$data['detail_url']=site_url("segmen/detail_segmen");
        $data['delete_url']=site_url("segmen/delete_segmen"); 
        $data['field_primary'] = 'id_segmen';
        $data['field'] =array(
            //'id_segmen'=>'ID Segmen',
            'nama_segmen'=>'Nama Segmen', 
        );

        $data['page'] = "display";
        $this->load->view('admin/index', $data);

    }

    function form_segmen(){
      
        $data['title'] = "Tambah Segmen";
        $data['save_url']=site_url("segmen/proses_add_segmen");
        $data['action']="add";
        $data['field_primary'] = 'id_segmen';
        $data['hidden'] = array('id_segmen'=>$this->getNumberID());
        $data['field'] =array(           
        //    'id_segmen'=>'ID Prduct',
            'nama_segmen'=>'Nama Segmen'
        );

        $data['page'] = "form";
        $this->load->view('admin/index', $data);
    }
    function detail_segmen($id){
        
        $this->db->select('*');
        $this->db->where('id_segmen',$id);
        $data['data'] = $this->db->get("tbl_segmen")->row_array();
        
        
        $data['title'] = "Detail Segmen";
        $data['save_url']=site_url("segmen/proses_add_segmen");
        $data['action']="add";
        $data['field_primary'] = 'id_segmen';
        
        $data['field'] =array(            
            'id_segmen'=>'ID Prduct',
            'nama_segmen'=>'Nama Segmen', 
        );

        $data['page'] = "detail";
        $this->load->view('admin/index', $data);
    }


    function proses_add_segmen(){
        //var_dump($this->form_validation->run() );
        $this->_rules();
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata("message",validation_errors());
            //die(validation_errors());
            $this->form_segmen();
        }else{
            $action = $this->input->post("action");
            $data['form'] =array(
                'id_segmen'=>$this->input->post("id_segmen"),
                'nama_segmen'=>$this->input->post("nama_segmen"), 
               
            );
            if($action=="add"){
               // array_merge($data['form'],array( 'id_user'=>$this->session->userdata('id_user')));
                $insert = $this->db->insert('tbl_segmen',$data['form']);
                if($insert ){
                    $this->session->set_flashdata("message","Data Berhasil disimpan");
                    redirect('Segmen/list_segmen');
                }else{
                    $this->form_segmen();
                }
            }else if($action=="edit"){
                $insert = $this->db->replace('tbl_segmen',$data['form']);
                if($insert ){
                    $this->session->set_flashdata("message","Data Berhasil disimpan");
                    redirect('Segmen/list_segmen');
                }else{
                    $this->form_segmen();
                }

            }else{
                redirect("Segmen");
            }
        }
    }
    function edit_segmen($id){
        //$id=$this->input->post("id");
        $this->db->select('*');
        $this->db->where('id_segmen',$id);
        $data['data'] = $this->db->get("tbl_segmen")->row_array();
        $data['title'] = "Edit Segmen";
        $data['save_url']=site_url("segmen/proses_add_segmen");
        $data['action']="edit";
        $data['field_primary'] = 'id_segmen';
        $data['hidden'] = array("id_segmen"=>$data['data']['id_segmen']);
        $data['field'] =array(            
            //'id_segmen'=>'ID Prduct',
            'nama_segmen'=>'Nama Segmen', 
        );

        $data['page'] = "form";
        $this->load->view('admin/index', $data);
    }

    function delete_segmen($id){
         
        $delete = $this->db->delete('tbl_segmen',array('id_segmen'=>$id));
        if($delete ){
            $this->session->set_flashdata("message","Data Berhasil dihapus");
            redirect('Segmen/list_segmen');
        }else{
            $this->session->set_flashdata("message","Data Gagal dihapus");
            $this->list_segmen();
        }
    }
    function getProduk_JSON($id){
        $this->db->select('*');
        $this->db->where('id_segmen',$id);
        $data = $this->db->get("tbl_segmen")->row_array();

        if(count($data) >0){
            $respone = array("message"=>"Load data sukses","data"=>$data,"error"=>"0");
        }else{
            $respone = array("message"=>"Load data gagal","data"=>"","error"=>"1");
        }

        echo json_encode($respone);
    }
    function getNumberID(){
        $lastSegmen = $this->db->select('max(id_segmen) last_id')
                        ->from('tbl_segmen')
                        ->get()->row_array();
        return $lastSegmen['last_id']+1;

    }

    public function _rules() 
    {
 
    $this->form_validation->set_rules('nama_segmen', 'Nama Segmen', 'trim|required'); 
    }





























   

}

/* End of file Segmen.php */
/* Location: ./application/controllers/Segmen.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2016-08-16 13:25:39 */
/* http://harviacode.com */