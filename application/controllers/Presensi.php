<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Presensi extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library(array('form_validation','session'));
		$this->load->library('custom_library');
	    if (!$this->ion_auth->logged_in()) {
            redirect('auth');
        }
        $this->user = $this->ion_auth->user()->row();
    }

    function index(){
        $this->list_presensi();
    }

    function loadMandatoryData($data=""){
        $user = $this->user;
        $data = [
            'user'      => $user,
            'judul'     => 'Presensi',
            'subjudul'  => 'Data Aplikasi',
            'parent'    => $this->config->item('parent_module'),
        ];
        $data['title'] = "INTEGRAL SMART SCHOOL";

        return $data;
    }

    function list_presensi(){
        $data = $this->loadMandatoryData();
        $data['list_data'] = $this->db->select("*,siswa.nama nama_siswa,kelas.nama_kelas kelas")
                            ->from("tbl_presensi")
                            ->join('siswa','siswa.id_siswa = tbl_presensi.id_siswa','left')
                            ->join('kelas','kelas.id_kelas = siswa.kelas_id','left')
                            ->where('date(datetime)','curdate()',false)
                            ->order_by('datetime','DESC')
                            ->get()
                            ->result_array();
        $data['title'] = "Data Presensi";
        //$data['back_url']=site_url("presensi/");
        //$data['add_url']=site_url("presensi/form_presensi");
        $data['edit_url']=site_url("presensi/edit_presensi");
        $data['detail_url']=site_url("presensi/detail_presensi");
        $data['delete_url']=site_url("presensi/delete_presensi"); 
        $data['field_primary'] = 'id_presensi';
        $data['js_file'] = 'presensi.js';
        $data['field'] =array( 
            'id_presensi'=>'ID Presensi',
            'nama_siswa'=>'Nama Siswa',
            'device_id'=>'ID Mesin',
            'rfid'=>'RFID',
            'datetime'=>'tanggal',
            'nama_siswa'=>'Nama Siswa',
            'kelas'=>'Kelas',

        );

        $data['page'] = "display";
        $this->load->view('tpl_form/index',$data);

    }

    function form_presensi(){
        $data = $this->loadMandatoryData();
        $data['title'] = "Tambah Presensi";
        $data['save_url']=site_url("presensi/proses_add_presensi");
        $data['action']="add";
        $data['field_primary'] = 'id_presensi';
        $data['hidden'] = array('id_presensi'=>$this->getNumberID());
        $data['form_grid']="1";
        $data['field'] =array( 
            'judul_presensi'=>'Judul Presensi',
            'isi_presensi'=>array('label'=>'Presensi','type'=>'textarea'),
            'image'=>array('label'=>'Gambar','type'=>'file')
        );

        $data['page'] = "form";
        $this->load->view('tpl_form/index',$data);
    }
    function detail_presensi($id){
        
        $this->db->select('*');
        $this->db->where('id_presensi',$id);
        $data['data'] = $this->db->get("tbl_presensi")->row_array();
        
        
        $data['title'] = "Detail Presensi";
        $data['save_url']=site_url("presensi/proses_add_presensi");
        $data['action']="add";
        $data['field_primary'] = 'id_presensi';
        
        $data['field'] =array( 
            'judul_presensi'=>'Judul Presensi',
            'isi_presensi'=>'Isi Presensi',
            'image'=>'Gambar',
        );

        $data['page'] = "detail";
        $this->load->view('tpl_form/index',$data);
    }


    function proses_add_presensi(){
        //var_dump($this->form_validation->run() );
        $this->_rules();
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata("message",validation_errors());
            //die(validation_errors());
            $this->form_presensi();
        }else{
            $action = $this->input->post("action");
            $data['form'] =array(
                'id_presensi'=>$this->input->post("id_presensi"), 
                'judul_presensi'=>$this->input->post("judul_presensi"),
                'isi_presensi'=>$this->input->post("isi_presensi"),
                'image'=>$this->input->post("image"),
            ); 
            if($action=="add"){
               // array_merge($data['form'],array( 'id_user'=>$this->session->userdata('id_user')));
                $insert = $this->db->insert('tbl_presensi',$data['form']);
                if($insert ){
                    $this->session->set_flashdata("message","Data Berhasil disimpan");
                    redirect('Presensi/list_presensi');
                }else{
                    $this->form_presensi();
                }
            }else if($action=="edit"){
                $insert = $this->db->replace('tbl_presensi',$data['form']);
                if($insert ){
                    $this->session->set_flashdata("message","Data Berhasil disimpan");
                    redirect('Presensi/list_presensi');
                }else{
                    $this->form_presensi();
                }

            }else{
                redirect("Presensi");
            }
        }
    }
    function edit_presensi($id){
        //$id=$this->input->post("id");
        $this->db->select('*');
        $this->db->where('id_presensi',$id);
        $data['data'] = $this->db->get("tbl_presensi")->row_array();
        $data['title'] = "Edit Presensi";
        $data['save_url']=site_url("Presensi/proses_add_presensi");
        $data['action']="edit";
        $data['field_primary'] = 'id_presensi';
        $data['hidden'] = array("id_presensi"=>$data['data']['id_presensi']);
        $data['field'] =array(            
            //'id_presensi'=>'ID Prduct',
            'nama_presensi'=>'Nama Presensi',
            'type'=>'Type',
            'harga_satuan'=>'Harga Satuan',
        );

        $data['page'] = "form";
        $this->load->view('tpl_form/index',$data);
    }

    function delete_presensi($id){
         
        $delete = $this->db->delete('tbl_presensi',array('id_presensi'=>$id));
        if($delete ){
            $this->session->set_flashdata("message","Data Berhasil dihapus");
            redirect('Presensi/list_presensi');
        }else{
            $this->session->set_flashdata("message","Data Gagal dihapus");
            $this->list_presensi();
        }
    }
 
    function getProduk_JSON($id){
        $this->db->select('*');
        $this->db->where('id_presensi',$id);
        $data = $this->db->get("tbl_presensi")->row_array();

        if(count($data) >0){
            $respone = array("message"=>"Load data sukses","data"=>$data,"error"=>"0");
        }else{
            $respone = array("message"=>"Load data gagal","data"=>"","error"=>"1");
        }

        echo json_encode($respone);
    }
    function getNumberID(){
        $lastPresensi = $this->db->select('max(id_presensi) last_id')
                        ->from('tbl_presensi')
                        ->get()->row_array();
        return $lastPresensi['last_id']+1;

    }

    public function _rules() 
    {
    if($this->input->post("action")=="add"){
        $this->form_validation->set_rules('id_presensi', 'ID Presensi', 'trim|required|is_unique[tbl_presensi.id_presensi]');
    }
    $this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
    }
 

}

/* End of file Presensi.php */
/* Location: ./application/controllers/Presensi.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2016-08-16 13:25:39 */
/* http://harviacode.com */