<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Berita extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library(array('form_validation','session'));
		$this->load->library('custom_library');
		if (!$this->ion_auth->logged_in()){
            redirect('auth');
        } 
        $this->user = $this->ion_auth->user()->row();
    }

    function index(){
        $this->list_berita();
    }

    function loadMandatoryData($data=""){
        $user = $this->user;
        $data = [
            'user'      => $user,
            'judul'     => 'Berita',
            'subjudul'  => 'Data Aplikasi',
            'parent'    => $this->config->item('parent_module'),
        ];
        $data['title'] = "INTEGRAL SMART SCHOOL";

        return $data;
    }

    function list_berita(){
        $data = $this->loadMandatoryData();
        $data['list_data'] = $this->db->select("*")
                            ->from("tbl_berita")
                            ->get()
                            ->result_array();
        $data['title'] = "Data Berita";
        $data['back_url']=site_url("berita/");
        $data['add_url']=site_url("Berita/form_berita");
        $data['edit_url']=site_url("Berita/edit_berita");
        $data['detail_url']=site_url("Berita/detail_berita");
        $data['delete_url']=site_url("Berita/delete_berita"); 
        $data['field_primary'] = 'id_berita';
        $data['field'] =array( 
            'judul_berita'=>'Judul Berita',
            'isi_berita'=>'Isi Berita',
            'image'=>'Gambar',
        );

        $data['page'] = "display";
        $this->load->view('tpl_form/index',$data);

    }

    function form_berita(){
        $data = $this->loadMandatoryData();
        $data['title'] = "Tambah Berita";
        $data['save_url']=site_url("Berita/proses_add_berita");
        $data['action']="add";
        $data['field_primary'] = 'id_berita';
        $data['hidden'] = array('id_berita'=>$this->getNumberID());
        $data['form_grid']="1";
        $data['field'] =array( 
            'judul_berita'=>'Judul Berita',
            'NULL'=>'NULL',
            'isi_berita'=>array('label'=>'Berita','type'=>'textarea'),
            'image'=>array('label'=>'Gambar','type'=>'file')
        );

        $data['page'] = "form";
        $this->load->view('tpl_form/index',$data);
    }
    function detail_berita($id){
        $data = $this->loadMandatoryData();
        $this->db->select('*');
        $this->db->where('id_berita',$id);
        $data['data'] = $this->db->get("tbl_berita")->row_array();
        
        
        $data['title'] = "Detail Berita";
        $data['save_url']=site_url("Berita/proses_add_berita");
        $data['action']="add";
        $data['field_primary'] = 'id_berita';
        
         $data['field'] =array( 
            'judul_berita'=>'Judul Berita',
            'isi_berita'=>array('label'=>'Berita','type'=>'textarea'),
            'image'=>'Gambar'
        );

        $data['page'] = "detail";
        $this->load->view('tpl_form/index',$data);
    }


    function proses_add_berita(){
        //var_dump($this->form_validation->run() );
        $this->_rules();
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata("message",validation_errors());
            //die(validation_errors());
            $this->form_berita();
        }else{
            $action = $this->input->post("action");
            $data['form'] =array(
                'id_berita'=>$this->input->post("id_berita"), 
                'judul_berita'=>$this->input->post("judul_berita"),
                'isi_berita'=>$this->input->post("isi_berita"),
                'image'=>$this->input->post("image"),
            ); 
            if($action=="add"){
               // array_merge($data['form'],array( 'id_user'=>$this->session->userdata('id_user')));
                $insert = $this->db->insert('tbl_berita',$data['form']);
                if($insert ){
                    $this->session->set_flashdata("message","Data Berhasil disimpan");
                    redirect('Berita/list_berita');
                }else{
                    $this->form_berita();
                }
            }else if($action=="edit"){
                $insert = $this->db->replace('tbl_berita',$data['form']);
                if($insert ){
                    $this->session->set_flashdata("message","Data Berhasil disimpan");
                    redirect('Berita/list_berita');
                }else{
                    $this->form_berita();
                }

            }else{
                redirect("Berita");
            }
        }
    }
    function edit_berita($id){
        //$id=$this->input->post("id");
        $data = $this->loadMandatoryData();
        $this->db->select('*');
        $this->db->where('id_berita',$id);
        $data['data'] = $this->db->get("tbl_berita")->row_array();
        $data['title'] = "Edit Berita";
        $data['save_url']=site_url("Berita/proses_add_berita");
        $data['action']="edit";
        $data['field_primary'] = 'id_berita';
        $data['hidden'] = array("id_berita"=>$data['data']['id_berita']);
         $data['field'] =array( 
            'judul_berita'=>'Judul Berita',
            'isi_berita'=>array('label'=>'Berita','type'=>'textarea'),
            'image'=>array('label'=>'Gambar','type'=>'file')
        );

        $data['page'] = "form";
        $this->load->view('tpl_form/index',$data);
    }

    function delete_berita($id){
         
        $delete = $this->db->delete('tbl_berita',array('id_berita'=>$id));
        if($delete ){
            $this->session->set_flashdata("message","Data Berhasil dihapus");
            redirect('Berita/list_berita');
        }else{
            $this->session->set_flashdata("message","Data Gagal dihapus");
            $this->list_berita();
        }
    }
    function getProduk_JSON($id){
        $this->db->select('*');
        $this->db->where('id_berita',$id);
        $data = $this->db->get("tbl_berita")->row_array();

        if(count($data) >0){
            $respone = array("message"=>"Load data sukses","data"=>$data,"error"=>"0");
        }else{
            $respone = array("message"=>"Load data gagal","data"=>"","error"=>"1");
        }

        echo json_encode($respone);
    }
    function getNumberID(){
        $lastBerita = $this->db->select('max(id_berita) last_id')
                        ->from('tbl_berita')
                        ->get()->row_array();
        return $lastBerita['last_id']+1;

    }

    public function _rules() 
    {
    if($this->input->post("action")=="add"){
        $this->form_validation->set_rules('id_berita', 'ID Berita', 'trim|required|is_unique[tbl_berita.id_berita]');
    }
    $this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
    }
 

}

/* End of file Berita.php */
/* Location: ./application/controllers/Berita.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2016-08-16 13:25:39 */
/* http://harviacode.com */