<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Payment extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library(array('form_validation','session'));
		$this->load->library('custom_library');
	    if (!$this->ion_auth->logged_in()) {
            redirect('auth');
        }
        $this->user = $this->ion_auth->user()->row();
    }

    function index(){
        $this->list_payment();
    }

    function loadMandatoryData($data=""){
        $user = $this->user;
        $data = [
            'user'      => $user,
            'judul'     => 'Payment',
            'subjudul'  => 'Data Aplikasi',
            'parent'    => $this->config->item('parent_module'),
        ];
        $data['title'] = "INTEGRAL SMART SCHOOL";

        return $data;
    }

    function list_payment(){
        $data = $this->loadMandatoryData();
        $data['list_data'] = $this->db->select("*,siswa.nama nama_siswa,kelas.nama_kelas kelas")
                            ->from("tbl_payment")
                            ->join('siswa','siswa.id_siswa = tbl_payment.id_siswa','left')
                            ->join('kelas','kelas.id_kelas = siswa.kelas_id','left')
                            ->where('date(datetime)','curdate()',false)
                            ->order_by('datetime','DESC')
                            ->get()
                            ->result_array();
        $data['title'] = "Data Payment";
        //$data['back_url']=site_url("payment/");
        $data['add_url']=site_url("payment/form_payment");
        $data['edit_url']=site_url("payment/edit_payment");
        $data['detail_url']=site_url("payment/detail_payment");
        $data['delete_url']=site_url("payment/delete_payment"); 
        $data['field_primary'] = 'id_payment';
        //$data['js_file'] = 'payment.js';
        $data['field'] =array( 
            'id_user'=>'nip',
            'jenis_payment'=>'Jenis Pembayaran',
            'tgl_payment'=>'Tgl Pembayaran',
            'nominal'=>'Nominal',
            'status'=>'Status', 
        );

        $data['page'] = "display";
        $this->load->view('tpl_form/index',$data);

    }

    function form_payment(){
        $data = $this->loadMandatoryData();
        $data['title'] = "Tambah Payment";
        $data['save_url']=site_url("payment/proses_add_payment");
        $data['action']="add";
        $data['field_primary'] = 'id_payment';
        $data['hidden'] = array('id_payment'=>$this->getNumberID());
        $data['form_grid']="1";
        $data['field'] =array( 
            'id_user'=>'nip',
            'jenis_payment'=>'Jenis Pembayaran',
            'nominal'=>'Nominal',
            'tgl_payment'=>array('label'=>'Tgl Pembayaran','type'=>'date'),
            
            //'status'=>'Status', 
        );

        $data['page'] = "form";
        $this->load->view('tpl_form/index',$data);
    }
    function detail_payment($id){
        
        $this->db->select('*');
        $this->db->where('id_payment',$id);
        $data['data'] = $this->db->get("tbl_payment")->row_array();
        
        
        $data['title'] = "Detail Payment";
        $data['save_url']=site_url("payment/proses_add_payment");
        $data['action']="add";
        $data['field_primary'] = 'id_payment';
        
        $data['field'] =array( 
            'judul_payment'=>'Judul Payment',
            'isi_payment'=>'Isi Payment',
            'image'=>'Gambar',
        );

        $data['page'] = "detail";
        $this->load->view('tpl_form/index',$data);
    }


    function proses_add_payment(){
        //var_dump($this->form_validation->run() );
        $this->_rules();
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata("message",validation_errors());
            //die(validation_errors());
            $this->form_payment();
        }else{
            $action = $this->input->post("action");
            $data['form'] =array(
                'id_payment'=>$this->input->post("id_payment"), 
                'judul_payment'=>$this->input->post("judul_payment"),
                'isi_payment'=>$this->input->post("isi_payment"),
                'image'=>$this->input->post("image"),
            ); 
            if($action=="add"){
               // array_merge($data['form'],array( 'id_user'=>$this->session->userdata('id_user')));
                $insert = $this->db->insert('tbl_payment',$data['form']);
                if($insert ){
                    $this->session->set_flashdata("message","Data Berhasil disimpan");
                    redirect('Payment/list_payment');
                }else{
                    $this->form_payment();
                }
            }else if($action=="edit"){
                $insert = $this->db->replace('tbl_payment',$data['form']);
                if($insert ){
                    $this->session->set_flashdata("message","Data Berhasil disimpan");
                    redirect('Payment/list_payment');
                }else{
                    $this->form_payment();
                }

            }else{
                redirect("Payment");
            }
        }
    }
    function edit_payment($id){
        //$id=$this->input->post("id");
        $this->db->select('*');
        $this->db->where('id_payment',$id);
        $data['data'] = $this->db->get("tbl_payment")->row_array();
        $data['title'] = "Edit Payment";
        $data['save_url']=site_url("Payment/proses_add_payment");
        $data['action']="edit";
        $data['field_primary'] = 'id_payment';
        $data['hidden'] = array("id_payment"=>$data['data']['id_payment']);
        $data['field'] =array(            
            //'id_payment'=>'ID Prduct',
            'nama_payment'=>'Nama Payment',
            'type'=>'Type',
            'harga_satuan'=>'Harga Satuan',
        );

        $data['page'] = "form";
        $this->load->view('tpl_form/index',$data);
    }

    function delete_payment($id){
         
        $delete = $this->db->delete('tbl_payment',array('id_payment'=>$id));
        if($delete ){
            $this->session->set_flashdata("message","Data Berhasil dihapus");
            redirect('Payment/list_payment');
        }else{
            $this->session->set_flashdata("message","Data Gagal dihapus");
            $this->list_payment();
        }
    }
 
    function getProduk_JSON($id){
        $this->db->select('*');
        $this->db->where('id_payment',$id);
        $data = $this->db->get("tbl_payment")->row_array();

        if(count($data) >0){
            $respone = array("message"=>"Load data sukses","data"=>$data,"error"=>"0");
        }else{
            $respone = array("message"=>"Load data gagal","data"=>"","error"=>"1");
        }

        echo json_encode($respone);
    }
    function getNumberID(){
        $lastPayment = $this->db->select('max(id_payment) last_id')
                        ->from('tbl_payment')
                        ->get()->row_array();
        return $lastPayment['last_id']+1;

    }

    public function _rules() 
    {
    if($this->input->post("action")=="add"){
        $this->form_validation->set_rules('id_payment', 'ID Payment', 'trim|required|is_unique[tbl_payment.id_payment]');
    }
    $this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
    }
 

}

/* End of file Payment.php */
/* Location: ./application/controllers/Payment.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2016-08-16 13:25:39 */
/* http://harviacode.com */