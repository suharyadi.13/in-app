<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Library extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library(array('form_validation','session'));
        $this->load->library('custom_library');
        if (!$this->ion_auth->logged_in()){
            redirect('auth');
        } 
        $this->user = $this->ion_auth->user()->row();
    }

    function index(){
        if( !$this->ion_auth->in_group('siswa') ){ 
            $this->list_buku();
        }else{
            $this->tampilkan_buku();
        }
    }
    function tampilkan_buku(){
        $data = $this->loadMandatoryData();
        $data['title'] = "Data Buku";
        $data['back_url']=site_url("library/");
        $data['list_data'] = $this->db->select("*")
                            ->from("tbl_buku")
                            ->get()
                            ->result_array();

        $data['page'] = "daftar_buku";
        $this->load->view('tpl_form/index',$data);
    }

    function loadMandatoryData($data=""){
        $user = $this->user;
        $data = [
            'user'      => $user,
            'judul'     => 'Buku',
            'subjudul'  => 'Data Aplikasi',
            'parent'    => $this->config->item('parent_module'),
        ];
        $data['title'] = "INTEGRAL APP";

        return $data;
    }

    function list_buku(){
        $data = $this->loadMandatoryData();
        $data['list_data'] = $this->db->select("*")
                            ->from("tbl_buku")
                            ->get()
                            ->result_array();
        $data['title'] = "Data Buku";
        $data['back_url']=site_url("library/");
        $data['add_url']=site_url("library/form_buku");
        $data['edit_url']=site_url("library/edit_buku");
        $data['detail_url']=site_url("library/detail_buku");
        $data['delete_url']=site_url("library/delete_buku"); 
        $data['field_primary'] = 'id_buku';
        $data['field'] =array( 
            'kategori_buku'=>'Kategori Buku',
            'judul_buku'=>'Judul Buku',
            'isi_buku'=>array('label'=>'Isi Buku','type'=>'textarea'),
            'pengarang'=>'Pengarang',
            'tahun'=>'Tahun Terbit', 
        );

        $data['page'] = "display";
        $this->load->view('tpl_form/index',$data);

    }

    function form_buku(){
        $data = $this->loadMandatoryData();
        $data['title'] = "Tambah Buku";
        $data['save_url']=site_url("library/proses_add_buku");
        $data['action']="add";
        $data['field_primary'] = 'id_buku';
        $data['hidden'] = array('id_buku'=>$this->getNumberID());
        $data['form_grid']="1";
        $data['field'] =array( 
             'kategori_buku'=>'Kategori Buku',
            'judul_buku'=>'Judul Buku',
            'isi_buku'=>array('label'=>'Isi Buku','type'=>'textarea'),
            'pengarang'=>'Pengarang',
            'tahun'=>'Tahun Terbit',
            'gambar'=>array('label'=>'Gambar Sampul','type'=>'file'),
            'file'=>array('label'=>'File Buku','type'=>'file')
        );

        $data['page'] = "form";
        $this->load->view('tpl_form/index',$data);
    }
    function detail_buku($id){
        $data = $this->loadMandatoryData();
        $this->db->select('*');
        $this->db->where('id_buku',$id);
        $data['data'] = $this->db->get("tbl_buku")->row_array();
        
        
        $data['title'] = "Detail Buku";
        $data['save_url']=site_url("library/proses_add_buku");
        $data['action']="add";
        $data['field_primary'] = 'id_buku';
        
         $data['field'] =array( 
            'kategori_buku'=>'Kategori Buku',
            'judul_buku'=>'Judul Buku',
            'isi_buku'=>array('label'=>'Isi Buku','type'=>'textarea'),
            'pengarang'=>'Pengarang',
            'tahun'=>'Tahun Terbit',
            'gambar'=>'Gambar Sampul',
        );

        $data['page'] = "detail";
        $this->load->view('tpl_form/index',$data);
    }


    function proses_add_buku(){
        //var_dump($this->form_validation->run() );
        $this->_rules();
        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata("message",validation_errors());
            //die(validation_errors());
            $this->form_buku();
        }else{
            $action = $this->input->post("action");
            $data['form'] =array(
                'kategori_buku'=>$this->input->post("kategori_buku"), 
                'judul_buku'=>$this->input->post("judul_buku"),
                'isi_buku'=>$this->input->post("isi_buku"),
                'pengarang'=>$this->input->post("pengarang"),
                'tahun'=>$this->input->post("tahun"),
                'gambar'=>$this->input->post("gambar")
            ); 

            if($_FILES['gambar']['name']!=""){
                $fileUpload = $this->uploadFile('gambar');
                if($fileUpload['error']==0){
                    $data['form']['gambar'] = $fileUpload['data']['file_name'];
                }
            }else{
                $fileUpload = array('error'=>'0','data'=>array('file_name'=>''));
                unset($data['form']['gambar']);
            }

            if($_FILES['file']['name']!=""){
                $fileUpload2 = $this->uploadFile('file');
                if($fileUpload2['error']==0){
                    $data['form']['file'] = $fileUpload2['data']['file_name'];
                }
            }else{
                $fileUpload2 = array('error'=>'0','data'=>array('file_name'=>''));
                unset($data['form']['file']);
            }


            if($action=="add"){
                if($fileUpload['error']==1){
                    //var_dump($fileUpload['data']);
                    $this->session->set_flashdata("message",$fileUpload['data']);
                    redirect('Library/form_buku');
                    //die('aa');
                }

               // array_merge($data['form'],array( 'id_user'=>$this->session->userdata('id_user')));
                $insert = $this->db->insert('tbl_buku',$data['form']);
                if($insert ){
                    $this->session->set_flashdata("message","Data Berhasil disimpan");
                    redirect('library/list_buku');
                }else{
                    $this->form_buku();
                }
            }else if($action=="edit"){
                if($fileUpload['error']==1){
                    $this->session->set_flashdata("message",$fileUpload['data']);
                    redirect("library/edit_buku/".$this->input->post("id_scene"));
                }

                $insert = $this->db->replace('tbl_buku',$data['form']);
                if($insert ){
                    $this->session->set_flashdata("message","Data Berhasil disimpan");
                    redirect('library/list_buku');
                }else{
                    $this->form_buku();
                }

            }else{
                redirect("Berita");
            }
        }
    }
    function edit_buku($id){
        //$id=$this->input->post("id");
        $data = $this->loadMandatoryData();
        $this->db->select('*');
        $this->db->where('id_buku',$id);
        $data['data'] = $this->db->get("tbl_buku")->row_array();
        $data['title'] = "Edit Buku";
        $data['save_url']=site_url("library/proses_add_buku");
        $data['action']="edit";
        $data['field_primary'] = 'id_buku';
        $data['hidden'] = array("id_buku"=>$data['data']['id_buku']);
        $data['field'] =array( 
             'kategori_buku'=>'Kategori Buku',
            'judul_buku'=>'Judul Buku',
            'isi_buku'=>array('label'=>'Isi Buku','type'=>'textarea'),
            'pengarang'=>'Pengarang',
            'tahun'=>'Tahun Terbit',
            'gambar'=>array('label'=>'Gambar','type'=>'file')
        );

        $data['page'] = "form";
        $this->load->view('tpl_form/index',$data);
    }

    function delete_buku($id){
         
        $delete = $this->db->delete('tbl_buku',array('id_buku'=>$id));
        if($delete ){
            $this->session->set_flashdata("message","Data Berhasil dihapus");
            redirect('library/list_buku');
        }else{
            $this->session->set_flashdata("message","Data Gagal dihapus");
            $this->list_buku();
        }
    }
    function getProduk_JSON($id){
        $this->db->select('*');
        $this->db->where('id_buku',$id);
        $data = $this->db->get("tbl_buku")->row_array();

        if(count($data) >0){
            $respone = array("message"=>"Load data sukses","data"=>$data,"error"=>"0");
        }else{
            $respone = array("message"=>"Load data gagal","data"=>"","error"=>"1");
        }

        echo json_encode($respone);
    }
    function getNumberID(){
        $lastBerita = $this->db->select('max(id_buku) last_id')
                        ->from('tbl_buku')
                        ->get()->row_array();
        return $lastBerita['last_id']+1;

    }

    public function _rules() 
    {
    if($this->input->post("action")=="add"){
        $this->form_validation->set_rules('id_buku', 'ID Buku', 'trim|required|is_unique[tbl_buku.id_buku]');
    }
    $this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
    } 
    function uploadFile($field_name,$resize=false,$path="./uploads/buku/"){

        $config['upload_path'] = $path;
        $config['allowed_types'] = 'gif|jpg|png|pdf';
        $config['max_size']     = '5000';
        /*$config['max_width'] = '1024';
        $config['max_height'] = '768';
*/
        $this->load->library('upload', $config);

        // Alternately you can set preferences by calling the ``initialize()`` method. Useful if you auto-load the class:
        $this->upload->initialize($config); 

        if($this->upload->do_upload($field_name)){
            $return = array('error'=>'0','data'=>$this->upload->data());
            if($resize==true){
                $this->resizeImage($this->upload->data('file_name'),$config['upload_path']);
            }
        }else{
             $return = array('error'=>'1','data'=>$this->upload->display_errors());
        }

        return $return;
    }
    public function resizeImage($filename,$path)
    {
      $source_path = $path . $filename;
      $target_path = $path .'/thumbnail/';
      $config_manip = array(
          'image_library' => 'gd2',
          'source_image' => $source_path,
          'new_image' => $target_path,
          'maintain_ratio' => TRUE,
          'create_thumb' => TRUE,
          'thumb_marker' => '',
          'width' => 150,
          'height' => 150
      );


      $this->load->library('image_lib', $config_manip);
      if (!$this->image_lib->resize()) {
          echo $this->image_lib->display_errors();
      }


      $this->image_lib->clear();
    }
 

}

/* End of file Buku.php */
/* Location: ./application/controllers/Berita.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2016-08-16 13:25:39 */
/* http://harviacode.com */