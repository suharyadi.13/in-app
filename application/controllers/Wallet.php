<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Wallet extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library(array('form_validation','session'));
		$this->load->library(array('custom_library','Incrypt'));
	    if (!$this->ion_auth->logged_in()) {
            redirect('auth');
        }
        $this->user = $this->ion_auth->user()->row();
    }
    function loadMandatoryData($data=""){
        $user = $this->user;
        $data = [
            'user'      => $user,
            'judul'     => 'Presensi',
            'subjudul'  => 'Data Aplikasi',
            'parent'    => $this->config->item('parent_module'),
        ];
        $data['title'] = "INTEGRAL SMART SCHOOL";

        return $data;
    }


    function index(){
        $user = $this->user;
        $data = [
            'user'      => $user,
            'judul'     => 'Dashboard',
            'subjudul'  => 'Data Aplikasi',
            'parent'    => $this->config->item('parent_module'),
            'logData' =>array($this->session->userdata('userLogin'),$this->session->userdata('passLogin')),
        ];
        $enCryptData  = $this->incrypt->encrypt($data);
        $data['title'] = "INTEGRAL SMART SCHOOL";   
        $data['sendData'] = urlencode(base64_encode($enCryptData)); 
        $data['modul'] =  $this->config->item('module');
        $data['saldo'] = $this->custom_library->cekSaldo($user->email);
        $data['mutasi'] = $this->load_mutasi($user->email);
        //var_dump($data['mutasi']);die;
        $this->load->view('wallet',$data);
    }

    function proses_topup(){
       $this->form_validation->set_rules('nominal', 'Nominal', 'trim|required|is_natural_no_zero',array(
            'is_natural_no_zero'=>"%s hanya boleh diisi dengan angka",
            'required'=>'Kolom %s Tidak boleh kosong')); 

       if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata("message",validation_errors());
            //die(validation_errors());
            $this->index();
        }else{
            $ref_id = md5("topup_saldo".$this->config->item('user_wallet')."".date("y-m-d H:i:s"));
            $dataItem = array(
                "user"=>$this->user->email,
                "nominal"=>$this->input->post('nominal'),
                "ref_id"=>$ref_id,
                "action"=>'topup_saldo'
            );
            //var_dump($dataItem);die;
            $dataPost =array(
                "user"=>$this->config->item('user_wallet'),
                "data"=>$this->incrypt->encrypt($dataItem)
            );

            $Data = $this->custom_library->loadDataAPI(
                        $this->config->item('server_wallet'),
                        'integral',
                        '1nt3gr4l', 
                        'topup',
                        $dataPost
                        );
            //  var_dump($Data );die;
            if(isset($Data['response']) && $Data['response'] ==TRUE){
                $mutasi = $this->incrypt->decrypt($Data['data']);
                $this->session->set_flashdata('message','Pengajuan topup berhasil dilakukan, silahkan transfer sebesar <strong>Rp. '.number_format($this->input->post('nominal')).' anda ke no rek BCA 2831613353 A.n Suharyadi </strong>, jika sudah melakukan transfer silahkan lakukan konfirmasi melalui tombol <a target="_blank" href="'.site_url('wallet/chat/?no='.$this->config->item('PPOB_CS').'&message='.urlencode("mohon diproses untuk topup dengan kode ".$ref_id.", Berikut ini saya lampirkan bukti transfernya")).'" class="btn btn-success  btn-sm"><i class="fa fa-whatsapp"></i></a> dibawah');
                redirect('wallet');
            }else{

                $this->session->set_flashdata('message','Pengajuan topup gagal silahkan coba lagi. ('.(isset($Data['message'])?$Data['message']:"").')');
                $this->index();
            }
            
        }
    }
    function load_mutasi(){
         
            $dataItem = array(
                "user"=>$this->user->email,  
                "action"=>'mutasi'
            );

            $dataPost =array(
                "user"=>$this->config->item('user_wallet'),
                "data"=>$this->incrypt->encrypt($dataItem)
            );

            $Data = $this->custom_library->loadDataAPI(
                        $this->config->item('server_wallet'),
                        'integral',
                        '1nt3gr4l', 
                        'mutasi',
                        $dataPost
                        );
            //var_dump($Data);die;
            if(isset($Data['response']) && $Data['response'] ==TRUE){
                $mutasi = $this->incrypt->decrypt($Data['data']);
                return $mutasi['data'];
            }else{

                return false;
            }
             
    }

    function chat(){
        $phone = str_replace("+","", $this->input->get('no'));
        $message = $this->input->get('message');
        // DO NOT EDIT BELOW
        $message = urlencode($message);
        $message = str_replace('+','%20',$message);
        $iphone = strpos($_SERVER['HTTP_USER_AGENT'],"iPhone");
        $android = strpos($_SERVER['HTTP_USER_AGENT'],"Android");
        $palmpre = strpos($_SERVER['HTTP_USER_AGENT'],"webOS");
        $berry = strpos($_SERVER['HTTP_USER_AGENT'],"BlackBerry");
        $ipod = strpos($_SERVER['HTTP_USER_AGENT'],"iPod");
        // check if is a mobile
        if ($iphone || $android || $palmpre || $ipod || $berry == true)
        {
        header('Location: whatsapp://send?phone='.$phone.'&text='.$message);
        //OR
        echo "<script>window.location='whatsapp://send?phone='.$phone.'&text='.$message</script>";
        }
        // all others
        else {
        header('Location: https://web.whatsapp.com/send?phone='.$phone.'&text='.$message);
        //OR
        echo "<script>window.location='https://web.whatsapp.com/send?phone='.$phone.'&text='.$message</script>";
        }
    }

         

}

/* End of file Wallet.php */
/* Location: ./application/controllers/Wallet.php */
/* Please DO NOT modify this information : */ 