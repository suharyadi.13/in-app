<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('form_validation');
        $this->load->helper(['url', 'language','form']);
        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
        $this->lang->load('auth');
    }

    public function index()
    {
        $data['title']="REGISTER ".$this->config->item('company_name');
        $data['desc']="pastikan semua data valid, dan email anda masih aktif";
        $this->form_validation->set_rules('first_name', 'First name','trim|required');
        $this->form_validation->set_rules('last_name', 'Last name','trim|required');
        $this->form_validation->set_rules('ktp','Kartu Pengenal','trim|required|is_unique[users.username]',array(
                'required'      => 'Kolom %s Tidak Boleh Kosong.',
                'is_unique'     => '%s telah digunakan sebelumnya'
        ));
        $this->form_validation->set_rules('email','Email','trim|valid_email|required|is_unique[users.email]',array(
                'required'      => 'Kolom %s Tidak Boleh Kosong.',
                'is_unique'     => '%s telah digunakan sebelumnya'
        ));
        $this->form_validation->set_rules('no_telp','No Telp','trim|is_natural|required|is_unique[siswa.no_telp]',array(
                'required'      => 'Kolom %s Tidak Boleh Kosong.',
                'is_unique'     => '%s telah digunakan sebelumnya'
        ));
        $this->form_validation->set_rules('password','Password','trim|min_length[8]|max_length[20]|required');
        $this->form_validation->set_rules('confirm_password','Confirm password','trim|matches[password]|required');

        if($this->form_validation->run()===FALSE)
        {
            $this->load->view('_templates/auth/_header.php');
            $this->load->view('auth/register', $data);
            $this->load->view('_templates/auth/_footer.php'); 
        }
        else
        {
            $first_name = $this->input->post('first_name');
            $last_name = $this->input->post('last_name');
            $username = $this->input->post('ktp');
            $ktp = $this->input->post('ktp');
            $kd_angg = $this->custom_library->getKodeAnggota();
            $email = $this->input->post('email');
            $no_telp = $this->input->post('no_telp');
            $password = $this->input->post('password');
            $kode_sekolah = $this->input->post('kode_sekolah');

            $additional_data = array(
                'first_name' => $first_name,
                'last_name' => $last_name,
                'kode_sekolah'=>$kode_sekolah,
                'active'=>'1',
            );

            $last_id = $this->ion_auth->register($username,$password,$email,$additional_data);
            if($last_id)
            {
                //$this->session->set_flashdata('message','Pendaftaran berhasil, silahkan cek email anda untuk aktivasi');
                $this->session->set_flashdata('message','Pendaftaran berhasil, silahkan login');
               // $last_id = $this->db->insert_id();
                $this->db->insert('siswa',array(
                    'id_siswa'=>$last_id,
                    'nim'=>$ktp,
                    'nama'=>$first_name, 
                    'email'=>$email,
                    'no_telp'=>$no_telp,
                    'kelas_id'=>'1'
                ));

                 $this->db->insert('tb_anggota',array(
                    'kd_angg'=>$kd_angg, 
                    'nm_angg'=>$first_name, 
                    'sejak'=>date("d-m-Y"),
                    'ktp'=>$ktp, 
                    'telp'=>$no_telp, 
                ));
                 
                //$this->session->mark_as_flash('message','Pendaftaran berhasil, silahkan cek email anda untuk aktivasi');
                redirect('auth');
            }
            else
            {
                $this->session->set_flashdata('message',$this->ion_auth->errors());
                $this->session->mark_as_flash('auth_message');
                redirect('register');
            }
        }
    }
}