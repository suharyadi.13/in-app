<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Materi_model extends CI_Model {
    
    public function getDataMateri($id, $guru)
    {
        $this->datatables->select('a.id_materi, 
            if (LENGTH(a.materi) > 100, concat(SUBSTR(a.materi, 1, 100),\'...\') ,a.materi) materi, 
            FROM_UNIXTIME(a.created_on) as created_on, FROM_UNIXTIME(a.updated_on) as updated_on, b.nama_matkul, c.nama_guru');
        $this->datatables->from('tb_materi a');
        $this->datatables->join('matkul b', 'b.id_matkul=a.matkul_id');
        $this->datatables->join('guru c', 'c.id_guru=a.guru_id');
        if ($id!==null && $guru===null) {
            $this->datatables->where('a.matkul_id', $id);            
        }else if($id!==null && $guru!==null){
            $this->datatables->where('a.guru_id', $guru);
        }
        return $this->datatables->generate();
    }

    public function getMateriById($id)
    {
        return $this->db->get_where('tb_materi', ['id_materi' => $id])->row();
    }

    public function getMatkulGuru($nip)
    {
        $this->db->select('matkul_id, nama_matkul, id_guru, nama_guru');
        $this->db->join('matkul', 'matkul_id=id_matkul');
        $this->db->from('guru')->where('nip', $nip);
        return $this->db->get()->row();
    }

    public function getAllGuru()
    {
        $this->db->select('*');
        $this->db->from('guru a');
        $this->db->join('matkul b', 'a.matkul_id=b.id_matkul');
        return $this->db->get()->result();
    }
}