<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Format class
 * Help convert between various formats such as XML, JSON, CSV, etc.
 *
 * @author    Phil Sturgeon, Chris Kacerguis, @softwarespot
 * @license   http://www.dbad-license.org/
 */
class Custom_library {
	protected $_ci;    
	public $NamaBulan = array("Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
	public $ShortNamaBulan = array("Jan","Feb","Mar","Apr","Mei","Jun","Jul","Agus","Sept","Okt","Nov","Des");
	function __construct($url = '')
	{
		$this->_ci = & get_instance();
	}
		
function cekCombo($val1,$val2){
	if($val1 == $val2)
		return "selected='selected' ";
	else
		return "";
}

function getNamaBulan($bln){
	if($bln >= 1 && $bln <= 12 ){
		return $this->NamaBulan[$bln-1];
	}else{
		return $bln;
	}


}

function loadJadwalSholat($idkota,$tgl){

	$curl = curl_init();

	curl_setopt_array($curl, array(
	  CURLOPT_URL => "https://api.banghasan.com/sholat/format/json/jadwal/kota/".$idkota."/tanggal/".$tgl,
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 0,
	  CURLOPT_FOLLOWLOCATION => true,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "GET",
	  CURLOPT_HTTPHEADER => array(
	    "Cookie: __cfduid=dc26d3b5d4df2b39baca6b44b8dcf731c1598845162"
	  ),
	));

	$response = curl_exec($curl);

	curl_close($curl);
	return (array)json_decode($response);
}

public function loadDataAPI($API_SERVER,$API_USER,$API_PASS,$function,$Data){

$curl = curl_init();
$module = $this->_ci->config->item('module');
 
curl_setopt_array($curl, array(
	CURLOPT_URL => $API_SERVER."/".$function,
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 0,
	CURLOPT_FOLLOWLOCATION => true,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "POST",
	CURLOPT_POSTFIELDS =>json_encode($Data),
	CURLOPT_HTTPHEADER => array(
	"Content-Type: application/json",
	"Authorization: Digest username=\"".$API_USER."\",password=\"".$API_PASS."\", realm=\"Integral-API\", nonce=\"121\", uri=\"/index.php/Wallet/ceksaldo\", algorithm=\"MD5\", qop=auth, nc=00000001, cnonce=\"A558jpRd\", response=\"7554b3b9965a4c35609dc56144f2b817\", opaque=\"15441c938348c30e0fe28369c669543d\""
	),
	));

	$response = curl_exec($curl);
	curl_close($curl); 
	return (array)json_decode($response);

}


function cekSaldo($user){
		 
	$dataItem = array(
		"user"=>$user,
		"action"=>'cek_saldo'
	);

	$dataPost =array(
		"user"=>$this->_ci->config->item('user_wallet'),
		"data"=>$this->_ci->incrypt->encrypt($dataItem)
	);

	$Data = $this->loadDataAPI(
				$this->_ci->config->item('server_wallet'),
				'integral',
				'1nt3gr4l', 
				'ceksaldo',
				$dataPost
				);
	
	//var_dump($Data);die;
	if(isset($Data['response']) && $Data['response'] ==true){
		$saldo = $this->_ci->incrypt->decrypt($Data['data']);
		return $saldo['saldo'];
	}else{
		return 'load saldo gagal';
	}
	
}

function getKodeAnggota(){
	$this->_ci->load->model(array('members_m'));
	$rowID=$this->_ci->members_m->generate_code()->row();
	$member_code=$rowID->kd_angg;
	if ($member_code){
		$setcode=substr($member_code,2);
		$code=(int)$setcode;
	    $code=$code+1;
	    $generate='AG'.str_pad($code, 3,'0',STR_PAD_LEFT);
	}else{
		$generate='AG001';
	}

	return $generate;
}

function getKodeSimpanan(){
	$this->_ci->load->model(array('deposit_m'));
	$rowID=$this->_ci->deposit_m->generate_code()->row();
	$dep_code=$rowID->kd_simp;
	if ($dep_code){
		$setcode=substr($dep_code,2);
		$code=(int)$setcode;
	    $code=$code+1;
	    $generate='SP'.str_pad($code, 4,'0',STR_PAD_LEFT);
	}else{
		$generate='SP0001';
	}

	return $generate;
}
function getKodeDepositMember(){
	//Generate Code Deposit
	$this->_ci->load->model(array('deposit_m'));
	$rowID=$this->_ci->deposit_m->generate_code()->row();
	$dep_code=$rowID->kd_simp;
	if ($dep_code){
		$setcode=substr($dep_code,2);
		$code=(int)$setcode;
	    $code=$code+1;
	    $this->data['generate']='SP'.str_pad($code, 4,'0',STR_PAD_LEFT);
	}else{
		$this->data['generate']='SP0001';
	}
}
function getDataSekolah($kode=""){
	return $this->_ci->db->get_where('tbl_sekolah',array('kode_sekolah'=>$this->_ci->config->item('KODE_SEKOLAH')))->row_array();
}
}