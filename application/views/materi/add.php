<div class="row">
    <div class="col-sm-12">    
        <?=form_open_multipart('materi/save', array('id'=>'formmateri'), array('method'=>'add'));?>
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"><?=$subjudul?></h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2">
                        <div class="form-group col-sm-12">
                            <label>Guru (Mata Kuliah)</label>
                            <?php if( $this->ion_auth->is_admin() ) : ?>
                            <select name="guru_id" required="required" id="guru_id" class="select2 form-group" style="width:100% !important">
                                <option value="" disabled selected>Pilih Guru</option>
                                <?php foreach ($guru as $d) : ?>
                                    <option value="<?=$d->id_guru?>:<?=$d->matkul_id?>"><?=$d->nama_guru?> (<?=$d->nama_matkul?>)</option>
                                <?php endforeach; ?>
                            </select>
                            <small class="help-block" style="color: #dc3545"><?=form_error('guru_id')?></small>
                            <?php else : ?>
                            <input type="hidden" name="guru_id" value="<?=$guru->id_guru;?>">
                            <input type="hidden" name="matkul_id" value="<?=$guru->matkul_id;?>">
                            <input type="text" readonly="readonly" class="form-control" value="<?=$guru->nama_guru; ?> (<?=$guru->nama_matkul; ?>)">
                            <?php endif; ?>
                        </div>
                        
                        <div class="col-sm-12">
                            <label for="materi" class="control-label">Materi</label>
                            <div class="form-group">
                                <input type="file" name="file_materi" class="form-control">
                                <small class="help-block" style="color: #dc3545"><?=form_error('file_materi')?></small>
                            </div>
                            <div class="form-group">
                                <textarea name="materi" id="materi" class="form-control froala-editor" ><?=set_value('materi')?></textarea>
                                <small class="help-block" style="color: #dc3545"><?=form_error('materi')?></small>
                            </div>
                        </div>
                        
                          
                        <div class="form-group pull-right">
                            <a href="<?=base_url('materi')?>" class="btn btn-flat btn-default"><i class="fa fa-arrow-left"></i> Batal</a>
                            <button type="submit" id="submit" class="btn btn-flat bg-purple"><i class="fa fa-save"></i> Simpan</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?=form_close();?>
    </div>
</div>