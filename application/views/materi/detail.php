<div class="box">
    <div class="box-header with-header">
        <h3 class="box-title">Detail Materi</h3>
        <div class="pull-right">
            <a href="<?=base_url()?>materi" class="btn btn-xs btn-flat btn-default">
                <i class="fa fa-arrow-left"></i> Kembali
            </a>
            <a href="<?=base_url()?>materi/edit/<?=$this->uri->segment(3)?>" class="btn btn-xs btn-flat btn-warning">
                <i class="fa fa-edit"></i> Edit
            </a>
        </div>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <h3 class="text-center">Materi</h3>
                <?php if(!empty($materi->file)): ?>
                    <div class="w-50">
                        <?= tampil_media('uploads/bank_materi/'.$materi->file); ?>
                    </div>
                <?php endif; ?>
                <?=$materi->materi?>
                <hr class="my-4"> 
                <strong>Dibuat pada :</strong> <?=strftime("%A, %d %B %Y", date($materi->created_on))?>
                <br>
                <strong>Terkahir diupdate :</strong> <?=strftime("%A, %d %B %Y", date($materi->updated_on))?>
            </div>
        </div>
    </div>
</div>