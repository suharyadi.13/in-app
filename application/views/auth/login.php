<div class="login-box pt-md-2 mh-100" >
	<div class="row">
		<div class="col-md-12">
			<!-- /.login-logo -->
			<div class="login-box-body">
			<h3 class="text-center mt-0 mb-4">
				<img src="<?php echo base_url() ?>/assets/dist/img/logo.png" style="width:200px;height:auto">
			</h3> 
			<p class="login-box-msg" style="font-size: 25px"><?php echo $this->config->item('company_name')."<br> "; //$this->sekolah['nama_sekolah'] ?></p>

			<div id="infoMessage" class="text-center"><?php echo $message;?></div>

			<?= form_open("auth/cek_login", array('id'=>'login'));?>
				<div class="input-group mb-3">
				  <?= form_input($identity,"","class='form-control'");?>
				  <div class="input-group-append">
				    <span class="input-group-text"><i class="fa fa-envelope "> </i></span>
				  </div>
				</div>
				<div class="input-group mb-3">
				  <?= form_input($password,"","class='form-control'");?>
				  <div class="input-group-append">
				    <span class="input-group-text"><i class="fa fa-key "> </i></span>
				  </div>
				</div>
				<div class="row">
					<div class="col-6">
					<div class="checkbox icheck">
						<label>
						<?= form_checkbox('remember', '', FALSE, 'id="remember"');?> Remember Me
						</label>
					</div>
					</div> 
					<div class="col-6">
						<!-- /.col -->
						<div class="col-xs-4 float-right">
						<?= form_submit('submit', lang('login_submit_btn'), array('id'=>'submit','class'=>'btn btn-primary btn-block btn-flat'));?>
						</div>
						<!-- /.col -->
					</div>
				</div>
				<?= form_close(); ?>

				<a href="<?=base_url()?>auth/forgot_password" class="text-center"><?= lang('login_forgot_password');?></a>
				<a class="float-right" href="<?php echo site_url('register'); ?>">Register</a>
				<!-- <a href="<?php echo site_url('facebook_login'); ?>">Login with Facebook</a> -->

			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	let base_url = '<?=base_url();?>';
$(document).ready(function(){
	var usr =  localStorage.getItem("usr");
	var ps =  localStorage.getItem("ps");
	var result="";

	if(usr!="" && usr!=null && ps !="" && ps !=null ){
		$.getJSON('<?php echo site_url('auth/cek_login_outside/?q1=') ?>'+encodeURI(btoa(usr))+'&q2='+encodeURI(btoa(ps)), 
				function(result){
					 document.location.href  = go;
				});
	}
	//alert(usr);
})	
</script>
<script src="<?=base_url()?>assets/dist/js/app/auth/login.js"></script>