<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $this->config->item('company_name'); ?> - Login </title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="<?=base_url()?>assets/bower_components/jquery/jquery-3.3.1.min.js"></script>
</head>
<body>
<div class="login-box pt-md-2 mh-100" >
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">Waiting</div>
				<div class="card-body">
				<h3> logging Out ... </h3>
				<img src="<?php echo base_url('assets/dist/img/loading2.gif') ?>" class="img img-responsive">
				</div>
			</div>
		</div>
	</div>
</div>
</body>
<script src="<?=base_url()?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		//alert('asd');
		localStorage.removeItem('usr');
		localStorage.removeItem('ps');
		document.location  = '<?php echo site_url() ?>';
	})
</script>
</html>