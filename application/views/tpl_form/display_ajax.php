<div class="row">
    <div class="col-lg-12">
        <div class="card card-light">
            <div class="card-header">
                <?php
                if($this->session->flashdata("message")!=""){
                    /*echo '<div class="alert alert-success fade in">
                  <button data-dismiss="alert" class="close close-sm" type="button">
                                      <i class="icon-remove"></i>
                                  </button>'.$this->session->flashdata("message").'</div>
                   ';*/
                }
                ?>
                <?php
                if(isset($add_url)){
                ?>
                <a href="<?php echo $add_url ?>" class="btn btn-lg btn-primary pull-right"> Tambah Data</a>
                <?php
                }
                ?> 
                <?php
                if(isset($back_url)){
                ?>
                <a href="<?php echo $back_url ?>" title="kembali" class="btn btn-lg btn-warning pull-right"> << </a>
                <?php
                }
                ?> 
                <h2><a href="#"><?php echo $title; ?></a></h2>  
            </div>
            <div class="card-body" > 
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body " style="overflow: auto;">
                                <table class="display" style="width:100%" <?php echo isset($ajaxTableURL)?'id="ajaxTable" data="'.$ajaxTableURL.'"':'id="mytable"' ?>>
                                    <thead>
                                        <tr>
                                             
                                        <?php
                                         echo '<td></td>';
                                        foreach($field as $key=>$item){ 
                                            if(is_array($item)){  
                                                echo '<th>'.$item['label'].'</th>';
                                            }else{
                                                echo '<th>'.$item.'</th>';
                                            }
                                        }
                                       
                                        ?>
                                        </tr>
                                    </thead> 
                                    <tbody>
                                    <?php
                                    $x=1;
                                    if(is_array($list_data)){
                                        foreach($list_data as $index=>$value){
                                            echo '<tr> ';
                                            echo '<td>';

                                            if(isset($edit_url)){
                                            echo '
                                                <a class="btn btn-warning btn-sm btn-block" href="'.$edit_url.'/'.$value[$field_primary].'">Edit</a>';
                                            }
                                            if(isset($detail_url)){
                                            echo '    <a class="btn btn-primary btn-sm btn-block" href="'.$detail_url.'/'.$value[$field_primary].'" target="_blank">Detail</a>';
                                            }
                                            if(Isset($delete_url)){
                                            echo '     <a class="btn btn-danger btn-sm btn-block" href="'.$delete_url.'/'.$value[$field_primary].'" onClick="return confirm(\'Apakah anda yakin akan menghapus data? \')">Delete</a>';
                                            }
                                            echo '</td>';
                                            
                                            foreach($field as $key=>$item){
                                                 if(is_array($item)){
                                                    if($item['type']=="number"){
                                                      $value[$key] = number_format($value[$key]);
                                                    }
                                                }
                                                echo '<td>'.$value[$key].'</td>';
                                            }
                                            
                                            echo '</tr>';
                                            $x++;
                                        }
                                    }
                                    ?>
                                    <tfoot>
                                        <tr>
                                            <td></td>
                                        <?php
                                         echo ' ';
                                        foreach($field as $key=>$item){ 
                                            if(is_array($item)){  
                                                echo '<th>'.$item['label'].'</th>';
                                            }else{
                                                echo '<td>'.$item.'</td>';
                                            }
                                        }
                                       
                                        ?>
                                        </tr>
                                    </tfoot> 
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>
                    
