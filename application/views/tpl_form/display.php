   <div class="row">
        <div class="col-12">
        <div class="box">
           <div class="box-header with-border">
        <?php
        if($this->session->flashdata("message")!=""){
            /*echo '<div class="alert alert-success fade in">
          <button data-dismiss="alert" class="close close-sm" type="button">
                              <i class="icon-remove"></i>
                          </button>'.$this->session->flashdata("message").'</div>
           ';*/
        }
        ?>
        <?php
        if(isset($add_url)){
        ?>
        <a href="<?php echo $add_url ?>" class="btn btn-sm btn-primary float-right"> Tambah Data</a>
        <?php
        }
        ?> 
        <?php
        if(isset($back_url)){
        ?>
        <a href="<?php echo $back_url ?>" title="kembali" class="btn btn-sm btn-warning pull-right"> << </a>
        <?php
        }
        ?> 
    </div> 
<div class="post-body"> 
            <div class="box-header  py-3"> 
                 <h6 class="m-0 font-weight-bold text-primary"><?php echo $title; ?></h6>  
            </div>
            <div class="box-body" > 
                <div class="table-responsive">
                <div class="row">
                    <div class="col-md-12">
                                <table  class="table  table-bordered table-striped" style="width:100%" <?php echo isset($ajaxTableURL)?'id="ajaxTable" data="'.$ajaxTableURL.'"':'id="mytable"' ?>>
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                        <?php
                                         echo '<td></td>';
                                        foreach($field as $key=>$item){ 
                                            if(is_array($item)){  
                                                echo '<th>'.$item['label'].'</th>';
                                            }else{
                                                echo '<th>'.$item.'</th>';
                                            }
                                        }
                                       
                                        ?>
                                        </tr>
                                    </thead> 
                                    <tbody>
                                    <?php
                                    $x=1;
                                    if(is_array($list_data)){
                                        foreach($list_data as $index=>$value){
                                            echo '<tr><td>'.$x.'</td>';
                                            echo '<td>';

                                            if(isset($edit_url)){
                                            echo '
                                                <a class="btn btn-warning btn-sm " href="'.$edit_url.'/'.$value[$field_primary].'">Edit</a>';
                                            }
                                            if(isset($detail_url)){
                                            echo '    <a class="btn btn-primary btn-sm" href="'.$detail_url.'/'.$value[$field_primary].'" target="_blank">Detail</a>';
                                            }
                                            if(Isset($delete_url)){
                                            echo '     <a class="btn btn-danger btn-sm" href="'.$delete_url.'/'.$value[$field_primary].'" onClick="return confirm(\'Apakah anda yakin akan menghapus data? \')">Delete</a>';
                                            }
                                            echo '</td>';
                                            
                                            foreach($field as $key=>$item){
                                                 if(is_array($item)){
                                                    if($item['type']=="number"){
                                                      $value[$key] = number_format($value[$key]);
                                                    }
                                                }
                                                echo '<td>'.$value[$key].'</td>';
                                            }
                                            
                                            echo '</tr>';
                                            $x++;
                                        }
                                    }
                                    ?>
                                    <tfoot>
                                        <tr>
                                            <td></td>
                                        <?php
                                         echo '<td></td>';
                                        foreach($field as $key=>$item){ 
                                            if(is_array($item)){  
                                                echo '<th>'.$item['label'].'</th>';
                                            }else{
                                                echo '<th>'.$item.'</th>';
                                            }
                                        }
                                       
                                        ?>
                                        </tr>
                                    </tfoot> 
                                    </tbody>
                                </table>
                             
                    </div>
                </div>
                </div>
            </div>
        </div>
        
    </div>
</div>
</div>  
