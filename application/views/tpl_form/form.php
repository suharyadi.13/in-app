<style type="text/css">
    .control-label{
        font-weight:bold; 
    }
    .card-header p{
        color: #ffffff !important;
    }
    input[type=checkbox]
    {
      /* Double-sized Checkboxes */
      -ms-transform: scale(2); /* IE */
      -moz-transform: scale(2); /* FF */
      -webkit-transform: scale(2); /* Safari and Chrome */
      -o-transform: scale(2); /* Opera */
      transform: scale(2);
      padding: 10px;
      margin: 10px;
    }

</style> 
        <div class="row table-responsive no-padding">
          <div class="col-lg-12">
           <div class="box "> 
            <div class="box-header with-border  py-3 <?php echo (isset($bgHeader)?$bgHeader:'') ?>"> 
                <h3 class="box-title"><?php echo $title; ?></h3>  
                <?php  
                    
                    if(strlen($this->session->flashdata('message'))>1){
                        echo $this->session->flashdata('message');
                    }else if(strlen($this->session->userdata('message'))>1){
                        echo $this->session->userdata('message');
                        //$this->session->userdata('message',"");
                    }
                    echo (isset($dditionaInfo)?$dditionaInfo:'');
                    
                ?> 
                 
            </div>
            <div class="box-body  ">
                <!-- <h6 class="text-right">( Semua Kolom Wajib Diisi )</h6> -->
                <?php
                if(isset($hidden)){ 
                   echo form_open_multipart($save_url , '', $hidden); 
                }else{
                   echo form_open_multipart($save_url , ''); 
                }
                if(!isset($grid)){
                    $grid = 2; 
                }
                ?>
                    <input type="hidden" name="action" value="<?php echo $action ?>">
                    <input type="hidden" name="field_primary" value="<?php echo $field_primary ?>">
                	<div><?php //echo (validation_errors()); ?></div>
                    <div class="form-body">
                    <div class="row p-t-20">
                    <?php
                    $x=0;
                    foreach($field as $key=>$item){

                    if(isset($data["$key"])){
                        $valueItem = $data["$key"];
                    }else{
                        if($action=="add" && (isset($reset) && $reset==true)){
                            $valueItem="";
                        }else{
                            if(isset($item['value'])){
                                $valueItem = $item['value'];
                            }else{
                                $valueItem=set_value($key);
                            }
                        }
                    }
                    if(is_array($item)){
                        if($item['type']=="combo"){
                            $itemInput = '<label class="control-label">'.$item['label'].'</label><br>
                                            <select name="'.$key.'" class="form-control select2" id="'.$key.'" >';
                                            foreach ($item['data'] as $indexItem => $Itemvalue) {
                                                if($valueItem ==$Itemvalue['value'] ){
                                                    $selected = "selected";
                                                }else{
                                                    $selected = "";
                                                }
                                                $itemInput .= '<option '.$selected.' value="'.$Itemvalue['value'].'">'.$Itemvalue['label'].'</option>';
                                            }
                            $itemInput .= '</select>';
                        } elseif($item['type']=="combocustom"){
                            $itemInput = '<label class="control-label">'.$item['label'].'</label>
                                            <select name="'.$key.'" class="form-control select2custom" id="'.$key.'" >';
                                            foreach ($item['data'] as $indexItem => $Itemvalue) {
                                                if($valueItem ==$Itemvalue['value'] ){
                                                    $selected = "selected";
                                                }else{
                                                    $selected = "";
                                                }
                                                $itemInput .= '<option '.$selected.' value="'.$Itemvalue['value'].'">'.$Itemvalue['label'].'</option>';
                                            }
                            $itemInput .= '</select>';
                        }elseif($item['type']=="combomulti"){
                            $itemInput = '<label class="control-label">'.$item['label'].'</label>
                                            <select name="'.$key.'[]" class="form-control select2custom" id="'.$key.'" multiple="true">';
                                            if(!is_array($valueItem)){
                                                $dataVal = explode(',',$valueItem);
                                            }else{
                                                $dataVal =  $valueItem;
                                            }
                                            foreach ($item['data'] as $indexItem => $Itemvalue) {
                                                if(in_array($Itemvalue['value'], $dataVal)  ){
                                                    $selected = "selected";
                                                }else{
                                                    $selected = "";
                                                }
                                                $itemInput .= '<option '.$selected.' value="'.$Itemvalue['value'].'">'.$Itemvalue['label'].'</option>';
                                            }
                            $itemInput .= '</select>';
                        }else if($item['type']=="date"){
                            $itemInput = '
                                <label class="control-label">'.$item['label'].'</label>
                                <div class="form-group"> 
                                    <input class="form-control date" readonly="true" id="'.$key.'" name="'.$key.'" class="form-control" placeholder="Masukan '.$item['label'].'"
                                    value ="'.$valueItem.'"/>
                                  </div> ';
                        }else if($item['type']=="month"){
                            $itemInput = '<label class="control-label">'.$item['label'].'</label>
                                    <input type="month" id="'.$key.'"  name="'.$key.'"    id="'.$key.'" name="'.$key.'" class="form-control" placeholder="Masukan '.$item['label'].'"
                                    value ="'.$valueItem.'">';
                        }else if($item['type']=="file"){
                            $itemInput = '<label class="control-label">'.$item['label'].'</label>
                                    <input type="file" id="'.$key.'" name="'.$key.'" class="form-control" placeholder="Masukan '.$item['label'].'"
                                    value ="'.$valueItem.'">';
                        }else if($item['type']=="textarea"){
                            $itemInput = '<label class="control-label">'.$item['label'].'</label>
                                    <textarea   id="'.$key.'" name="'.$key.'" class="form-control" placeholder="Masukan '.$item['label'].'" >'.$valueItem.'</textarea>';
                        }else if($item['type']=="check"){
                            $itemInput = ' 
                                   <span>'.$item['label'].'</span>
                                   <input type="checkbox" value="1" name="'.$key.'"   >';
                        }else if($item['type']=="label"){
                            $itemInput = '  
                                   <span>'.$item['label'].' : </span>
                                   <span class="strong"><b>'.$item['value'].'</b></span>
                                   <input type="hidden" value="'.$item['value'].'" name="'.$key.'"> ';
                        }else if($item['type']=="readonly"){
                            $itemInput = '  
                                   <span>'.$item['label'].' : </span>
                                   <input type="text" id="'.$key.'"  name="'.$key.'"    id="'.$key.'" name="'.$key.'" class="form-control" placeholder="Masukan '.$item['label'].'"
                                    value ="'.$valueItem.'" readonly="true">';
                        }else if($item['type']=="selectAjax"){
                            $itemInput = '<label class="control-label">'.$item['label'].'</label>
                                            <select name="'.$key.'" class="form-control select2Ajax" id="'.$key.'" data="'.$item['url'].'">';
                                             
                            $itemInput .= '</select>';
                        }

                    }else if($item=="NULL"){
                        $itemInput = '';
                    }else{
                        $itemInput = '<label class="control-label">'.$item.'</label>
                                    <input type="text" id="'.$key.'" name="'.$key.'" class="form-control" placeholder="Masukan '.$item.'" value ="'.$valueItem.'">';
                    }
                    
                    if($x%2==0){
                    ?>
                        
                            <div class="col-md-<?php echo 12/$grid ?> mb-3">
                                <div class="form-group m-0">
                                    <?php echo $itemInput; ?>
                                </div>
                            </div>
                          
                    <?php
                	}else{
                	?>
                		 <div class="col-md-<?php echo 12/$grid ?>  mb-3">
                                <div class="form-group m-0">
                                    <?php echo $itemInput; ?>
                                </div>
                            </div>
                           
                        
                	<?php	
                	}

                    $x++;
                    }
                    ?>  
                    </div>
                    </div>
                    <div class="form-actions mt-2">
                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> <?php echo (isset($submitLabel))?$submitLabel:"Simpan Data" ?></button>
                        <?php if(isset($back_url)){ ?>
                             <a href="<?php echo $back_url ?>" class="btn btn-primary" >kembali</a> 
                        <?php
                        }else{
                        ?>
                        <!-- <button type="button" class="btn btn-primary" onclick="window.history.go(-1); return false;">Cancel</button> -->
                        <?php
                        }    
                        ?>
                    </div>
                </form>
            </div>
            </div>
        </div> 
</div>
<script type="text/javascript">
    <?php 
    if($action=="add"){
        //echo "$('form')[0].reset();";
    } 

    ?>
</script>