<div class="card shadow mb-4">
            <div class="card-header  py-3 <?php echo (isset($bgHeader)?$bgHeader:'') ?>"> 
                <h6 class="m-0 font-weight-bold text-primary"><?php echo $title; ?></h6>   
                <?php  
                    
                    if(strlen($this->session->flashdata('message'))>1){
                        echo $this->session->flashdata('message');
                    }else if(strlen($this->session->userdata('message'))>1){
                        echo $this->session->userdata('message');
                        $this->session->userdata('message',"");
                    }
                ?>     
                </div>
                <div class="card-body">
                   
                    <?php
                    if(isset($hidden)){ 
                       echo form_open_multipart($save_url , '', $hidden); 
                    }else{
                       echo form_open_multipart($save_url , ''); 
                    }
                    if(!isset($grid)){
                        $grid = 2; 
                    }
                    ?>
                    <!-- <form action="<?php echo $save_url ?>" method="POST" enctype="multipart"> -->
                        <input type="hidden" name="action" value="<?php echo $action ?>">
                        <input type="hidden" name="field_primary" value="<?php echo $field_primary ?>">
                        <div><?php //echo (validation_errors()); ?></div>
                        <div class="form-body">
                        <div class="row p-t-20">
                        <?php
                        $x=0;
                        foreach($field as $key=>$item){

                        if(isset($data[$key])){
                            $valueItem = $data[$key];
                        }else{
                            $valueItem=set_value($key);
                        }
                        if(is_array($item)){
                            if($item['type']=="combo"){
                                $itemInput = '<label class="control-label">'.$item['label'].'</label><br>
                                                <select name="'.$key.'" class="form-control select2" id="'.$key.'" >';
                                                foreach ($item['data'] as $indexItem => $Itemvalue) {
                                                    if($valueItem ==$Itemvalue['value'] ){
                                                        $selected = "selected";
                                                    }else{
                                                        $selected = "";
                                                    }
                                                    $itemInput .= '<option '.$selected.' value="'.$Itemvalue['value'].'">'.$Itemvalue['label'].'</option>';
                                                }
                                $itemInput .= '</select>';
                            } elseif($item['type']=="combocustom"){
                                $itemInput = '<label class="control-label">'.$item['label'].'</label>
                                                <select name="'.$key.'" class="form-control select2custom" id="'.$key.'" >';
                                                foreach ($item['data'] as $indexItem => $Itemvalue) {
                                                    if($valueItem ==$Itemvalue['value'] ){
                                                        $selected = "selected";
                                                    }else{
                                                        $selected = "";
                                                    }
                                                    $itemInput .= '<option '.$selected.' value="'.$Itemvalue['value'].'">'.$Itemvalue['label'].'</option>';
                                                }
                                $itemInput .= '</select>';
                            }elseif($item['type']=="combomulti"){
                                $itemInput = '<label class="control-label">'.$item['label'].'</label>
                                                <select name="'.$key.'" class="form-control select2custom" id="'.$key.'" multiple="true">';
                                                foreach ($item['data'] as $indexItem => $Itemvalue) {
                                                    if($valueItem ==$Itemvalue['value'] ){
                                                        $selected = "selected";
                                                    }else{
                                                        $selected = "";
                                                    }
                                                    $itemInput .= '<option '.$selected.' value="'.$Itemvalue['value'].'">'.$Itemvalue['label'].'</option>';
                                                }
                                $itemInput .= '</select>';
                            }else if($item['type']=="date"){
                                $itemInput = '
                                    <label class="control-label">'.$item['label'].'</label>
                                    <div class="form-group"> 
                                        <input class="form-control date" id="'.$key.'" name="'.$key.'" class="form-control" placeholder="Masukan '.$item['label'].'"
                                        value ="'.$valueItem.'"/>
                                      </div> ';
                            }else if($item['type']=="month"){
                                $itemInput = '<label class="control-label">'.$item['label'].'</label>
                                        <input type="month" id="'.$key.'"  name="'.$key.'"    id="'.$key.'" name="'.$key.'" class="form-control" placeholder="Masukan '.$item['label'].'"
                                        value ="'.$valueItem.'">';
                            }else if($item['type']=="file"){
                                $itemInput = '<label class="control-label">'.$item['label'].'</label>
                                        <input type="file" id="'.$key.'" name="'.$key.'" class="form-control" placeholder="Masukan '.$item['label'].'"
                                        value ="'.$valueItem.'">';
                            }else if($item['type']=="textare"){
                                $itemInput = '<label class="control-label">'.$item['label'].'</label>
                                        <textarea   id="'.$key.'" name="'.$key.'" class="form-control" placeholder="Masukan '.$item['label'].'" >'.$valueItem.'</textarea>';
                            }else if($item['type']=="check"){
                                $itemInput = ' 
                                       <span>'.$item['label'].'</span>
                                       <input type="checkbox" value="1" name="'.$key.'"   >';
                            }else if($item['type']=="label"){
                                $itemInput = '  
                                       <span>'.$item['label'].' : </span>
                                       <span class="strong"><b>'.$item['value'].'</b></span>
                                       <input type="hidden" value="'.$item['value'].'" name="'.$key.'"> ';
                            }

                        }else if($item=="NULL"){
                            $itemInput = '';
                        }else{
                            $itemInput = '<label class="control-label">'.$item.'</label>
                                        <input type="text" id="'.$key.'" name="'.$key.'" class="form-control" disabled="true" placeholder="Masukan '.$item.'" value ="'.$valueItem.'">';
                        }
                        
                        if($x%2==0){
                        ?>
                            
                                <div class="col-md-<?php echo 12/$grid ?>">
                                    <div class="form-group m-0">
                                        <?php echo $itemInput; ?>
                                    </div>
                                </div>
                              
                        <?php
                        }else{
                        ?>
                             <div class="col-md-<?php echo 12/$grid ?>">
                                    <div class="form-group m-0">
                                        <?php echo $itemInput; ?>
                                    </div>
                                </div>
                               
                            
                        <?php   
                        }

                        $x++;
                        }
                        ?>  
                        </div>
                        </div>
                        <div class="form-actions mt-2"> 
                            <?php if(isset($back_url)){ ?>
                               <a href="<?php echo $back_url ?>" class="btn btn-primary" >Kembali</a>
                            <?php
                            }else{
                            ?>
                            <!-- <button type="button" class="btn btn-primary" onclick="window.history.go(-1); return false;">Cancel</button> -->
                            <?php
                            }    
                            ?>
                        </div>
                    </form>
                </div>
            </div>
        </div>
</div>
