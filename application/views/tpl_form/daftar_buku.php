<?php
//$this->load->view('_templates/dashboard/_header');
?>


		<div class="row">
			<?php
			foreach ($list_data as $key => $value) { 
			?>
			<div class="col-md-4">
				<div class="box" style="padding: 10px;">
					<h3 class="box-title text-center"><?php echo $value['judul_buku'] ?></h3>
				  <img src="<?php echo base_url('uploads/buku/'.$value['gambar']) ?>" class="card-img-top" alt="..." width="300px" height="300px">
				  <div class="box-body">
				    <!-- <h5 class="card-title"><?php echo $value['judul_buku'] ?></h5> -->
				    <!-- <p class="card-text"><?php echo $value['isi_buku'] ?></p> -->
				    <a target="_blank" href="<?php echo base_url('uploads/buku/'.$value['file']) ?>" class="btn btn-primary">Tampilkan</a>
				  </div>
				</div>
			</div>
			<?php
			}
			?>
		</div>
	 
<?php
//$this->load->view('_templates/dashboard/_footer');
?>
