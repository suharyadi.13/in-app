<?php
$this->load->view('_templates/auth/_header');
?>
<style type="text/css">
@media (max-width: 992px) {
    .navbar-collapse {
        position: absolute;
        top: 54px;
        left: 100%;
        padding-right: 15px;
        padding-left: 15px;
        padding-bottom: 15px;
        width: 100%;
        transition: all 0.3s ease;
        display: block;
    }
    .navbar-collapse.collapsing {
        height: auto !important;
        margin-left: 50%;
        transition: all 0.3s ease;
        display: block;
    }
    .navbar-collapse.show {
        left: 0;
    }
}
</style>
<nav class="navbar navbar-expand-lg navbar-dark fixed-top bg-dark">
	<a class="navbar-brand" href=""><i class="fa fa-lg fa-user"></i>  <?php echo $user->first_name ?></a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
	<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse bg-dark" id="navbarCollapse">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item active d-sm-none">
				<div class="row"> 
					<div class="col-4">  
						<img src="<?=base_url()?>assets/dist/img/user1.png" class="img-fluid img-thumbnail" alt="User Image">
					</div>
					<div class="col-8 small">
						<ul class="list-group list-group-flush text-light "> 
						  <li class=""><?php echo $user->first_name.' '.$user->last_name ?></li>
						  <li class=""><?php echo $user->email ?></li>
						  <li class=""><?php echo $user->phone ?></li>
						  <li class=""><?php echo $user->username ?></li>
						  <li class=""><?php echo date("d-M-Y H:i:s",($user->last_login)); ?></li>
						   
						</ul>
					</div>
				</div>
				<a class="nav-link" href="#"></a>
			</li>
			<?php

			foreach ($modul as $key => $value) {
				if($value['url']==""){
			 		$link = "javascript:popUp('Informasi','Maaf, Modul Sedang dalam perbaikan')";	
			 	}else{
			 		$link = $value['url']."?data=".$sendData;	
			 	}
			echo '
			<li class="nav-item">
				<a class="nav-link" href="'.$link.'">'.$key.'</a>
			</li>';
			 
			}
			?>
			<li class="d-xs-none"><a href="<?php echo site_url('auth/logout') ?>" class="btn btn-warning btn-sm btn-block">
						  	Logout</a></li>
		</ul> 
	  </div>
    </nav>
<div class="row pt-5  mt-xl-4" style="background-image: linear-gradient(180deg,#45ab8f, #7deacc);height: 100% !important "> 
	<div class="col-12 col-md-10 offset-md-1">
		<?php 
		$messageNotif ="";
		if(strlen($this->session->flashdata('message'))>1){
			$messageNotif= $this->session->flashdata('message');
		}else if($this->input->get('error')!=""){
			$messageNotif= base64_decode($this->input->get('error'));
		} 
		

		if($messageNotif != ""){
			echo '<div class="bg-warning text-center p-2"> '.$messageNotif.'</div> ';
		}
		?>
	<a href="<?php echo site_url('home') ?>" class="btn btn-dark btn-block"><i class="fa fa-home"></i> Kembali</a>		 
	</div>
	 
	<div class="col-10 offset-1 mt-5 ">
		<div class="card bg-secondary text-white">
			<div class="card-header ">
				<h3>Form Topup</h3>
			</div>
			<div class="card-body bg-white text-dark"> 
			<?php 
			echo form_open(site_url('wallet/proses_topup'))
			?>
			 
				<div class="form-group">
					<label>Masukan Nominal</label>
					<input type="text" class="form-control" name="nominal">
				</div> 
				<div class="form-group"> 
					<button type="submit" class="btn btn-primary btn-block">Proses</button>
					
				</div> 
			<?php 
			echo form_close();
			?>
			</div>
		</div>
	</div>
	<div class="col-12 col-md-10 offset-md-1 mt-5 mb-5">
		<div class="card bg-primary text-white">
			<div class="card-header">
				<div class='float-left' ><i class="fa fa-money"></i>  Saldo :<br>Rp. <?php echo (is_numeric($saldo)?number_format($saldo):$saldo); ?></div>
				<h5 class="text-center">History Keuangan</h5>
				
			</div>
			<div class="card-body bg-white">
				<div class="table-responsive">
					<table class="table display nowrap"  id="mytable" style="width:100%">
						<thead>
							<tr>
							<td>No</td>
							<td></td> 
							<td>Aksi</td>
							<td>Nominal</td>
							<td>Tanggal</td> 
							</tr>
						</thead> 
						<tbody>
						<?php 
						$x=1;
						if(is_array($mutasi)){
							foreach ($mutasi as $key => $value) { 
							 	echo '
								<tr>
								<td>'.$x.'</td>
								<td>';
								if($value['status']==1){
									echo '<a target="_blank" href="'.site_url('wallet/chat/?no='.$this->config->item('PPOB_CS').'&message='.urlencode("mohon diproses untuk topup dengan kode ".$value['ref_id'].", Berikut ini saya lampirkan bukti transfernya")).'" class="btn btn-success  btn-sm"><i class="fa fa-whatsapp"></i></a>';
								}else if($value['status']==2){
									echo '<span class="btn btn-full text-success"><i class="fa fa-check">Sukses</span>';
								}else{
									echo '<span class="btn btn-full text-danger"><i class="fa fa-lock">Gagal</span>';
								}
								echo '</td>
								
								<td>'.$value['jenis_transaksi'].'</td>
								<td>'.number_format($value['nominal']).'</td>
								<td>'.$value['datetime'].'</td> ';
								
								echo '</tr>'; 
								$x++;
							}
						 
						}
						?>
						</tbody>
						
					</table>
				</div>
			</div>
		</div>
	</div>
	
</div>

<?php
$this->load->view('_templates/auth/_footer');
?>

<script type="text/javascript">
  
</script>