<?php
$this->load->view('_templates/auth/_header');
?>
<style type="text/css">
@media (max-width: 992px) {
    .navbar-collapse {
        position: absolute;
        top: 54px;
        left: 100%;
        padding-right: 15px;
        padding-left: 15px;
        padding-bottom: 15px;
        width: 100%;
        transition: all 0.3s ease;
        display: block;
    }
    .navbar-collapse.collapsing {
        height: auto !important;
        margin-left: 50%;
        transition: all 0.3s ease;
        display: block;
    }
    .navbar-collapse.show {
        left: 0;
    }
}
</style>
<nav class="navbar navbar-expand-lg navbar-dark fixed-top bg-dark">
	<a class="navbar-brand" href=""><i class="fa fa-lg fa-user"></i>  <?php echo $user->first_name ?></a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
	<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse bg-dark" id="navbarCollapse">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item active d-sm-none">
				<div class="row"> 
					<div class="col-4">  
						<img src="<?=base_url()?>assets/dist/img/user1.png" class="img-fluid img-thumbnail" alt="User Image">
					</div>
					<div class="col-8 small">
						<ul class="list-group list-group-flush text-light "> 
						  <li class=""><?php echo $user->first_name.' '.$user->last_name ?></li>
						  <li class=""><?php echo $user->email ?></li>
						  <li class=""><?php echo $user->phone ?></li>
						  <li class=""><?php echo $user->username ?></li>
						  <li class=""><?php echo date("d-M-Y H:i:s",($user->last_login)); ?></li>
						   
						</ul>
					</div>
				</div>
				<a class="nav-link" href="#"></a>
			</li>
			<?php

			foreach ($modul as $key => $value) {
				if($value['url']==""){
			 		$link = "javascript:popUp('Informasi','Maaf, Modul Sedang dalam perbaikan')";	
			 	}else{
			 		if($key =="E-Wallet"){ 
			 			if(!$this->ion_auth->is_admin()){
			 				$link = site_url('wallet');
			 			}else{
			 				$link = $value['url']."?data=".$sendData;
			 			}
			 		}else{
			 			$link = $value['url']."?data=".$sendData;
			 		}
			 		 
			 	}
			echo '
			<li class="nav-item">
				<a class="nav-link" href="'.$link.'">'.$key.'</a>
			</li>';
			 
			}
			?>
			<li class="d-xs-none"><a href="<?php echo site_url('auth/logout') ?>" class="btn btn-warning btn-sm btn-block">
						  	Logout</a></li>
		</ul> 
	  </div>
    </nav>
<div class="row pt-5  mt-xl-4" style="background-image: linear-gradient(180deg,#45ab8f, #7deacc);height: 100% !important "> 
	<div class="col-12 com-md-10 offset-md-1">
		<?php 
		$messageNotif ="";
		if(strlen($this->session->flashdata('message'))>1){
			$messageNotif= $this->session->flashdata('message');
		}else if($this->input->get('error')!=""){
			$messageNotif= base64_decode($this->input->get('error'));
		} 
		

		if($messageNotif != ""){
			echo '<div class="bg-warning text-center p-2"> '.$messageNotif.'</div> ';
		}
		?> 
	</div>
	<div class="col-10  offset-1 text-cneter pt-3 " >
		<div class="row row-cols-1 ">
		<div class="card bg-warning" style="  border-radius:50px !important" >
		  <!-- <img class="card-img-top" src="<?php echo base_url('assets/dist/img/wallet.png') ?>" class="card-img-top" alt="..."> -->
		  <div class="card-body pt-1 pr-5 pl-5">
		    <div class="row" style="border-radius:50px !important">
			<div class="col-6 text-left p-2   mt-2 " >
				<i class="fa fa-money"></i> Saldo <br> <h5> Rp. <?php echo (is_numeric($saldo)? number_format($saldo):$saldo); ?></h5>
			</div>
			<div class="col-6 text-right p-2 heading-2 mt-2">
				<a href="<?php echo site_url('wallet') ?>" class="btn btn-primary text-white">Topup</a>
			</div>
			</div>
		  </div>
		</div>
		</div>
		
	</div> 
	<div class="col-10 col-xl-5 offset-1" >
		<div class="card mt-4" style="border-radius: 25px;">
			<div class="" >
				<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
				  <div class="carousel-inner" style="border-radius: 25px;">
				  	<?php 
				  	$x=0;
				  	$active = "";
				  	for ($i=1; $i<=3 ; $i++) { 
				  		if($x<=0){
				  			$active ="active";
				  		}else{
				  			$active ="";
				  		}

				  	?>
				    <div class="carousel-item <?php echo $active ?> ">
				      <img src="<?php echo base_url('assets/dist/img/img.jpg') ?>" class="d-block w-100" alt="slide">
				    </div> 
				    <?php
				    $x++;
					}
				  	?>
				  </div>
				  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
				    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
				    <span class="sr-only">Previous</span>
				  </a>
				  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
				    <span class="carousel-control-next-icon" aria-hidden="true"></span>
				    <span class="sr-only">Next</span>
				  </a>
				</div>
			</div>
		</div>
	</div>

	<div class="col-10 col-xl-4 offset-1 mt-4">
		<div class="row">
		<?php

		foreach ($modul as $key => $value) {
		if($value['url']==""){
	 		$link = "javascript:popUp('Informasi','Maaf, Modul Sedang dalam perbaikan')";	
	 	}else{
	 		if($key =="E-Wallet"){ 
	 			if(!$this->ion_auth->is_admin()){
	 				$link = site_url('wallet');
	 			}else{
	 				$link = $value['url']."?data=".$sendData;
	 			}
	 		}else{
	 			$link = $value['url']."?data=".$sendData;
	 		}
	 	}
		?>	
		<div class="col-4">
			<div class="">
				<div class=" " style="">
				<a href="<?php echo $link  ?>" class="text-white">
				  <img src="<?php echo base_url('assets/dist/img/'.$value['image']) ?>" class="card-img-top" alt="<?php echo $key ?>">
				  <div class="text-center">
				    <div class="card-title"><?php echo $value['label']?></div> 
				  </div>
				</a>
				</div>
			</div>
		</div>
		<?php
		} 
		?>
	   </div>
	</div>
	<div class="col-12 col-xl-10 offset-xl-1 mt-4">
		<div class="card text-left   text-white bg-info mb-3">
		  <div class="card-header">
		  	<ul class="nav nav-pills card-header-pills">
		  	<li class="nav-item active">
			    <a class="nav-link   text-white" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true"><i class="fa fa-lg fa-bell"></i></a>
			  </li>	
		  	<li class="nav-item"> 
			    <a class="nav-link    text-white" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false"><i class="fa fa-lg fa-users"></i></a>
			  </li> 
			  <li class="nav-item">
			    <a class="nav-link   text-white" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false"><i class="fa fa-lg fa-book"></i></a>
			  </li> 
		    </ul>
		  </div>
		  <div class="card-body bg-white text-dark">
		    <div class="tab-content" id="myTabContent" style="min-height: 200px;">
		      <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">  
		      	<h5 class="card-title">Top 10 User Terbaru</h5>
			  	<ol class="list-group list-group-flush">  
			  		<?php 
			  			foreach ($top_user as $key => $value) {
			  				echo '<li class="list-group-item d-flex justify-content-between align-items-left">  '.$value['first_name'].' '.$value['last_name'].' <span class="badge badge-primary badge-pill"><i class="fa fa-user"></i></span></li>';
			  			}
			  		?>
				   
				</ol>
			  </div>
			  <div class="tab-pane fade  show active" id="home" role="tabpanel" aria-labelledby="home-tab">
			  	<h5 class="card-title">Notifikasi</h5>
			  	<ul class="list-group">
			  	<?php  
			  	$urutan = array('subuh','terbit','dhuha','dzuhur','ashar','maghrib','isya');
			  	if(isset($notifikasi['jadwal']->data)){
			  	$jadwalSholat = (array)$notifikasi['jadwal']->data;
			  	foreach ($urutan as $key => $value) {
			  		
			  		echo '
					  <li class="list-group-item d-flex justify-content-between align-items-center">
					   '.$value.'
					    <span class="badge badge-primary badge-pill">'.$jadwalSholat[$value].'</span>
					  </li>';
					}
				 }
				 ?> 
				</ul>

			  </div>
			  <div class="tab-pane fade " id="profile" role="tabpanel" aria-labelledby="profile-tab">
			  	<h5 class="card-title">Berita Terbaru</h5>
			  	<ul>
                   <?php
                   foreach ($berita as $key => $value) {
                       echo '<li>'.$value['judul_berita'].'<p>'.$value['isi_berita'].'</p>';
                   }
                   ?>
               </ul>
			  </div> 
			</div>
		  </div>
		</div>
	</div>
</div>

<?php
$this->load->view('_templates/auth/_footer');
?>

<script type="text/javascript">
$('.carousel').carousel()
$(document).ready(function() {   
    var sideslider = $('[data-toggle=collapse-side]');
    var sel = sideslider.attr('data-target');
    var sel2 = sideslider.attr('data-target-2');
    sideslider.click(function(event){
        $(sel).toggleClass('in');
        $(sel2).toggleClass('out');
    });
});	
 
</script>