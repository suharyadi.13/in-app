			</section>
			<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->

			<!-- Main Footer -->
			<footer class="main-footer">
				<!-- To the right -->
				<div class="pull-right hidden-xs">
					<?php echo $this->config->item('company_name'); ?> v2
				</div>
				<!-- Default to the left -->
				<strong>Copyright &copy; 2019. By <a href="#">OrbIT</a>. </strong> All rights reserved
			</footer>

			</div>
			
			<!-- Required JS -->
			 
			<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script> 
			<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script> 


			<!-- Datatables Buttons -->
			<script src="<?= base_url() ?>assets/bower_components/datatables.net-bs/plugins/Buttons-1.5.6/js/dataTables.buttons.min.js"></script>
			<script src="<?= base_url() ?>assets/bower_components/datatables.net-bs/plugins/Buttons-1.5.6/js/buttons.bootstrap.min.js"></script>
			<script src="<?= base_url() ?>assets/bower_components/datatables.net-bs/plugins/JSZip-2.5.0/jszip.min.js"></script>
			<script src="<?= base_url() ?>assets/bower_components/datatables.net-bs/plugins/pdfmake-0.1.36/pdfmake.min.js"></script>
			<script src="<?= base_url() ?>assets/bower_components/datatables.net-bs/plugins/pdfmake-0.1.36/vfs_fonts.js"></script>
			<script src="<?= base_url() ?>assets/bower_components/datatables.net-bs/plugins/Buttons-1.5.6/js/buttons.html5.min.js"></script>
			<script src="<?= base_url() ?>assets/bower_components/datatables.net-bs/plugins/Buttons-1.5.6/js/buttons.print.min.js"></script>
			<script src="<?= base_url() ?>assets/bower_components/datatables.net-bs/plugins/Buttons-1.5.6/js/buttons.colVis.min.js"></script>

			<script src="<?= base_url() ?>assets/bower_components/pace/pace.min.js"></script>
			<script src="<?= base_url() ?>assets/dist/js/adminlte.min.js"></script>

			<!-- Textarea editor -->
			<script src="<?= base_url() ?>assets/bower_components/codemirror/lib/codemirror.min.js"></script>
			<script src="<?= base_url() ?>assets/bower_components/codemirror/mode/xml.min.js"></script>
			 

			<!-- App JS -->
			<script src="<?= base_url() ?>assets/dist/js/app/dashboard.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.2.1/jquery.tinymce.min.js" integrity="sha256-QSEmWaMfbggoQqgSQiT2DuMAUwPpgm3I486JaEUEEb4=" crossorigin="anonymous"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.2.1/tinymce.min.js" integrity="sha256-6Q5EaYOf1K2LsiwJmuGtmWHoT1X/kuXKnuZeGudWFB4=" crossorigin="anonymous"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.2.1/plugins/image/plugin.min.js" integrity="sha256-byAWT5UyM3Jh1egZk3TGI6l5Cj5LwYGPJxjAuvsMbck=" crossorigin="anonymous"></script>
			
			<script>
			  tinymce.init({
			    selector: 'textarea',
			    menubar: true
			  });
			</script>
			<!-- Custom JS -->
			<script type="text/javascript">
				$.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
					return {
						"iStart": oSettings._iDisplayStart,
						"iEnd": oSettings.fnDisplayEnd(),
						"iLength": oSettings._iDisplayLength,
						"iTotal": oSettings.fnRecordsTotal(),
						"iFilteredTotal": oSettings.fnRecordsDisplay(),
						"iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
						"iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
					};
				};

				function ajaxcsrf() {
					var csrfname = '<?= $this->security->get_csrf_token_name() ?>';
					var csrfhash = '<?= $this->security->get_csrf_hash() ?>';
					var csrf = {};
					csrf[csrfname] = csrfhash;
					$.ajaxSetup({
						"data": csrf
					});
				}

				function reload_ajax() {
					table.ajax.reload(null, false);
				}
				function popUp($title,message){ 
				Swal({
					title: $title,
					text: message, 
					showCancelButton: false,
					confirmButtonColor: '#3085d6', 
					confirmButtonText: 'Ok'
				}).then((result) => {
					 
				});

				}
				$("#mytable").DataTable({
					dom: 'Bfrtip',
					buttons: [
					    'copy', 'csv', 'excel', 'pdf', 'print'
					]
				});
				
			</script>
			<?php
			if(isset($js_file)){
				echo '<script src="'.base_url(),'assets/dist/js/app/'.$js_file.'"></script>';
			}
			?>
			</body>

			</html>