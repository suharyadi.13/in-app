<script src="<?=base_url()?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>assets/plugins/iCheck/icheck.min.js"></script>
<!-- Required JS -->
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script> 
<script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script> 
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.flash.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script> 
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script> 
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script> 

<script>
    $(function () {
        $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' /* optional */
        });
    });
    $("#mytable").DataTable({
		dom: 'Bfrtip',
		buttons: [
		    'excel', 'pdf', 'print'
		]
	});
    function popUp($title,message){ 
				Swal({
					title: $title,
					text: message, 
					showCancelButton: false,
					confirmButtonColor: '#3085d6', 
					confirmButtonText: 'Ok'
				}).then((result) => {
					 
				});

				}
</script>
</body>
</html>