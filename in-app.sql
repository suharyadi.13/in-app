/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 100424
 Source Host           : localhost:3306
 Source Schema         : in-app

 Target Server Type    : MySQL
 Target Server Version : 100424
 File Encoding         : 65001

 Date: 03/08/2024 19:49:06
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for groups
-- ----------------------------
DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups`  (
  `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of groups
-- ----------------------------
INSERT INTO `groups` VALUES (1, 'admin', 'Administrator');
INSERT INTO `groups` VALUES (2, 'guru', 'Pembuat Soal dan ujian');
INSERT INTO `groups` VALUES (3, 'siswa', 'Peserta Ujian');

-- ----------------------------
-- Table structure for guru
-- ----------------------------
DROP TABLE IF EXISTS `guru`;
CREATE TABLE `guru`  (
  `id_guru` int(11) NOT NULL AUTO_INCREMENT,
  `nip` char(12) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nama_guru` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `email` varchar(254) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `matkul_id` int(11) NOT NULL,
  PRIMARY KEY (`id_guru`) USING BTREE,
  UNIQUE INDEX `email`(`email`) USING BTREE,
  UNIQUE INDEX `nip`(`nip`) USING BTREE,
  INDEX `matkul_id`(`matkul_id`) USING BTREE,
  CONSTRAINT `guru_ibfk_1` FOREIGN KEY (`matkul_id`) REFERENCES `matkul` (`id_matkul`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1016 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of guru
-- ----------------------------
INSERT INTO `guru` VALUES (1000, '20122251000', 'Drs. H. AEP GUMIWA, MM', 'aep@orbit.co.id', 10);
INSERT INTO `guru` VALUES (1001, '20122251001', 'Dra. Hj. NURAENI GUMILAR', 'eni@orbit.co.id', 11);
INSERT INTO `guru` VALUES (1002, '20122251002', 'Drs. ASEP SUHENDI', 'asep@orbit.co.id', 12);
INSERT INTO `guru` VALUES (1003, '20122251003', 'Dra. Hj. IDA FARIDA', 'ida@orbit.co.id', 13);
INSERT INTO `guru` VALUES (1004, '20122251004', 'Dra. ETTY SUGIARTI', 'eti@orbit.co.id', 14);
INSERT INTO `guru` VALUES (1005, '20122251005', 'DR. H. ARIS GUMILAR, MM', 'aris@orbit.co.id', 15);
INSERT INTO `guru` VALUES (1006, '20122251006', 'Rd.H. RUHIYAT TAUFIK, SE, MM', 'ruhiyat@orbit.co.id', 20);
INSERT INTO `guru` VALUES (1007, '20122251007', 'Dra. TRI HASTUTI', 'tri@orbit.co.id', 21);
INSERT INTO `guru` VALUES (1008, '20122251008', 'Drs. MAHMUD YUNUS', 'mahmud@orbit.co.id', 30);
INSERT INTO `guru` VALUES (1009, '20122251009', 'Drs. R. MARGONO', 'margono@orbit.co.id', 31);
INSERT INTO `guru` VALUES (1010, '20122251010', 'Dra. Hj. MACHFUDOH BAHTA', 'machfudoh@orbit.co.id', 52);
INSERT INTO `guru` VALUES (1011, '20122251011', 'LIDA MELANI', 'lida@orbit.co.id', 51);
INSERT INTO `guru` VALUES (1012, '20122251012', 'Dra. HERLINA YULIASIH', 'herlina@orbit.co.id', 41);
INSERT INTO `guru` VALUES (1013, '20122251013', 'Dra. ENTIN SURYANINGSIH', 'entin@orbit.co.id', 42);
INSERT INTO `guru` VALUES (1015, '20070010', 'Suhendi', 'suhendi@yahoo.com', 15);

-- ----------------------------
-- Table structure for h_ujian
-- ----------------------------
DROP TABLE IF EXISTS `h_ujian`;
CREATE TABLE `h_ujian`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ujian_id` int(11) NOT NULL,
  `siswa_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `list_soal` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `list_jawaban` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `jml_benar` int(11) NOT NULL,
  `nilai` decimal(10, 2) NOT NULL,
  `nilai_bobot` decimal(10, 2) NOT NULL,
  `tgl_mulai` datetime(0) NOT NULL,
  `tgl_selesai` datetime(0) NOT NULL,
  `status` enum('Y','N') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `ujian_id`(`ujian_id`) USING BTREE,
  INDEX `siswa_id`(`siswa_id`) USING BTREE,
  CONSTRAINT `h_ujian_ibfk_1` FOREIGN KEY (`ujian_id`) REFERENCES `m_ujian` (`id_ujian`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of h_ujian
-- ----------------------------
INSERT INTO `h_ujian` VALUES (1, 5, '7100005', '7,2,34,37,9,4,40,1,25,22,30,39,31,19,28,13,21,8,10,26,24,17,18,3,20,29,36,38,32,27,35,12,14,11,16,23,15,33,5,6', '7:B:N,2:C:N,34:A:N,37:B:N,9:A:N,4:D:N,40:D:N,1:C:N,25:C:N,22:A:N,30:D:N,39:B:N,31:D:N,19:C:N,28:C:N,13:B:N,21:C:N,8:E:N,10:B:Y,26:B:N,24:A:N,17:E:N,18:A:N,3:B:N,20:E:N,29:D:N,36:E:N,38:D:N,32:D:N,27:C:N,35:D:N,12:C:N,14:E:N,11:B:N,16:E:N,23:B:N,15:C:N,33:D:N,5:D:N,6:C:N', 9, 22.00, 100.00, '2020-07-12 08:56:23', '2020-07-12 09:36:23', 'N');

-- ----------------------------
-- Table structure for jurusan
-- ----------------------------
DROP TABLE IF EXISTS `jurusan`;
CREATE TABLE `jurusan`  (
  `id_jurusan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jurusan` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id_jurusan`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7410 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jurusan
-- ----------------------------
INSERT INTO `jurusan` VALUES (7100, 'Sistem Informasi');
INSERT INTO `jurusan` VALUES (7200, 'Teknik Informatika');
INSERT INTO `jurusan` VALUES (7300, 'Akuntansi');
INSERT INTO `jurusan` VALUES (7400, 'Pemasaran');
INSERT INTO `jurusan` VALUES (7409, 'Kimia Analis');

-- ----------------------------
-- Table structure for jurusan_matkul
-- ----------------------------
DROP TABLE IF EXISTS `jurusan_matkul`;
CREATE TABLE `jurusan_matkul`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `matkul_id` int(11) NOT NULL,
  `jurusan_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `jurusan_id`(`jurusan_id`) USING BTREE,
  INDEX `matkul_id`(`matkul_id`) USING BTREE,
  CONSTRAINT `jurusan_matkul_ibfk_1` FOREIGN KEY (`jurusan_id`) REFERENCES `jurusan` (`id_jurusan`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `jurusan_matkul_ibfk_2` FOREIGN KEY (`matkul_id`) REFERENCES `matkul` (`id_matkul`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 40 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jurusan_matkul
-- ----------------------------
INSERT INTO `jurusan_matkul` VALUES (1, 20, 7200);
INSERT INTO `jurusan_matkul` VALUES (2, 21, 7200);
INSERT INTO `jurusan_matkul` VALUES (10, 41, 7300);
INSERT INTO `jurusan_matkul` VALUES (11, 13, 7300);
INSERT INTO `jurusan_matkul` VALUES (12, 13, 7400);
INSERT INTO `jurusan_matkul` VALUES (13, 13, 7100);
INSERT INTO `jurusan_matkul` VALUES (14, 13, 7200);
INSERT INTO `jurusan_matkul` VALUES (15, 11, 7300);
INSERT INTO `jurusan_matkul` VALUES (16, 11, 7400);
INSERT INTO `jurusan_matkul` VALUES (17, 11, 7100);
INSERT INTO `jurusan_matkul` VALUES (18, 11, 7200);
INSERT INTO `jurusan_matkul` VALUES (19, 12, 7300);
INSERT INTO `jurusan_matkul` VALUES (20, 12, 7400);
INSERT INTO `jurusan_matkul` VALUES (21, 12, 7100);
INSERT INTO `jurusan_matkul` VALUES (22, 12, 7200);
INSERT INTO `jurusan_matkul` VALUES (23, 14, 7300);
INSERT INTO `jurusan_matkul` VALUES (24, 14, 7400);
INSERT INTO `jurusan_matkul` VALUES (25, 14, 7100);
INSERT INTO `jurusan_matkul` VALUES (26, 14, 7200);
INSERT INTO `jurusan_matkul` VALUES (27, 15, 7300);
INSERT INTO `jurusan_matkul` VALUES (28, 15, 7400);
INSERT INTO `jurusan_matkul` VALUES (29, 15, 7100);
INSERT INTO `jurusan_matkul` VALUES (30, 15, 7200);
INSERT INTO `jurusan_matkul` VALUES (31, 10, 7300);
INSERT INTO `jurusan_matkul` VALUES (32, 10, 7400);
INSERT INTO `jurusan_matkul` VALUES (33, 10, 7100);
INSERT INTO `jurusan_matkul` VALUES (34, 10, 7200);
INSERT INTO `jurusan_matkul` VALUES (35, 31, 7100);
INSERT INTO `jurusan_matkul` VALUES (36, 42, 7300);
INSERT INTO `jurusan_matkul` VALUES (37, 51, 7400);
INSERT INTO `jurusan_matkul` VALUES (38, 52, 7400);
INSERT INTO `jurusan_matkul` VALUES (39, 53, 7409);

-- ----------------------------
-- Table structure for kelas
-- ----------------------------
DROP TABLE IF EXISTS `kelas`;
CREATE TABLE `kelas`  (
  `id_kelas` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kelas` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `jurusan_id` int(11) NOT NULL,
  `id_sekolah` int(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id_kelas`) USING BTREE,
  INDEX `jurusan_id`(`jurusan_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 402 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of kelas
-- ----------------------------
INSERT INTO `kelas` VALUES (1, 'X.SI', 7100, NULL);
INSERT INTO `kelas` VALUES (200, 'X.TI', 7200, NULL);
INSERT INTO `kelas` VALUES (300, 'X.A', 7300, NULL);
INSERT INTO `kelas` VALUES (400, 'X.P', 7400, NULL);
INSERT INTO `kelas` VALUES (401, 'X.KA', 7409, NULL);

-- ----------------------------
-- Table structure for kelas_guru
-- ----------------------------
DROP TABLE IF EXISTS `kelas_guru`;
CREATE TABLE `kelas_guru`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kelas_id` int(11) NOT NULL,
  `guru_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `kelas_id`(`kelas_id`) USING BTREE,
  INDEX `guru_id`(`guru_id`) USING BTREE,
  CONSTRAINT `kelas_guru_ibfk_1` FOREIGN KEY (`guru_id`) REFERENCES `guru` (`id_guru`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `kelas_guru_ibfk_2` FOREIGN KEY (`kelas_id`) REFERENCES `kelas` (`id_kelas`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of kelas_guru
-- ----------------------------
INSERT INTO `kelas_guru` VALUES (2, 1, 1001);
INSERT INTO `kelas_guru` VALUES (3, 1, 1002);
INSERT INTO `kelas_guru` VALUES (4, 1, 1003);
INSERT INTO `kelas_guru` VALUES (5, 1, 1004);
INSERT INTO `kelas_guru` VALUES (6, 1, 1005);
INSERT INTO `kelas_guru` VALUES (7, 1, 1006);
INSERT INTO `kelas_guru` VALUES (8, 300, 1007);
INSERT INTO `kelas_guru` VALUES (9, 300, 1008);
INSERT INTO `kelas_guru` VALUES (10, 300, 1009);
INSERT INTO `kelas_guru` VALUES (11, 300, 1010);
INSERT INTO `kelas_guru` VALUES (12, 400, 1010);
INSERT INTO `kelas_guru` VALUES (17, 300, 1000);
INSERT INTO `kelas_guru` VALUES (18, 400, 1000);
INSERT INTO `kelas_guru` VALUES (19, 1, 1000);
INSERT INTO `kelas_guru` VALUES (20, 200, 1000);
INSERT INTO `kelas_guru` VALUES (21, 401, 1015);

-- ----------------------------
-- Table structure for login_attempts
-- ----------------------------
DROP TABLE IF EXISTS `login_attempts`;
CREATE TABLE `login_attempts`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `login` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` int(11) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 37 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of login_attempts
-- ----------------------------
INSERT INTO `login_attempts` VALUES (26, '::1', 'siswa@gmail.com', 1706590140);
INSERT INTO `login_attempts` VALUES (27, '::1', 'siswa@gmail.com', 1706590146);
INSERT INTO `login_attempts` VALUES (28, '::1', 'siswa@gmail.com', 1706590151);
INSERT INTO `login_attempts` VALUES (29, '::1', 'admin@integral.com', 1706590170);
INSERT INTO `login_attempts` VALUES (30, '::1', 'admin@integral.com', 1706590177);
INSERT INTO `login_attempts` VALUES (31, '::1', 'aydan@gmail.com', 1706590216);
INSERT INTO `login_attempts` VALUES (32, '::1', 'aydan@gmail.com', 1706590219);
INSERT INTO `login_attempts` VALUES (33, '::1', 'aydan@gmail.com', 1706590224);
INSERT INTO `login_attempts` VALUES (35, '::1', 'admin@integral.com', 1706590433);

-- ----------------------------
-- Table structure for m_ujian
-- ----------------------------
DROP TABLE IF EXISTS `m_ujian`;
CREATE TABLE `m_ujian`  (
  `id_ujian` int(11) NOT NULL AUTO_INCREMENT,
  `guru_id` int(11) NOT NULL,
  `matkul_id` int(11) NOT NULL,
  `nama_ujian` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `jumlah_soal` int(11) NOT NULL,
  `waktu` int(11) NOT NULL,
  `jenis` enum('acak','urut') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `tgl_mulai` datetime(0) NOT NULL,
  `terlambat` datetime(0) NOT NULL,
  `token` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id_ujian`) USING BTREE,
  INDEX `matkul_id`(`matkul_id`) USING BTREE,
  INDEX `guru_id`(`guru_id`) USING BTREE,
  CONSTRAINT `m_ujian_ibfk_1` FOREIGN KEY (`guru_id`) REFERENCES `guru` (`id_guru`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `m_ujian_ibfk_2` FOREIGN KEY (`matkul_id`) REFERENCES `matkul` (`id_matkul`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of m_ujian
-- ----------------------------
INSERT INTO `m_ujian` VALUES (5, 1000, 10, 'Ujian 1', 40, 40, 'acak', '2020-07-12 08:28:32', '2020-07-13 08:28:34', 'LNMWF');
INSERT INTO `m_ujian` VALUES (6, 1000, 10, 'Ujian 1', 20, 30, 'acak', '2020-07-15 14:58:59', '2020-07-17 14:59:01', 'GOUMW');

-- ----------------------------
-- Table structure for matkul
-- ----------------------------
DROP TABLE IF EXISTS `matkul`;
CREATE TABLE `matkul`  (
  `id_matkul` int(11) NOT NULL AUTO_INCREMENT,
  `nama_matkul` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id_matkul`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 54 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of matkul
-- ----------------------------
INSERT INTO `matkul` VALUES (10, 'Matematika');
INSERT INTO `matkul` VALUES (11, 'Bahasa Indonesia');
INSERT INTO `matkul` VALUES (12, 'Bahasa Inggris');
INSERT INTO `matkul` VALUES (13, 'Agama');
INSERT INTO `matkul` VALUES (14, 'Fisika');
INSERT INTO `matkul` VALUES (15, 'Kimia');
INSERT INTO `matkul` VALUES (20, 'Informatika Dasar');
INSERT INTO `matkul` VALUES (21, 'Konsep Jaringan');
INSERT INTO `matkul` VALUES (30, 'Algoritma');
INSERT INTO `matkul` VALUES (31, 'Dasar Pemograman');
INSERT INTO `matkul` VALUES (41, 'Dasar Akuntansi');
INSERT INTO `matkul` VALUES (42, 'Dasar Ekonomi');
INSERT INTO `matkul` VALUES (51, 'Dasar Pemasaran');
INSERT INTO `matkul` VALUES (52, 'Konsep Pemasaran');
INSERT INTO `matkul` VALUES (53, 'Dasar dasar Kimia');

-- ----------------------------
-- Table structure for siswa
-- ----------------------------
DROP TABLE IF EXISTS `siswa`;
CREATE TABLE `siswa`  (
  `id_siswa` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nim` char(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `no_kartu` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `no_telp` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email` varchar(254) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `jenis_kelamin` enum('L','P') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `kelas_id` int(11) NULL DEFAULT 1 COMMENT 'kelas&jurusan',
  PRIMARY KEY (`id_siswa`) USING BTREE,
  UNIQUE INDEX `nim`(`nim`) USING BTREE,
  UNIQUE INDEX `email`(`email`) USING BTREE,
  INDEX `kelas_id`(`kelas_id`) USING BTREE,
  CONSTRAINT `siswa_ibfk_1` FOREIGN KEY (`kelas_id`) REFERENCES `kelas` (`id_kelas`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 30 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of siswa
-- ----------------------------
INSERT INTO `siswa` VALUES (1, 'Ghifari', '12183018', 'e7-69-b-4e', NULL, 'siswa@gmail.com', 'L', 1);
INSERT INTO `siswa` VALUES (2, 'suhar', '123456789', NULL, NULL, 'Suharyadi.13@gmail.com', 'L', 1);
INSERT INTO `siswa` VALUES (25, 'Thomas', '0001', NULL, '08976156258', 'info@athos-hr.com', 'L', 1);
INSERT INTO `siswa` VALUES (27, 'Sais', '12345699', NULL, '08976156259', 'Admin@saisschool.sch.id', 'L', 1);
INSERT INTO `siswa` VALUES (28, 'aydan', '1234567777', NULL, '12312313', 'aydan@gmail.com', 'L', 1);
INSERT INTO `siswa` VALUES (29, 'adi', '123456', NULL, '0123123123', 'adi@gmail.com', 'L', 1);

-- ----------------------------
-- Table structure for tb_anggota
-- ----------------------------
DROP TABLE IF EXISTS `tb_anggota`;
CREATE TABLE `tb_anggota`  (
  `kd_angg` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `ktp` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nm_angg` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kelamin` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_lahir` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tmpt_lahir` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `telp` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `alamat` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `aktif` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sejak` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `photo` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`kd_angg`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_anggota
-- ----------------------------
INSERT INTO `tb_anggota` VALUES ('AG001', '1123123', 'user1', 'L', '2020-03-02', 'Bandung', '08976156259', 'Jl. Gunung puntang rt 01/09 Ds. Campakamulya', NULL, '01-03-2020', NULL);
INSERT INTO `tb_anggota` VALUES ('AG002', NULL, 'Suhar', NULL, NULL, NULL, NULL, NULL, NULL, '19-03-2020', NULL);
INSERT INTO `tb_anggota` VALUES ('AG003', '0001', 'Thomas', NULL, NULL, NULL, '08976156258', NULL, NULL, '11-07-2020', NULL);
INSERT INTO `tb_anggota` VALUES ('AG004', '12345699', 'Sais', NULL, NULL, NULL, '08976156259', NULL, NULL, '21-07-2020', NULL);
INSERT INTO `tb_anggota` VALUES ('AG005', '1234567777', 'aydan', NULL, NULL, NULL, '12312313', NULL, NULL, '21-07-2020', NULL);
INSERT INTO `tb_anggota` VALUES ('AG006', '123456', 'adi', NULL, NULL, NULL, '0123123123', NULL, NULL, '30-01-2024', NULL);

-- ----------------------------
-- Table structure for tb_angsuran
-- ----------------------------
DROP TABLE IF EXISTS `tb_angsuran`;
CREATE TABLE `tb_angsuran`  (
  `no_kwitansi` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kd_pinj` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_bayar` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jml_bayar` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `angs_ke` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tb_deb_simpanan
-- ----------------------------
DROP TABLE IF EXISTS `tb_deb_simpanan`;
CREATE TABLE `tb_deb_simpanan`  (
  `kd_deb_simp` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `kd_angg` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jml_deb` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_deb_simp` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`kd_deb_simp`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tb_kategori
-- ----------------------------
DROP TABLE IF EXISTS `tb_kategori`;
CREATE TABLE `tb_kategori`  (
  `id_kat` bigint(255) NOT NULL AUTO_INCREMENT,
  `nm_kategori` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `seo` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_kat`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_kategori
-- ----------------------------
INSERT INTO `tb_kategori` VALUES (1, 'Makanan', 'makanan');
INSERT INTO `tb_kategori` VALUES (2, 'Minuman', 'minuman');
INSERT INTO `tb_kategori` VALUES (3, 'Pakain', 'pakain');
INSERT INTO `tb_kategori` VALUES (4, 'Obat-obatan', 'obat-obatan');

-- ----------------------------
-- Table structure for tb_materi
-- ----------------------------
DROP TABLE IF EXISTS `tb_materi`;
CREATE TABLE `tb_materi`  (
  `id_materi` int(11) NOT NULL AUTO_INCREMENT,
  `guru_id` int(11) NOT NULL,
  `matkul_id` int(11) NOT NULL,
  `bobot` int(11) NOT NULL,
  `file` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `tipe_file` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `materi` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `created_on` int(11) NOT NULL,
  `updated_on` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_materi`) USING BTREE,
  INDEX `matkul_id`(`matkul_id`) USING BTREE,
  INDEX `guru_id`(`guru_id`) USING BTREE,
  CONSTRAINT `tb_materi_ibfk_1` FOREIGN KEY (`matkul_id`) REFERENCES `matkul` (`id_matkul`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `tb_materi_ibfk_2` FOREIGN KEY (`guru_id`) REFERENCES `guru` (`id_guru`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_materi
-- ----------------------------
INSERT INTO `tb_materi` VALUES (2, 1, 1, 0, '44ac3d5516ffc25ff1f3d51bd3e3d3d7.jpg', 'image/jpeg', '<p>Periklanan adalah satu dari empat barang penting yang digunakan oleh</p>\r\n<p>perusahaan untuk melancarkan komunikasi persuasif terhadap pembeli dan</p>\r\n<p>masyarakat yang ditargetkan. Pada dasarnya periklanan merupakan salah satu</p>\r\n<p>bentuk komunikasi untuk memenuhi fungsi pemasaran. </p>\r\n<p>Periklanan harus mampu</p>\r\n<p>membujuk konsumen supaya berperilaku sedemikian rupa sesuai dengan strategi</p>\r\n<p>pemasaran pada perusahaan untuk mendapatkan penjualan dan laba.</p>\r\n<p>Periklanan juga dipandang sebagai salah satu media yang paling efektif</p>\r\n<p>dalam mengkomunikasikan suatu produk dan jasa. Selain itu juga periklanan</p>\r\n<p>dibuat oleh setiap perusahaan tidak lain agar konsumen tertarik dan berharap</p>\r\n<p>tidak akan berpaling dari perusahaan yang sejenis lainnya, karena itu perusahanan</p>\r\n<p>harus menciptakan iklan yang semenarik mungkin.</p>\r\n<p>Adapun menurut Tjiptono dalam Rahman, (2012:20) menyatakan bahwa,</p>\r\n<p>Periklanan adalah bentuk komunikasi tidak langsung, yang didasari pada</p>\r\n<p>informasi tentang keungulan, atau keunggulan suatu produk, yang disusun</p>\r\n<p>sedemikian rupa sehingga menimbulkan rasa menyenangkan yang akan mengubah</p>\r\n<p>pikiran seseorang untuk melakukan pembelian.</p>\r\n<p>Periklanan merupakan salah satu bentuk promosi yang paling banyak</p>\r\n<p>digunakan perusahaan dalam mempromosika produknya. Menurut Kustandi</p>\r\n<p>dalam Rahman, (2012:21) iklan adalah suatu proses komunikasi masa yang</p>\r\n<p>melibatkan sponsor tertentu, yang membayar jasa sebuah media massa atas</p>\r\n<p>penyiaran iklannya.</p>\r\n<p>Menurut Keegan dan Green dalam Rahman, (2012:21) iklan adalah</p>\r\n<p>sebagai pesan-pesan yang unsur seni, teks/tulisan, judul, foto-foto, tageline,</p>\r\n<p>unsur-unsur lainnya yang telah dikembangkan untuk kesesuaian mereka.</p>', 1585136942, 1585177666);

-- ----------------------------
-- Table structure for tb_paraf
-- ----------------------------
DROP TABLE IF EXISTS `tb_paraf`;
CREATE TABLE `tb_paraf`  (
  `id_paraf` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ka_koperasi` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ka_keu` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `manajer` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_paraf
-- ----------------------------
INSERT INTO `tb_paraf` VALUES ('1', 'Kepala Koperasi', 'Keuangan', 'Manajer');

-- ----------------------------
-- Table structure for tb_pinjaman
-- ----------------------------
DROP TABLE IF EXISTS `tb_pinjaman`;
CREATE TABLE `tb_pinjaman`  (
  `kd_pinj` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `kd_angg` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_pinj` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jml_pinj` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tenor` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `bunga` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jml_angs` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pinj_ke` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` int(255) NOT NULL DEFAULT 1,
  `ket_pinj` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ket_tolak` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`kd_pinj`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tb_produk
-- ----------------------------
DROP TABLE IF EXISTS `tb_produk`;
CREATE TABLE `tb_produk`  (
  `kd_prod` bigint(255) NOT NULL AUTO_INCREMENT,
  `barcode` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nm_prod` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_kat` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `harga_beli` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `harga_jual` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `diskon` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `satuan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `stok` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `keluar` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `dijual` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sisa` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `gambar` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`kd_prod`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tb_simpanan
-- ----------------------------
DROP TABLE IF EXISTS `tb_simpanan`;
CREATE TABLE `tb_simpanan`  (
  `kd_simp` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `kd_angg` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pokok` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `wajib` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sukarela` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_simp` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`kd_simp`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tb_simpanan_ajuan
-- ----------------------------
DROP TABLE IF EXISTS `tb_simpanan_ajuan`;
CREATE TABLE `tb_simpanan_ajuan`  (
  `kd_simp` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `kd_angg` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pokok` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `wajib` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sukarela` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_simp` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`kd_simp`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tb_soal
-- ----------------------------
DROP TABLE IF EXISTS `tb_soal`;
CREATE TABLE `tb_soal`  (
  `id_soal` int(11) NOT NULL AUTO_INCREMENT,
  `guru_id` int(11) NOT NULL,
  `matkul_id` int(11) NOT NULL,
  `bobot` int(11) NOT NULL,
  `file` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `tipe_file` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `soal` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `opsi_a` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `opsi_b` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `opsi_c` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `opsi_d` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `opsi_e` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `file_a` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `file_b` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `file_c` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `file_d` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `file_e` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `jawaban` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `created_on` int(11) NOT NULL,
  `updated_on` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_soal`) USING BTREE,
  INDEX `matkul_id`(`matkul_id`) USING BTREE,
  INDEX `guru_id`(`guru_id`) USING BTREE,
  CONSTRAINT `tb_soal_ibfk_1` FOREIGN KEY (`matkul_id`) REFERENCES `matkul` (`id_matkul`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `tb_soal_ibfk_2` FOREIGN KEY (`guru_id`) REFERENCES `guru` (`id_guru`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_soal
-- ----------------------------
INSERT INTO `tb_soal` VALUES (2, 1, 1, 1, '', '', '<p>Fitri : The French homework is really hard. I don’t feel like to do it.<br>Rahmat : … to help you?<br>Fitri : It sounds great. Thanks, Rahmat!</p><p><br></p><p>Which of the following offering expressions best fill the blank?</p>', '<p>Would you like me</p>', '<p>Do you mind if I</p>', '<p>Shall I</p>', '<p>Can I</p>', '<p>I will</p>', '', '', '', '', '', 'A', 1550225952, 1550225952);
INSERT INTO `tb_soal` VALUES (3, 1, 1, 1, 'd166959dabe9a81e4567dc44021ea503.jpg', 'image/jpeg', '<p>What is the picture describing?</p><p><small class=\"text-muted\">Sumber gambar: meros.jp</small></p>', '<p>The students are arguing with their lecturer.</p>', '<p>The students are watching their preacher.</p>', '<p>The teacher is angry with their students.</p>', '<p>The students are listening to their lecturer.</p>', '<p>The students detest the preacher.</p>', '', '', '', '', '', 'D', 1550226174, 1550226174);
INSERT INTO `tb_soal` VALUES (5, 3, 5, 1, '', '', '<p>(2000 x 3) : 4 x 0 = ...</p>', '<p>NULL</p>', '<p>NaN</p>', '<p>0</p>', '<p>1</p>', '<p>-1</p>', '', '', '', '', '', 'C', 1550289702, 1550289724);

-- ----------------------------
-- Table structure for tbl_akses
-- ----------------------------
DROP TABLE IF EXISTS `tbl_akses`;
CREATE TABLE `tbl_akses`  (
  `id_akses` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `ip_registered` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `token` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `key` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `datetime` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id_akses`, `email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbl_akses
-- ----------------------------
INSERT INTO `tbl_akses` VALUES ('1', '116.206.14.0', 'admin@integral.com', 'integralToken2020', '08g0kkwgos8c084wowkog0oks00k4kwskokckwcs', '1', '2020-03-24 18:47:28');
INSERT INTO `tbl_akses` VALUES ('2', '::1', 'admin@integral.com', 'integralToken2020', '8ccsswswcg8gg4oo88sw0ccgs0w8ows008og0oco', '1', '2020-03-24 18:47:27');

-- ----------------------------
-- Table structure for tbl_berita
-- ----------------------------
DROP TABLE IF EXISTS `tbl_berita`;
CREATE TABLE `tbl_berita`  (
  `id_berita` bigint(20) NOT NULL AUTO_INCREMENT,
  `judul_berita` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `isi_berita` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `image` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `target_level` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `datetime` datetime(0) NULL DEFAULT current_timestamp() ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id_berita`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbl_berita
-- ----------------------------
INSERT INTO `tbl_berita` VALUES (1, 'Nilai Lomba Baris-Berbaris Kini Bisa Dilihat Online di HP', '<p>Menonton Lomba Baris-Berbaris memang sangat mengasyikkan, apa lagi bagi yang ikut terjun langsung ke lapangan. Lelah karena latihan berbulan-bulan bisa hilang. Tapi ada satu hal yang umumnya masih menjadi kekurangan penyelenggaraan lomba baris-berbaris', NULL, NULL, '2020-07-21 06:25:15');
INSERT INTO `tbl_berita` VALUES (2, 'Pembayaran Biaya Pendidikan Melalui Bank Syariah Mandiri', '<p>Pada hari ini, Jum&rsquo;at, 8 Juni 2018, telah dilakukan penandatanganan kerjasama antara SMK PGRI 1 Tangerang yang diwakili oleh bapak Drs. H. Aep Gumiwa MM dengan Bank Syariah Mandiri yang diwakili oleh bapak Agustiono Ferdianto. Dengan kerjasama in', NULL, NULL, '2020-07-01 06:25:20');

-- ----------------------------
-- Table structure for tbl_buku
-- ----------------------------
DROP TABLE IF EXISTS `tbl_buku`;
CREATE TABLE `tbl_buku`  (
  `id_buku` int(11) NOT NULL AUTO_INCREMENT,
  `kategori_buku` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `judul_buku` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `isi_buku` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `pengarang` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tahun` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `gambar` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `file` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `bintang` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `datetime` datetime(0) NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id_buku`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbl_buku
-- ----------------------------
INSERT INTO `tbl_buku` VALUES (1, NULL, 'asdasd', '<p>sd</p>', 'asd', '123', 'vario3.jpg', NULL, '2', '2020-07-12 05:37:10');
INSERT INTO `tbl_buku` VALUES (2, 'Pelajaran', 'Buku Pelajaran Agama', NULL, 'Noname', '2020', 'guku2.jpg', '4783-10429-1-PB.pdf', NULL, '2020-08-31 11:05:50');

-- ----------------------------
-- Table structure for tbl_device
-- ----------------------------
DROP TABLE IF EXISTS `tbl_device`;
CREATE TABLE `tbl_device`  (
  `id_device` int(255) NOT NULL AUTO_INCREMENT,
  `status` enum('read','register') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT 'read',
  `device_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `on` enum('1','0') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  `datetime` datetime(0) NOT NULL DEFAULT current_timestamp() ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id_device`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbl_device
-- ----------------------------
INSERT INTO `tbl_device` VALUES (22, 'read', 'gerbang_depan', '1', '2020-03-21 14:11:19');

-- ----------------------------
-- Table structure for tbl_jenis_transaksi
-- ----------------------------
DROP TABLE IF EXISTS `tbl_jenis_transaksi`;
CREATE TABLE `tbl_jenis_transaksi`  (
  `id_jenis_transaksi` bigint(255) NOT NULL AUTO_INCREMENT,
  `jenis_transaksi` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_jenis_transaksi`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbl_jenis_transaksi
-- ----------------------------
INSERT INTO `tbl_jenis_transaksi` VALUES (1, 'Pulsa');
INSERT INTO `tbl_jenis_transaksi` VALUES (2, 'Data');
INSERT INTO `tbl_jenis_transaksi` VALUES (3, 'PLN');
INSERT INTO `tbl_jenis_transaksi` VALUES (4, 'PDAM');
INSERT INTO `tbl_jenis_transaksi` VALUES (5, 'BPJS');

-- ----------------------------
-- Table structure for tbl_kartu
-- ----------------------------
DROP TABLE IF EXISTS `tbl_kartu`;
CREATE TABLE `tbl_kartu`  (
  `id_kartu` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_device` int(10) NULL DEFAULT NULL,
  `rfid` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `siswa_id` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` int(1) NULL DEFAULT 0,
  PRIMARY KEY (`id_kartu`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tbl_kategori_buku
-- ----------------------------
DROP TABLE IF EXISTS `tbl_kategori_buku`;
CREATE TABLE `tbl_kategori_buku`  (
  `kategori_buku_id` int(11) NOT NULL,
  `nama_kategori` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`kategori_buku_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tbl_mutasi
-- ----------------------------
DROP TABLE IF EXISTS `tbl_mutasi`;
CREATE TABLE `tbl_mutasi`  (
  `id_topup` bigint(255) NOT NULL AUTO_INCREMENT,
  `user` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nominal` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jenis_transaksi` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ref_id` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` int(255) NULL DEFAULT NULL COMMENT '1=new,2=verified,3=reject',
  `datetime` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id_topup`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbl_mutasi
-- ----------------------------
INSERT INTO `tbl_mutasi` VALUES (1, 'Suharyadi.13@gmail.com', '40000', 'topup_saldo', 'ebc3cadf7dd489424090a94cfd361c21', 2, '2020-03-25 00:37:27');
INSERT INTO `tbl_mutasi` VALUES (2, 'Suharyadi.13@gmail.com', '50000', 'topup_saldo', '316d914f2665c977361e57e3f9315233', 2, '2020-03-25 00:38:02');
INSERT INTO `tbl_mutasi` VALUES (3, 'Suharyadi.13@gmail.com', '1785', 'payment', '1c64d4c59a6eda11bbc2e842d4b57242', 2, '2020-03-25 01:23:04');
INSERT INTO `tbl_mutasi` VALUES (4, 'Suharyadi.13@gmail.com', '1785', 'payment', '9187fce5391e239f48ca43c128ce7a1f', 2, '2020-03-25 01:23:32');
INSERT INTO `tbl_mutasi` VALUES (5, 'Suharyadi.13@gmail.com', '1785', 'payment', '46ca8bc28d75fb551e2b86808933a872', 2, '2020-03-25 01:23:51');
INSERT INTO `tbl_mutasi` VALUES (6, 'Suharyadi.13@gmail.com', '1785', 'payment', '5afdf1d689029c4ba67b87736a8f2d95', 2, '2020-03-25 01:24:03');
INSERT INTO `tbl_mutasi` VALUES (7, 'Suharyadi.13@gmail.com', '1785', 'payment', 'c06959edc995c5442c1bd48bd2d9283a', 2, '2020-03-25 01:24:16');
INSERT INTO `tbl_mutasi` VALUES (8, 'Suharyadi.13@gmail.com', '1785', 'payment', '6b7e908d2caaad9623001a008dba8cdd', 2, '2020-03-25 01:24:31');
INSERT INTO `tbl_mutasi` VALUES (9, 'Suharyadi.13@gmail.com', '1785', 'payment', '9a3de967b1348df9e8f3b888a386389c', 2, '2020-03-25 01:25:16');
INSERT INTO `tbl_mutasi` VALUES (10, 'Suharyadi.13@gmail.com', '1785', 'payment', '01498cd3bc5aa0848535ffcd2497f4a7', 2, '2020-03-25 01:25:28');
INSERT INTO `tbl_mutasi` VALUES (11, 'Suharyadi.13@gmail.com', '24995', 'payment', '17800ce959d0dc2bbdc8ac776c134036', 2, '2020-03-25 01:28:22');
INSERT INTO `tbl_mutasi` VALUES (12, 'Suharyadi.13@gmail.com', '40000', 'topup_saldo', '6b6ebc3c721f993825d93a767d81ba1e', 2, '2020-03-25 08:47:02');
INSERT INTO `tbl_mutasi` VALUES (13, 'admin@integral.com', '40000', 'topup_saldo', 'aacf23b246b1bef7230345ccffc9dfa0', 2, '2020-03-25 08:48:55');
INSERT INTO `tbl_mutasi` VALUES (14, 'admin@integral.com', '21210', 'payment', '06b405f0c1421f2b9185b0b54f3f59cd', 2, '2020-03-25 02:49:51');
INSERT INTO `tbl_mutasi` VALUES (15, 'Suharyadi.13@gmail.com', '40000', 'topup_saldo', '8659ae89fbd76379bda0434dd1a3512f', 1, '2020-03-25 08:14:21');
INSERT INTO `tbl_mutasi` VALUES (16, 'Suharyadi.13@gmail.com', '5000', 'topup_saldo', '5d19d3776b8a8d3bf97baa907e6c9c34', 1, '2020-03-25 08:30:34');
INSERT INTO `tbl_mutasi` VALUES (17, 'Suharyadi.13@gmail.com', '1785', 'payment', '3410b274f7b495aabe98f56f8443186f', 2, '2020-03-25 08:42:31');
INSERT INTO `tbl_mutasi` VALUES (18, 'Suharyadi.13@gmail.com', '1785', 'payment', '6af4bdd234e2fcbdf679f9abbcd8c39a', 2, '2020-03-25 11:04:43');
INSERT INTO `tbl_mutasi` VALUES (19, 'Suharyadi.13@gmail.com', '1785', 'payment', '457f2e9d06aff1b62b5628e548a6f188', 2, '2020-03-25 11:05:49');
INSERT INTO `tbl_mutasi` VALUES (20, 'Suharyadi.13@gmail.com', '25350', 'payment', '015747af08423980698e0f4df54bbfdb', 2, '2020-03-25 11:07:07');
INSERT INTO `tbl_mutasi` VALUES (21, 'Suharyadi.13@gmail.com', '3619', 'payment', 'd20705c35bcab25264883eedebb6e27c', 2, '2020-03-25 11:07:35');
INSERT INTO `tbl_mutasi` VALUES (22, 'Suharyadi.13@gmail.com', '1785', 'payment', 'ee003f3d12fb78400f6767f830f046f0', 2, '2020-03-25 11:08:26');
INSERT INTO `tbl_mutasi` VALUES (23, 'info@athos-hr.com', '40000', 'topup_saldo', 'b578b471aa20ad7e53869fd72434a4bd', 1, '2020-07-11 06:23:17');
INSERT INTO `tbl_mutasi` VALUES (24, 'admin@integral.com', '5000', 'topup_saldo', '1574514d8d758f1913250dfd0245d915', 1, '2020-08-31 05:45:29');
INSERT INTO `tbl_mutasi` VALUES (25, 'adi@gmail.com', '350000', 'topup_saldo', 'f4bacb3d7a1dfec3252410c7b01a2ddf', 2, '2024-01-30 11:58:12');

-- ----------------------------
-- Table structure for tbl_payment
-- ----------------------------
DROP TABLE IF EXISTS `tbl_payment`;
CREATE TABLE `tbl_payment`  (
  `id_payment` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `id_siswa` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jenis_payment` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tgl_payment` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nominal` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `datetime` datetime(0) NULL DEFAULT current_timestamp()
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tbl_presensi
-- ----------------------------
DROP TABLE IF EXISTS `tbl_presensi`;
CREATE TABLE `tbl_presensi`  (
  `id_presensi` bigint(255) NOT NULL AUTO_INCREMENT,
  `id_siswa` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nama_siswa` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `device_id` int(11) NULL DEFAULT NULL,
  `rfid` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `datetime` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id_presensi`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbl_presensi
-- ----------------------------
INSERT INTO `tbl_presensi` VALUES (1, '1', 'Suharyadi', 121, '0123012931230', '2020-07-06 18:38:12');
INSERT INTO `tbl_presensi` VALUES (3, '28', 'aydan aydan', NULL, NULL, '2020-07-21 06:49:55');
INSERT INTO `tbl_presensi` VALUES (4, '1', 'Admin Istrator', NULL, NULL, '2020-08-29 06:22:39');
INSERT INTO `tbl_presensi` VALUES (5, '1', 'Admin Istrator', NULL, NULL, '2020-08-31 10:17:24');
INSERT INTO `tbl_presensi` VALUES (6, '3', 'siswa siswa', NULL, NULL, '2020-08-31 11:07:15');
INSERT INTO `tbl_presensi` VALUES (7, '1', 'Admin Istrator', NULL, NULL, '2020-09-07 07:48:51');
INSERT INTO `tbl_presensi` VALUES (8, '1', 'Admin Istrator', NULL, NULL, '2020-09-13 05:13:09');
INSERT INTO `tbl_presensi` VALUES (9, '1', 'Admin Istrator', NULL, NULL, '2020-09-16 05:34:43');
INSERT INTO `tbl_presensi` VALUES (10, '1', 'Admin Istrator', NULL, NULL, '2020-09-27 03:56:43');
INSERT INTO `tbl_presensi` VALUES (11, '1', 'Admin Istrator', NULL, NULL, '2020-12-15 05:09:16');
INSERT INTO `tbl_presensi` VALUES (12, '29', 'adi only', NULL, NULL, '2024-01-30 11:52:44');
INSERT INTO `tbl_presensi` VALUES (13, '27', 'Sais School', NULL, NULL, '2024-01-30 11:57:11');

-- ----------------------------
-- Table structure for tbl_saldo
-- ----------------------------
DROP TABLE IF EXISTS `tbl_saldo`;
CREATE TABLE `tbl_saldo`  (
  `id_saldo` bigint(255) NOT NULL AUTO_INCREMENT,
  `id_user` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `saldo` double(255, 0) NOT NULL,
  `datetime` datetime(0) NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id_saldo`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbl_saldo
-- ----------------------------
INSERT INTO `tbl_saldo` VALUES (1, 'Suharyadi.13@gmail.com', 173166, '2020-03-24 18:30:32');
INSERT INTO `tbl_saldo` VALUES (4, 'admin@integral.com', 61210, '2020-03-25 02:48:55');
INSERT INTO `tbl_saldo` VALUES (5, 'adi@gmail.com', 350000, '2024-01-30 05:58:12');

-- ----------------------------
-- Table structure for tbl_sekolah
-- ----------------------------
DROP TABLE IF EXISTS `tbl_sekolah`;
CREATE TABLE `tbl_sekolah`  (
  `id_sekolah` bigint(255) NULL DEFAULT NULL,
  `nama_sekolah` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kode_sekolah` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` int(2) NULL DEFAULT NULL,
  `tgl_register` datetime(0) NULL DEFAULT current_timestamp(),
  `datetime` datetime(0) NULL DEFAULT current_timestamp() ON UPDATE CURRENT_TIMESTAMP(0)
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbl_sekolah
-- ----------------------------
INSERT INTO `tbl_sekolah` VALUES (1, 'IN SCHOOL', 'INT001', 1, '2020-06-11 19:11:58', '2020-06-11 19:15:54');
INSERT INTO `tbl_sekolah` VALUES (2, 'Sais School', 'INT002', 1, '2020-06-11 19:16:06', '2020-06-11 19:16:06');

-- ----------------------------
-- Table structure for tbl_transaksi
-- ----------------------------
DROP TABLE IF EXISTS `tbl_transaksi`;
CREATE TABLE `tbl_transaksi`  (
  `id_transaksi` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_user` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `noRef` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `amount` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nominal` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `payment_type` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `payment_description` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sig` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_transaksi`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tbl_trx
-- ----------------------------
DROP TABLE IF EXISTS `tbl_trx`;
CREATE TABLE `tbl_trx`  (
  `id_trx` bigint(255) NOT NULL AUTO_INCREMENT,
  `user` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ref_id` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `customer_no` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `buyer_sku_code` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `message` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `status` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `rc` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sn` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `buyer_last_saldo` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `price` double(200, 0) NULL DEFAULT NULL,
  `price_user` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `datetime` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id_trx`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbl_trx
-- ----------------------------
INSERT INTO `tbl_trx` VALUES (1, 'Suharyadi.13@gmail.com', '357b4ac394ad8b5c081d828ccd58995a', '087800001233', 'xld10', 'Transaksi Pending', 'Sukses', '03', '', '990000', 10000, '1785', '2020-03-25 06:40:31');
INSERT INTO `tbl_trx` VALUES (2, 'Suharyadi.13@gmail.com', '3410b274f7b495aabe98f56f8443186f', '08976156259', 'tri1', '', 'Sukses', '', '', '990000', 10000, '1785', '2020-03-25 16:32:12');
INSERT INTO `tbl_trx` VALUES (3, 'Suharyadi.13@gmail.com', '6af4bdd234e2fcbdf679f9abbcd8c39a', '08976156259', 'tri1', '', 'Sukses', '', '', '990000', 10000, '1785', '2020-03-25 17:04:51');
INSERT INTO `tbl_trx` VALUES (4, 'Suharyadi.13@gmail.com', '457f2e9d06aff1b62b5628e548a6f188', '08976156259', 'tri1', '', 'Sukses', '', '', '990000', 10000, '1785', '2020-03-25 17:06:07');
INSERT INTO `tbl_trx` VALUES (5, 'Suharyadi.13@gmail.com', '015747af08423980698e0f4df54bbfdb', '087800001230', 'xl25', 'Transaksi Sukses', 'Sukses', '00', '1234567890', '990000', 10000, '25350', '2020-03-25 11:07:07');
INSERT INTO `tbl_trx` VALUES (6, 'Suharyadi.13@gmail.com', 'd20705c35bcab25264883eedebb6e27c', '087800001234', 'tri2', 'Transaksi Pending', 'Sukses', '03', '', '990000', 10000, '3619', '2020-03-25 17:07:42');
INSERT INTO `tbl_trx` VALUES (7, 'Suharyadi.13@gmail.com', 'ee003f3d12fb78400f6767f830f046f0', '087800001234', 'tri1', 'Transaksi Pending', 'Pending', '03', '', '990000', 10000, '1785', '2020-03-25 11:08:26');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `username` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `email` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `activation_selector` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `activation_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `forgotten_password_selector` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `forgotten_password_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED NULL DEFAULT NULL,
  `remember_selector` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `remember_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED NULL DEFAULT NULL,
  `active` tinyint(1) UNSIGNED NULL DEFAULT NULL,
  `first_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `last_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `company` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `salt` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `photo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `kode_sekolah` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uc_activation_selector`(`activation_selector`) USING BTREE,
  UNIQUE INDEX `uc_forgotten_password_selector`(`forgotten_password_selector`) USING BTREE,
  UNIQUE INDEX `uc_remember_selector`(`remember_selector`) USING BTREE,
  UNIQUE INDEX `uc_email`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 30 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, '127.0.0.1', 'Administrator', '$2y$10$k3BWfvpny9yqTp6PwYE5bueewHKl98Clqqd4eqeG6/t6M/B3JTZv.', 'admin@integral.com', NULL, '', '2fc408949a1db9bb74a2', '$2y$10$xkF5kuC95RYRWHfUZXNib.DK/q4lIKks2MkEv80Xf4Slzy.LVpr3i', 1706590241, NULL, NULL, 1268889823, 1607983756, 1, 'Admin', 'Istrator', 'ADMIN', '0', NULL, NULL, 'INT001');
INSERT INTO `users` VALUES (3, '::1', '12183018', '$2y$10$k3BWfvpny9yqTp6PwYE5bueewHKl98Clqqd4eqeG6/t6M/B3JTZv.', 'siswa@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1550225511, 1598846835, 1, 'siswa', 'siswa', NULL, NULL, NULL, NULL, 'INT001');
INSERT INTO `users` VALUES (4, '::1', '12345678', '$2y$10$k3BWfvpny9yqTp6PwYE5bueewHKl98Clqqd4eqeG6/t6M/B3JTZv.', 'guru1@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1550226286, 1594257173, 1, 'Koro', 'Sensei', NULL, NULL, NULL, NULL, 'INT001');
INSERT INTO `users` VALUES (8, '::1', '01234567', '$2y$10$k3BWfvpny9yqTp6PwYE5bueewHKl98Clqqd4eqeG6/t6M/B3JTZv.', 'tobirama@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1550289356, 1583373711, 1, 'Tobirama', 'Sensei', NULL, NULL, NULL, NULL, 'INT001');
INSERT INTO `users` VALUES (21, '::1', '123456789', '$2y$10$k3BWfvpny9yqTp6PwYE5bueewHKl98Clqqd4eqeG6/t6M/B3JTZv.', 'Suharyadi.13@gmail.com', '233f23b153608f56726f', '$2y$10$zHEHcGr4F1mtpuvYaqmucuERqrA2Xu1r0GKRc4G/dRY.mewX0VypC', NULL, NULL, NULL, NULL, NULL, 1585050668, 1585178163, 1, 'Suhar', 'yadi', NULL, NULL, NULL, NULL, 'INT001');
INSERT INTO `users` VALUES (22, '::1', '13123123', '$2y$10$k3BWfvpny9yqTp6PwYE5bueewHKl98Clqqd4eqeG6/t6M/B3JTZv.', 'info@gemss-hr.com', 'a6196273045c9c933c13', '$2y$10$UYH6Vo8MGBumTH7fCQ3enunkmEi3dFkjY2zsxOfDeK4e.Ct8hWLk2', NULL, NULL, NULL, NULL, NULL, 1585274064, NULL, 0, 'Agus', 'Fajar', NULL, NULL, NULL, NULL, 'INT001');
INSERT INTO `users` VALUES (23, '::1', 'adminkkj', '$2y$10$k3BWfvpny9yqTp6PwYE5bueewHKl98Clqqd4eqeG6/t6M/B3JTZv.', 'spider_bit13@yahoo.com', '05cfe470b41be895e710', '$2y$10$L3HicIyn0WmkuliS5V2lSeE2POjgdiecDSeUvzxGc/BiOypcB6ipO', NULL, NULL, NULL, NULL, NULL, 1585274118, NULL, 0, 'Suhar', 'School', NULL, NULL, NULL, NULL, 'INT001');
INSERT INTO `users` VALUES (24, '::1', '14123123123', '$2y$10$k3BWfvpny9yqTp6PwYE5bueewHKl98Clqqd4eqeG6/t6M/B3JTZv.', 'Suharyadi.18@gmail.com', '6ec1d324240eb7cbb9de', '$2y$10$rGedzxWgE/8yzOtzWydPROXIi38m4/BBHk0AblA24KFeHEu3y3CzO', NULL, NULL, NULL, NULL, NULL, 1585274238, NULL, 0, 'Suhar', 'yadi', NULL, NULL, NULL, NULL, 'INT001');
INSERT INTO `users` VALUES (25, '::1', '0001', '$2y$10$k3BWfvpny9yqTp6PwYE5bueewHKl98Clqqd4eqeG6/t6M/B3JTZv.', 'info@athos-hr.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1594435803, 1594435855, 1, 'Thomas', 'Fleming', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `users` VALUES (27, '::1', '12345699', '$2y$12$qeoHtEqwhMnCYhRZI7dNnu2fTYIbTDZXuYjOPSiW1vWr8ok11HzKu', 'Admin@saisschool.sch.id', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1595288552, 1706590659, 1, 'Sais', 'School', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `users` VALUES (28, '::1', '1234567777', '$2y$10$k3BWfvpny9yqTp6PwYE5bueewHKl98Clqqd4eqeG6/t6M/B3JTZv.', 'aydan@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1595288986, 1595288995, 1, 'aydan', 'aydan', NULL, NULL, NULL, NULL, 'INT001');
INSERT INTO `users` VALUES (29, '::1', '123456', '$2y$10$k3BWfvpny9yqTp6PwYE5bueewHKl98Clqqd4eqeG6/t6M/B3JTZv.', 'adi@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1706590348, 1706590957, 1, 'adi', 'only', NULL, NULL, NULL, NULL, 'INT001');

-- ----------------------------
-- Table structure for users_groups
-- ----------------------------
DROP TABLE IF EXISTS `users_groups`;
CREATE TABLE `users_groups`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uc_users_groups`(`user_id`, `group_id`) USING BTREE,
  INDEX `fk_users_groups_users1_idx`(`user_id`) USING BTREE,
  INDEX `fk_users_groups_groups1_idx`(`group_id`) USING BTREE,
  CONSTRAINT `users_groups_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `users_groups_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE = InnoDB AUTO_INCREMENT = 32 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users_groups
-- ----------------------------
INSERT INTO `users_groups` VALUES (3, 1, 1);
INSERT INTO `users_groups` VALUES (5, 3, 3);
INSERT INTO `users_groups` VALUES (6, 4, 2);
INSERT INTO `users_groups` VALUES (23, 21, 3);
INSERT INTO `users_groups` VALUES (24, 22, 3);
INSERT INTO `users_groups` VALUES (25, 23, 3);
INSERT INTO `users_groups` VALUES (26, 24, 3);
INSERT INTO `users_groups` VALUES (27, 25, 3);
INSERT INTO `users_groups` VALUES (29, 27, 1);
INSERT INTO `users_groups` VALUES (30, 28, 3);
INSERT INTO `users_groups` VALUES (31, 29, 3);

SET FOREIGN_KEY_CHECKS = 1;
