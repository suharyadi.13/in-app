<div class="row "> 
	<div class="col-12 col-xl-8 offset-xl-2 mt-6 mb-2">
		<div class="card">
		<div class="card-header">
			<div class="row"> 
				<div class="col-6 col-md-4 text-left">
					<smal class=""><i class="fa fa-money"></i>  Saldo</smal>
					<h4>Rp. <?php echo (is_numeric($saldo)? number_format($saldo):$saldo); ?> </h4>
				</div>
				<div class="col-12  col-md-4  d-none d-md-block d-lg-block text-center "> 
		            <h4>Pilih Layanan</h4>
		        </div>
				<div class="col-6 col-md-4  text-right "> 
					 <a href="<?php echo $this->config->item('parent_module') ?>">
		             <i class="fa fa-home "></i>Home
		            </a> 
		        </div>
		        <div class="col-12 d-md-none  col-md-4 text-center ">   
		            <h4>Pilih Layanan</h4>
		        </div>
			</div>
		</div>
		<div class="card-body">	 
			<div class="row">
			<?php

			foreach ($layanan as $key => $value) {
			 	if($value['url']==""){
			 		$link = "javascript:popUp('Informasi','Maaf, Modul Sedang dalam perbaikan')";	
			 	}else{
			 		$link = $value['url'];	
			 	}
			?>	
			<div class="col-4 col-xl-2">
				<div class="">
					<div class=" " style="">
					<a href="<?php echo $link  ?>" class="text-white">
					  <img src="<?php echo base_url('assets/img/'.$value['image']) ?>" class="card-img-top" alt="<?php echo $key ?>">
					  <div class="text-center">
					    <div class="text-dark small"><?php echo $key?></div> 
					  </div>
					</a>
					</div>
				</div>
			</div>
			<?php
			} 
			?>
		   </div> 
		</div>
		</div>
	</div>
	<div class="col-12 col-xl-8 offset-xl-2">
		<div class="card">
		<div class="card-header">
			<div class="row">
				<div class="col-6">Data Transaksi</div> 
			</div>
		</div>
		<div class="card-body table-responsive">
			<table class="table table-sm">
				<thead>
					<tr>
						<td>No </td>
						<td>SKU</td>
						<td>Tujuan </td>
						<td>Nominal </td>
						<td>Status </td>
						<td></td>
					</tr>
				</thead>
				<tbody>
					<?php 
						$x=1;
						foreach ($pendingData as $key => $value) {
							$dataInq = array(
									'sku'=>$value['buyer_sku_code'],
									'cust'=>$value['customer_no'],
									'refId'=>$value['ref_id'],
								);
							$dataInq = base64_encode(json_encode($dataInq));
							if($value['status']=='Sukses'){
								$class = "bg-success text-white";
							}else{
								$class = "bg-warning";
							} 
							echo '
							<tr>
							<td>'.$x.'</td>
							<td>'.$value['buyer_sku_code'].'</td>
							<td>'.$value['customer_no'].'</td>
							<td>'.number_format($value['price_user']).'</td>
							<td class="'.$class.' ">'.$value['status'].'</td> 
							<td >  ';
							if($value['status']=="Pending"){
							echo '<a  href="'.site_url('ppob/PostInquiry?data='.$dataInq).'" class="btn btn-success btn-sm">Reload</a>';

							echo '<a target="_blank" href="'.site_url('ppob/chat/?no='.$this->config->item('PPOB_CS').'&message='.urlencode("mohon dicek untuk trx berikut ".$value['ref_id'])).'" class="btn btn-primary  btn-sm">Chat</a>';
							}
							echo '</td>
							</tr>
							';
							$x++;
						}	
					?>
					
				</tbody>
			</table>
		</div>
		</div>
	</div>
</div>