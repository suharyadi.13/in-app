<?php
function LoadData($Category,$brand,$itemProduk){
if(is_array($itemProduk)){
	foreach ($itemProduk as $key => $value) {
	 if($brand ==$value->brand ){
		 if($value->price <=3000){	
		 	$price = $value->price+700;
		 }else if($value->price <=5000){	
		 	$price = $value->price+500;
		 }else if($value->price <=15000){	
		 	$price = $value->price+600;
		 }else if($value->price <= 25000){
		 	$price = $value->price+700;
		 }else if($value->price <= 35000){
		 	$price = $value->price+800;
		 }else if($value->price <= 45000){
		 	$price = $value->price+900;
		 }else if($value->price <= 65000){
		 	$price = $value->price+700;
		 }else if($value->price <= 75000){
		 	$price = $value->price+100;
		 }else{
		 	$price = $value->price+2000;
		 }
		 //$price = 130000;
	?>	
	<div class="col-4 col-xl-2">
		<div class="">
			<div id="<?php echo $value->buyer_sku_code ?>" data-toggle="tooltip" data-placement="top" title="<?php echo $value->desc ?>">
			<a href="#" onClick="buy('<?php echo $brand ?>','<?php echo $value->buyer_sku_code ?>','<?php echo $value->category ?>','<?php echo $price ?>')" class="text-white">
			  <img src="<?php echo base_url('assets/img/'.strtolower($Category).'.png') ?>" class="card-img-top" alt="<?php echo $value->product_name ?>">
			  <div class="text-center">
			    <div class="text-dark small" >
			    	<?php echo $value->product_name ?><br>
			    	<span class="medium">(<?php echo "Rp.".number_format($price)?>)</span>
			    </div> 

			  </div>
			</a>
			</div>
		</div>
	</div>
	<?php
	}
	} 
}
}

?>

<div class="row ">
	<div class="col-12 col-xl-8 offset-xl-2 mt-6">
		<div class="card">
		<div class="card-header">
			<div class="row">
				<div class="col-6">Pilih Produk : <strong><?php echo $Category ?></strong></div>
				<div class="col-6 text-right"><a href="<?php echo site_url('ppob') ?>" class="btn btn-default">Kembali</a>
				</div>
			</div>
		</div>
		<div class="card-body">	 
			<div class="row">
			<div class="col-12 bg-warning p-3" id="message" >
			<?php 
			if($this->session->flashdata('message')!=""){
				echo $this->session->flashdata('message');
			}
			?>
			</div>
			<div class="col-12 mb-5"> 
				<form method="post" action="<?php echo site_url('ppob/beli_pulsa') ?>" id="formBelipulsa">
					Masukan Nomor : <input type="text" name="no_tujuan" id="no_tujuan" class="form-control" >
					<input type="hidden" value="" name="sku" id="sku">
					<input type="hidden" value="" name="priceUser" id="priceUser">
					<input type="hidden" value="<?php echo $Category ?>" name="category" id="category">
				</form>
			</div>
			<div class="col-12">
				<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
				<?php
				if(is_array($itemProduk)){
					$brand = array();
					foreach ($itemProduk as $key => $value) {
						if(!in_array($value->brand, $brand)){
							$brand[] = $value->brand;
						}
					}
					 
					$x=0;
					foreach ($brand as $key => $branditem) {
						if($x==0){
							$active = "active";
						}else{
							$active = "";
						} 
						echo '
						  <li class="nav-item ">
						    <a class="nav-link '.$active.'" id="pills-'.str_replace(" ","_",$branditem) .'-tab" data-toggle="pill" href="#pills-'.str_replace(" ","_",$branditem) .'" role="tab" aria-controls="pills-'.str_replace(" ","_",$branditem) .'" aria-selected="true">'.$branditem.'</a>
						  </li>';
						$x++;
					}
				?>
				</ul>
				<div class="tab-content" id="pills-tabContent">
					<?php
					$x=0;
					foreach ($brand as $key => $branditem) { 
						if($x==0){
							$active = "active";
						}else{
							$active = "";
						} 
						echo '
					  	<div class="tab-pane fade show '.$active .'" id="pills-'.str_replace(" ","_",$branditem).'" role="tabpanel" aria-labelledby="pills-'.str_replace(" ","_",$branditem).'-tab">
					  	<div class="row">
					  	';
					  	LoadData($Category,$branditem,$itemProduk);
					  	echo '</div></div>'; 
						$x++;
					}
					?>
				</div>
				<?php
				}
				?>
			</div>
			 
		   </div> 
		</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$("#no_tujuan").keyup(function(){
			//validateNo($("#no_tujuan").val());
		})

		function validateNo(noTujuan){
			/*var noTelp = noTujuan.substring(1, 4);
			if(noTelp =="" || $notelp.length < 4){
				$("#message").html("periksa kembali Nomor yang anda masukan");
			}else if(noTelp =="0899" || noTelp =="0897" || noTelp =="0897"){
				$("#message").html("Nomor anda 3 (Three)");
			}else if(noTelp =="0877" || noTelp =="0817" ){
				$("#message").html("Nomor anda XL");
			}else if(noTelp =="0812" || noTelp =="0822"  ){
				$("#message").html("Nomor anda Telkomsel");
			}*/
		}
	})
	function buy(brand,sku,cat,price){
		var noTujuan = $("#no_tujuan").val();
		var desc = $("#"+sku).attr('title');
		var message= 'Anda yakin akan membeli '+brand+'  '+cat+' ( '+desc+') ?';
		Swal({
			title: 'Konfirmasi',
			text: message,
			showCancelButton: true,
			confirmButtonColor: '#3085d6', 
			confirmButtonText: 'Ya',
		  	cancelButtonText: 'Batal!',
		}).then((result) => {
			if (result.value) {
			   if(noTujuan==""){
					$("#message").removeClass("bg-success");
					$("#message").addClass("bg-warning"); 
					$("#message").html("no tujuan tidak boleh kosong");
					Swal({
						title: 'Peringatan',
						text: 'No Tujuan tidak boleh kosong',
						showCancelButton: false,
						confirmButtonColor: '#ffc107', 
						confirmButtonText: 'Ok',
					})
				}else{
					$("#sku").val(sku)
					$("#priceUser").val(price)
					$("#formBelipulsa").submit();
				}
			}  
		});
	}
	
</script>