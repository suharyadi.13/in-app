<?php
$this->load->view('auth/header');


?>
  
<div class="container bg-white col-md-6 col-xl-4  offset-xl-4 p-4">   
    <div class="row">
        <div class="col-md-12">
            <!-- /.login-logo -->
            <div class="login-box-body">
                <h3 class="text-center">
                    
                <img src="<?php echo base_url() ?>/assets/img/logo.png" style="width:200px;height:auto">
                 </h3> 
                <?php
                echo ($this->session->flashdata('message')!="") ? $this->session->flashdata('message') : "";
                ?>
                <h4 class="text-center"><?php echo $title ?></h4>
                <?php
                echo form_open();
                echo form_label('First name:','first_name').'<br />';
                echo form_error('first_name');
                echo form_input('first_name',set_value('first_name'),'class="form-control" required').'<br />';
                echo form_label('Last name:','last_name').'<br />';
                echo form_error('last_name');
                echo form_input('last_name',set_value('last_name'),'class="form-control" required').'<br />';
                echo form_label('No ID Pengenal (KTP/SIM/Passport):','ktp').'<br />';
                echo form_error('ktp');
                echo form_input('ktp',set_value('ktp'),'class="form-control" required').'<br />';
                echo form_label('No telp:','no_telp').'<br />';
                echo form_error('no_telp');
                echo form_input('no_telp',set_value('no_telp'),'class="form-control" required').'<br />';
                echo form_label('Email:','email').'<br />';
                echo form_error('email');
                echo form_input('email',set_value('email'),'class="form-control" required').'<br />';
                echo form_label('Password:', 'password').'<br />';
                echo form_error('password');
                echo form_password('password','','class="form-control" required').'<br />';
                echo form_label('Confirm password:', 'confirm_password').'<br />';
                echo form_error('confirm_password');
                echo form_password('confirm_password','','class="form-control" required').'<br /><br />';
                echo form_submit('register','Register','class="btn btn-success"');
                echo anchor(site_url('auth'),'Kembali','class="btn btn-primary float-right"');
                echo form_close();
                ?>
            </div>
        </div>
    </div>
</div>
<?php
$this->load->view('auth/footer');
?>
  