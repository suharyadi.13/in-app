
<!DOCTYPE html>
<html>
  <head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <title><?php echo $title; ?></title>
  <link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon.ico') ?>" type="image/x-icon">
  <link rel="icon" href="<?php echo base_url('assets/img/favicon.ico') ?>" type="image/x-icon">
  <!-- Main CSS-->
   <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/font-awesome/css/font-awesome.min.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/css/AdminLTE.css');?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/iCheck/square/blue.css');?>">
  </head>
  <body style="background:url(https://www.harmony.co.id/wp-content/uploads/2019/10/Membuat-Laporan-Keuangan-Manual-VS-Aplikasi.jpg) #ffffff; background-size:cover;background-repeat:no-repeat;" class="hold-transition skin-green login-page" onload="document.login.identity.focus();">