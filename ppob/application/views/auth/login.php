<?php
$this->load->view('components/head_view');
?>
  
<div class="container bg-white col-md-6 col-xl-4  offset-xl-4 p-4">   
 
	<div class="row">
		<div class="col-md-12 ">
			<!-- /.login-logo -->
			<div class="login-box-body">
			<h3 class="text-center mt-0 mb-4">
				<img src="<?php echo base_url() ?>/assets/img/logo.png" style="width:200px;height:auto">
			</h3> 
			<p class="login-box-msg text-center" style="font-size: 25px"><?php echo $this->config->item('company_name') ?></p>

			<div id="infoMessage" class="text-center"><?php echo $message;?></div>

			<?= form_open("auth/login", array('id'=>'login'));?>
				<div class="input-group mb-3">
				  <?= form_input($identity,"","class='form-control'");?>
				  <div class="input-group-append">
				    <span class="input-group-text"><i class="fa fa-envelope "> </i></span>
				  </div>
				</div>
				<div class="input-group mb-3">
				  <?= form_input($password,"","class='form-control'");?>
				  <div class="input-group-append">
				    <span class="input-group-text"><i class="fa fa-key "> </i></span>
				  </div>
				</div>
				<div class="row">
					<div class="col-6">
					<div class="checkbox icheck">
						<label>
						<?= form_checkbox('remember', '', FALSE, 'id="remember"');?> Remember Me
						</label>
					</div>
					</div> 
					<div class="col-6">
						<!-- /.col -->
						<div class="col-xs-4 float-right">
						<?= form_submit('submit', lang('login_submit_btn'), array('id'=>'submit','class'=>'btn btn-primary btn-block btn-flat'));?>
						</div>
						<!-- /.col -->
					</div>
				</div>
				<?= form_close(); ?>

				<a href="<?=base_url()?>auth/forgot_password" class="text-center"><?= lang('login_forgot_password');?></a>
				<!-- <a class="float-right" href="<?php echo site_url('register'); ?>">Klik Untuk Daftar ? </a> -->

			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	let base_url = '<?=base_url();?>';
</script>
<script src="<?=base_url()?>assets/js/login.js"></script>
<?php
$this->load->view('components/footer_view');
?>
