<?php
$this->load->view('auth/header');
?>
  
<div class="container bg-white col-md-6 col-xl-4  offset-xl-4 p-4">   
    <div class="row">
        <div class="col-md-12">
            <!-- /.login-logo -->
            <div class="login-box-body">
                <h3 class="text-center">
                    
                <img src="<?php echo base_url() ?>/assets/img/logo.png" style="width:200px;height:auto">
                 </h3> 
                <?php
                echo ($this->session->flashdata('message')!="") ? $this->session->flashdata('message') : "";
                ?>
                <h4 class="text-center"><?php echo $title ?></h4>
                <?php
                echo form_open();
                echo form_hidden('username',$username);
                echo form_hidden('email',$email);
                echo form_hidden('password',$password);
                echo form_submit('register','Lanjutkan','class="btn btn-success"');
                echo anchor($this->config->item('parent_module'),'Kembali','class="btn btn-primary float-right"');
                echo form_close();
                ?>
            </div>
        </div>
    </div>
</div>
<?php
$this->load->view('auth/footer');
?>
  