<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * Keys Controller
 * This is a basic Key Management REST controller to make and delete keys
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Wallet extends REST_Controller {

    protected $methods = [
            'index_put' => ['level' => 10, 'limit' => 10],
            'index_delete' => ['level' => 10],
            'level_post' => ['level' => 10],
            'regenerate_post' => ['level' => 10],
        ];
    private $klienID ="001203";
    private $secret = "8ccsswswcg8gg4oo88sw0ccgs0w8ows008og0oco"; 
    private $token = '$2a$08$RhnY4OennrqswNljoJJ.4ei/AS3TW1OldoXbGUaB3oFnSidveks6S';

    
    function __construct(){
        // Construct the parent class
        parent::__construct();
        //load the models 
        $this->load->model('custom_model');
        $this->load->library(array('session','curl','Custom_library','Incrypt','Bcrypt','ion_auth'));  
        $this->output->set_header('Access-Control-Allow-Origin: *');
    }
    public function ceksaldo_post()
    {
        if($this->_validateAkses()){ 
            $this->custom_model->table = "users";
            $condition = array('email'=>$this->post('user')) ;
            $user = $this->custom_model->get_all($condition)->row_array();
            //var_dump($user);var_dump($this->post('user'));die;
            $data = $this->db->select('*')->from('tbl_saldo')->where('id_user',$user['id'])
                    ->get()->row_array();



            $this->response([
                'response' => TRUE,
                'message'=>"Load Saldo berhasil",
                'data' => $this->_encodeData($data,$this->klienID,$this->secret)
                ], REST_Controller::HTTP_CREATED); 
        }

        
    }
    public function topup_post()
    {
        $this->_validateAkses();
        $data['message']="";


        $this->response([
            'response' => FALSE,
            'message'=>"",
            'data' => $data
            ], REST_Controller::HTTP_CREATED); 

        
    }
    public function payment_post()
    {
        $this->_validateAkses();
        $data['message']="";


        $this->response([
            'response' => FALSE,
            'message'=>"",
            'data' => $data
            ], REST_Controller::HTTP_CREATED); 

        
    }
    public function transfer_post()
    {
        $this->_validateAkses();
        $data['message']="";


        $this->response([
            'response' => FALSE,
            'message'=>"",
            'data' => $data
            ], REST_Controller::HTTP_CREATED); 
    }

    function _validateAkses(){   

        $param = json_decode(base64_encode($this->post('param')));
       
        $datDecrypt = $this->_decodeData($this->post('data'),);
         

        $data = array(
            'ip_registered'=>$_SERVER['REMOTE_ADDR'], 
            'token'=>$datDecrypt['token'],
            'user'=>$this->post('user'),
        );
        //var_dump($data);die;
        if($data['ip_registered']=="" || $data['token']=="" || $data['user']==""){
            $this->response([
                'response' => FALSE,
                'message'=>"Format Data yang dikirim tidak lengkap",
                'data' => $data
                ], REST_Controller::HTTP_FORBIDDEN); 

        }else{
            $this->custom_model->table = 'tbl_akses';
            $condition = array(
                "ip_registered"=>$data['ip_registered'], 
                "status"=>"1",
            );
            
            $cek = $this->custom_model->get_all($condition);

            if($cek->num_rows()>0){
                $dataAkses = $cek->row_array();
                //var_dump($dataAkses);die;
                if ($this->bcrypt->check_password($dataAkses['token'], $data['token']))
                {
                    return true;
                }else{
                    $this->response([
                        'response' => FALSE,
                        'message'=>"Akses anda ditolak,token tidak dikenali",
                        'data' => $data
                        ], REST_Controller::HTTP_FORBIDDEN); 
                    }
            }else{
                $this->response([
                'response' => FALSE,
                'message'=>"Akses anda ditolak",
                'data' => $data
                ], REST_Controller::HTTP_FORBIDDEN); 
            }
        }
        

    }

    function requestToken_post(){
       /* $reqToken = array("token"=>"token2020","user"=>"admin@admin.com","secret"=>"123456789");
        
        var_dump(base64_encode(json_encode($reqToken)));die;*/
        var_dump($this->post());die;
        $this->custom_model->table = 'tbl_akses';
        $condition = array(
            "ip_registered"=>$_SERVER['REMOTE_ADDR'],
            "email"=>$this->post('user'), 
            "status"=>"1",
        );
         
        
        $cek = $this->custom_model->get_all($condition);

        if($cek->num_rows()>0){
            $dataPost =(array) json_decode(base64_decode($this->post('data')));
            //var_dump($dataPost);die;
            if(isset($dataPost['secret'])){
                $token = $this->getToken($dataPost['token']);
                $data = array(
                    'token' => $token,
                    'ip_address'=>$_SERVER['REMOTE_ADDR']
                );
                $this->response([
                'response' => TRUE,
                'message'=>"",
                'data' => $data
                ], REST_Controller::HTTP_CREATED); 
            }else{
                $this->response([
                    'response' => FALSE,
                    'message'=>"mohon masukan secret code anda",
                    'data' => $dataPost
                    ], REST_Controller::HTTP_FORBIDDEN); 
            }
        }else{
                $this->response([
                    'response' => FALSE,
                    'message'=>"Akses sever anda ditolak",
                    'data' => ""    
                    ], REST_Controller::HTTP_FORBIDDEN); 
            }
    }
    function getToken($token){ 
       return  $this->bcrypt->hash_password($token);
    }


    function _decodeData($data,$klienID="",$secret=""){ 
        $decoded = $this->incrypt->decrypt($data,$this->klienID,$this->secret); 
         
        return $decoded;
    }

    function _encodeData($data,$klienID="",$secret=""){ 
        return $this->incrypt->encrypt($data,$this->klienID,$this->secret); 
    }


}
