<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ppob extends CI_Controller {
	
	public function __construct(){
		parent::__construct(); 
		$this->load->database();
        $this->load->library(array('form_validation','custom_library','Incrypt'));
        $this->load->helper(array('url', 'language')); 
        
		if (!$this->ion_auth->logged_in()){
			redirect('auth');
		}
		
		$this->load->model('Dashboard_model', 'dashboard');
		$this->user = $this->ion_auth->user()->row();
	}

	function index(){

		$user = $this->user;
		$data = [
			'user' 		=> $user,
			'judul'		=> 'PPOB',
			'subjudul'	=> 'Data Aplikasi',
		];
		$data['sendData'] = urlencode(base64_encode(json_encode($data)));
		$data['saldo'] = $this->custom_library->cekSaldo($user->email);
		$data['sub_view'] = "list";
		$data['layanan'] = array(
			"Pulsa"=>array("image"=>"pulsa.png","url"=>site_url('ppob/Dataitem/Pulsa')),
			"Paket Data"=>array("image"=>"data.png","url"=>site_url('ppob/Dataitem/Data')),
			"PLN"=>array("image"=>"pln.png","url"=>site_url('ppob/Dataitem/PLN')),
			"PDAM"=>array("image"=>"pdam.png","url"=>site_url('ppob/Dataitem/PDAM')),
			"Games"=>array("image"=>"bpjs.png","url"=>site_url('ppob/Dataitem/Games')),
			"E-money"=>array("image"=>"E-Money.png","url"=>site_url('ppob/Dataitem/E-Money')),
		);
		$data['pendingData'] = $this->db->select('*')
							->from('tbl_trx')
							->where('user',$this->user->email)
							->limit(20,0)
							->order_by('datetime','DESC')->get()
							->result_array();
		$this->load->view('ppob/home',$data);
	}
	function Dataitem($category="Pulsa"){
		$user = $this->user;
		$dataAPI = array(
			"cmd"=>"prepaid",
		    "username"=> $this->config->item('PPOB_USER'),
		    "sign"=> $this->custom_library->createSign('pricelist')
		);
		$data['sub_view'] = "list_item"; 
		$data['Category'] = $category;
		
		$data['itemProduk'] = $this->LoadDataItem($dataAPI,$category);
		
		 
		
		$this->load->view('ppob/home',$data);
	}
	function beli_pulsa(){
		$user = $this->user;
		$category = $this->input->post('category');
		$priceUser = $this->input->post('priceUser');
		$time = date("Y-m-d- h:i:s");
		$refId = md5($this->input->post('no_tujuan')."".$time ); 
		$saldo = (float)$this->custom_library->cekSaldo($user->email);
		if($saldo >=$priceUser){
			//post data payment ke wallet
			
				$dataAPI = array( 
				    "username"=>$this->config->item('PPOB_USER'), 
				    "buyer_sku_code"=> $this->input->post('sku'),
				    "customer_no"=>$this->input->post('no_tujuan'),  
				    "ref_id"=>$refId,
				    "sign"=>$this->custom_library->createSign($refId)
				);

				$data['responData'] = $this->custom_library->loadppobAPI('transaction',$dataAPI); 
				//var_dump($data['responData']);die;
				if(isset($data['responData']->data)){  
					if($data['responData']->data->status=="Gagal"){ 
						$respon = array(
							'message'=>"Pembelian gagal, silahkan ulangi. Error (".$data['responData']->data->message.")",
							'data'=>$data['responData']->data,
							"ref_id"=>md5($this->input->post('no_tujuan')."".$time ),
							'error'=>"0",
						);

					}elseif($data['responData']->data->status=="Pending"){   
						$respon = array(
							'message'=>"Pembelian pending, silahkan tunggu. Error (".$data['responData']->data->message.")",
							'data'=>$data['responData']->data,
							"ref_id"=>md5($this->input->post('no_tujuan')."".$time ),
							'error'=>"0",
						);
					}else{  
						$respon = array(
							'message'=>"Selamat Pembelian Sudah Berhasil",
							'data'=>$data['responData']->data,
							"ref_id"=>md5($this->input->post('no_tujuan')."".$time ),
							'error'=>"0",
						);
					}

					//potong saldo jika transaksi pending atau sukses
					if($data['responData']->data->status=="Pending" || $data['responData']->data->status=="Sukses"){
						$dataWallet = array(
							"user"=>$user->email,
							"action"=>'payment',
							"nominal"=>$priceUser,
							"ref_id"=>$refId
						);
						$bayar = $this->custom_library->paymentWallet($dataWallet);
						$this->postTrx($data['responData']->data,$priceUser );
						//if($bayar){
					}
					
				}else{
					$respon = array(
						'message'=>"Mohon Maaf, Pembelian gagal, silahkan ulangi (ada masalah koneksi ke server PPOB",
						'data'=>"",
						"ref_id"=>"",
						'error'=>"1",
					);
				} 
			/*}else{
				$respon = array(
					'message'=>"Mohon Maaf, Pembelian gagal, silahkan ulangi. (Ada permaslaahan dalam payment)",
					'data'=>"",
					"ref_id"=>"",
					'error'=>"1",
				);
			}*/
		}else{
			$respon = array(
					'message'=>"Mohon Maaf, Pembelian gagal, saldo anda tidak mencukupi (<strong>saldo Rp. ".number_format($saldo)."  </strong>), silahkan topup terlebih dahulu",
					'data'=>"",
					"ref_id"=>"",
					'error'=>"1",
				);
		}
		$this->session->set_flashdata('message',$respon['message']);
		redirect('Ppob/Dataitem/'.$category,'refresh');
	}
	 

	function LoadDataItem($dataAPI,$Category){ 
		$data['itemProduk'] = array();
		if($this->session->userdata('itemProduk')=="" || is_null($this->session->userdata('itemProduk'))){
			$data['responData'] = $this->custom_library->loadppobAPI('price-list',$dataAPI); 
			/*echo '<pre>';
			var_dump($data['responData']);die;*/
			if(isset($data['responData']->data)){ 
			
				foreach ($data['responData']->data as $key => $value) {
					 
					if($value->category==$Category){
						$data['itemProduk'][] = $value;
					}
				}
				
			}
		}else{ 
			$ItemProduk = $this->session->userdata('itemProduk'); 

			if(is_array($ItemProduk)){
				foreach ($this->session->userdata('itemProduk') as $key => $value) {
					 
					if($value->category==$Category){
						$data['itemProduk'][] = $value;
					}
				}
			}else{
				$data['itemProduk'] = array();
			}

		}

		usort($data['itemProduk'],function($first,$second){
		    return $first->price > $second->price;
		});
		/*echo '<pre>';
		 var_dump($data['itemProduk']);die;*/
		return $data['itemProduk'];
	}
	function cmp($a, $b) {
		return strcmp($a->price, $b->price);
	}
	function postTrx($data,$price_user){

		$dataTrx = array(
			'ref_id'=>$data->ref_id ,
			'user'=>$this->user->email,
			'customer_no'=>$data->customer_no ,
			'buyer_sku_code'=>$data->buyer_sku_code ,
			'message'=>$data->message ,
			'status'=>$data->status ,
			'rc'=>$data->rc ,
			'sn'=>$data->sn ,
			'buyer_last_saldo'=>$data->buyer_last_saldo ,
			'price'=>$data->price ,
			'price_user'=>$price_user ,
			'datetime'=>date("Y-m-d H:i:s")

		);
		//var_dump($dataTrx);die;
		if(isset($data->buyer_last_saldo) && $data->status!="Gagal"){
			if($data->buyer_last_saldo < (double)$this->config->item('min_saldo_warning')){

			}
		}
		$this->db->insert('tbl_trx',$dataTrx);
	}

	function chat(){
		$phone = str_replace("+","", $this->input->get('no'));
		$message = $this->input->get('message');
		// DO NOT EDIT BELOW
		$message = urlencode($message);
		$message = str_replace('+','%20',$message);
		$iphone = strpos($_SERVER['HTTP_USER_AGENT'],"iPhone");
		$android = strpos($_SERVER['HTTP_USER_AGENT'],"Android");
		$palmpre = strpos($_SERVER['HTTP_USER_AGENT'],"webOS");
		$berry = strpos($_SERVER['HTTP_USER_AGENT'],"BlackBerry");
		$ipod = strpos($_SERVER['HTTP_USER_AGENT'],"iPod");
		// check if is a mobile
		if ($iphone || $android || $palmpre || $ipod || $berry == true)
		{
		header('Location: whatsapp://send?phone='.$phone.'&text='.$message);
		//OR
		echo "<script>window.location='whatsapp://send?phone='.$phone.'&text='.$message</script>";
		}
		// all others
		else {
		header('Location: https://web.whatsapp.com/send?phone='.$phone.'&text='.$message);
		//OR
		echo "<script>window.location='https://web.whatsapp.com/send?phone='.$phone.'&text='.$message</script>";
		}
	}

	function PostInquiry(){
		$data = json_decode(base64_decode($this->input->get('data')));
		$sku = $data->sku;
		$cust = $data->cust;
		$refId  = $data->refId;
		$dataAPI = array(
		    "commands"=> "inq-pasca",
		    "username"=>$this->config->item('PPOB_USER'), 
		    "buyer_sku_code"=>$sku,
		    "customer_no"=> $cust,
		    "ref_id"=>$refId,
		    "sign"=>$this->custom_library->createSign($refId)
		);
		$responData = $this->custom_library->loadppobAPI('transaction',$dataAPI);
		echo "<pre>";
		var_dump($responData);die;
		 
		if(isset($responData->data)){
			if($responData->data->status=="Sukses"){
				$this->db->update('tbl_trx',array('status'=>"Sukses"),array("ref_id"=>$refId));
				redirect('ppob','refresh');
			}else{
				redirect('ppob');
			} 
		}else{
			redirect('ppob');
		}
	}

	

	
}