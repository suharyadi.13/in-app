<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library(array('form_validation','Custom_library'));
        $this->load->helper(['url', 'language','form']);
        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
        $this->lang->load('auth');
    }

    public function index()
    {
       
        $this->data['title']="REGISTER ".$this->config->item('company_name');
        
        $this->form_validation->set_rules('first_name', 'First name','trim|required');
        $this->form_validation->set_rules('last_name', 'Last name','trim|required');
        $this->form_validation->set_rules('ktp','Kartu Pengenal','trim|required|is_unique[users.username]',array(
                'required'      => 'Kolom %s Tidak Boleh Kosong.',
                'is_unique'     => '%s telah digunakan sebelumnya'
        ));
        /*$this->form_validation->set_rules('email','Email','trim|valid_email|required|is_unique[users.email]',array(
                'required'      => 'Kolom %s Tidak Boleh Kosong.',
                'is_unique'     => '%s telah digunakan sebelumnya'
        ));
        $this->form_validation->set_rules('no_telp','No Telp','trim|required|is_unique[tb_anggota.telp]',array(
                'required'      => 'Kolom %s Tidak Boleh Kosong.',
                'is_unique'     => '%s telah digunakan sebelumnya'
        ));*/
        $this->form_validation->set_rules('password','Password','trim|min_length[8]|max_length[20]|required');
        $this->form_validation->set_rules('confirm_password','Confirm password','trim|matches[password]|required');
        
        
        if($this->form_validation->run()===FALSE)
        { 
            
            $this->data['view'] = 'auth/register';
            $this->_render_page('layout_main', $this->data);
        }
        else
        {
            $first_name = $this->input->post('first_name');
            $last_name = $this->input->post('last_name');
            $username = $this->input->post('ktp');
            $email = $this->input->post('email');
            $password = $this->input->post('password');
            $kd_angg = $this->custom_library->getKodeAnggota();
            $ktp = $this->input->post('ktp');
            $no_telp = $this->input->post('no_telp');
            $additional_data = array(
                'first_name' => $first_name,
                'last_name' => $last_name,
                'kd_angg'=>$kd_angg,
                'active'=>'0',
            );

            $last_id = $this->ion_auth->register($username,$password,$email,$additional_data);
            if($last_id)
            {
                $this->session->set_flashdata('message','The account has been created. You may now login.');
               // $last_id = $this->db->insert_id();
                $this->db->insert('tb_anggota',array(
                    'kd_angg'=>$kd_angg, 
                    'nm_angg'=>$first_name, 
                    'sejak'=>date("d-m-Y"),
                    'ktp'=>$ktp, 
                    'telp'=>$no_telp, 
                ));
                 
                $this->session->set_flashdata('message','auth_message');
                redirect('auth');
            }
            else
            {
                $this->session->set_flashdata('message',$this->ion_auth->errors()); 
                
                redirect('register');
            }
        }
    }

    function outside(){
        //var_dump($this->session->all_userdata());die;
        $this->data['title']="ANDA YAKIN AKAN MELANJUTKAN ".$this->config->item('company_name')." ??";
        $this->data['username'] = $this->session->userdata('identity');
        $this->data['email'] = $this->session->userdata('email');
        $this->data['password'] = $this->session->userdata('passLogin');
        $this->form_validation->set_rules('password','Password','trim|matches[password]|required'); 
        if($this->form_validation->run()===FALSE)
        { 
            $this->data['view'] = 'auth/register_outside';
            $this->_render_page('layout_main', $this->data);
        }else{
            $username = $this->input->post('username');
            $password = $this->input->post('password');
            $email = $this->input->post('email');
            $last_id = $this->ion_auth->register($username,$password,$email);
            if($last_id)
            {
                $this->session->set_flashdata('message','The account has been created. You may now login.');
                //$this->ion_auth->login($username,$password ,false);
                redirect('ppob');
            }
            else
            {
                $this->session->set_flashdata('message',$this->ion_auth->errors()); 
                
                redirect('register');
            }
        }
        
    }
    public function _render_page($view, $data=null, $returnhtml=false)//I think this makes more sense
    {
        
        $this->viewdata = (empty($data)) ? $this->data: $data;

        $view_html = $this->load->view($view, $this->viewdata, $returnhtml);

        if ($returnhtml) return $view_html;//This will return html on 3rd argument being true
    }
}