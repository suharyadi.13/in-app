<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * Password hashing with PBKDF2.
 * Author: havoc AT defuse.ca
 * www: https://defuse.ca/php-pbkdf2.htm
 */

// These constants may be changed without breaking existing hashes.
define("PBKDF2_HASH_ALGORITHM", "sha256");
define("PBKDF2_ITERATIONS", 1000);
define("PBKDF2_SALT_BYTES", 24);
define("PBKDF2_HASH_BYTES", 24);

define("HASH_SECTIONS", 4);
define("HASH_ALGORITHM_INDEX", 0);
define("HASH_ITERATION_INDEX", 1);
define("HASH_SALT_INDEX", 2);
define("HASH_PBKDF2_INDEX", 3); 
/**
 * Format class
 * Help convert between various formats such as XML, JSON, CSV, etc.
 *
 * @author    Phil Sturgeon, Chris Kacerguis, @softwarespot
 * @license   http://www.dbad-license.org/
 */
class Custom_library {
	public $NamaBulan = array("Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
	public $ShortNamaBulan = array("Jan","Feb","Mar","Apr","Mei","Jun","Jul","Agus","Sept","Okt","Nov","Des");
    protected $_ci; 
function __construct(){
    $this->_ci = & get_instance();
}

function Filter_by_level($userlogin,$level){

		if($level == 1)
			$leveNew =  substr($userlogin,0,1);	
		else if($level == 2)
			$leveNew =  substr($userlogin,0,2);	
		else if($level == 3)
			$leveNew =  substr($userlogin,0,3);
		else if($level == 4)
			$leveNew =  substr($userlogin,0,4);
		else if($level == 5)
			$leveNew =  substr($userlogin,0,5);

		// var_dump($userlogin);
		// die();
		return $leveNew;
}

function GetJenisPelanggan(){
	$options = array(
				'FARMASI'=>'FARMASI',
				'COSMETIC'=>'COSMETIC',
				'F & B'=>'F & B',
				'INSTANSI'=>'INSTANSI',
				'TEXTILE'=>'TEXTILE',
				'KAPUR'=>'KAPUR',
				'LAB COFE'=>'LAB COFE',
				'LAB   '=>'LAB   ',
				'LAB'=>'LAB',
				'FIBER'=>'FIBER',
				'SUPPLIER'=>'SUPPLIER',

		);
	return $options;	
}
function GetStatus(){
	$options = array(
				'PROSPEK'         => 'PROSPEK',
				'DEAL'           => 'DEAL',
				'CANCEL'           => 'CANCEL'			
		);
	return $options;	
}

function GetJenisTransaksi(){
	$options = array(
				'Tender'       => 'Tender',
				'PL'           => 'PL',
				'Beli_Putus'   => 'Beli_Putus'
		);
	return $options;	
}	

function GetJenisOrder(){
	$options = array(
				'Bahan_Kimia'       => 'Bahan_Kimia',
				'Alat_Lab'           => 'Alat_Lab',
				'Lain-lain'           => 'Lain-lain',
		);
	return $options;	
}
function GetJenisArea(){
	$options = array(
				'BANDUNG'=>'BANDUNG',
				'NANJUNG'=>'NANJUNG',
				'CILAME'=>'CILAME',
				'GEDE BAGE'=>'GEDE BAGE',
				'PARAKAN SAAT'=>'PARAKAN SAAT',
				'CIWASTRA'=>'CIWASTRA',
				'PADALARANG'=>'PADALARANG',
				'CIHAMPELAS'=>'CIHAMPELAS',
				'JAKARTA'=>'JAKARTA',
				'SUMBER SARI'=>'SUMBER SARI',
				'BOGOR'=>'BOGOR',
				'CIKOPO, CIKAMPEK'=>'CIKOPO, CIKAMPEK',
				'INDOTASEI, CIKAMPEK'=>'INDOTASEI, CIKAMPEK',
				'SEMARANG'=>'SEMARANG',
				'MAJALENGKA'=>'MAJALENGKA',
				'RANCAEKEK'=>'RANCAEKEK',
				'LAMPUNG'=>'LAMPUNG',
				'CIJAMBE'=>'CIJAMBE',
				'CIMAHI'=>'CIMAHI',
				'PALASARI'=>'PALASARI',
				'SOEKARNO HATTA'=>'SOEKARNO HATTA',
				'CIPATAT'=>'CIPATAT',
				'PASTEUR'=>'PASTEUR',
				'BATUJAJAR'=>'BATUJAJAR',
		);
	return $options;	
}		
function cekCombo($val1,$val2){
	if($val1 == $val2)
		return "selected='selected' ";
	else
		return "";
}

function getNamaBulan($bln){
	if($bln >= 1 && $bln <= 12 ){
		return $this->NamaBulan[$bln-1];
	}else{
		return $bln;
	}


}



function encodeData($data)
{
    return $this->_ci->bcrypt(json_encode($data)); 
}

function decodeData($data, $key)
{
    $result = '';
        $strls = strlen($string);
        $strlk = strlen($key);
        for($i = 0; $i < $strls; $i++) {
            $char = substr($string, $i, 1);
            $keychar = substr($key, ($i % $strlk) - 1, 1);
            $char = $char."".$keychar;
            $result .= $char;
        }
    return $result;
}

public function loadDataAPI($API_SERVER,$API_USER,$API_PASS,$function,$Data){
	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => $API_SERVER."/".$function,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLOPT_POSTFIELDS =>json_encode($Data),
		CURLOPT_HTTPHEADER => array(
			"Content-Type: application/json",
			"Authorization: Digest username=\"".$API_USER."\", realm=\"Integral-API\", nonce=\"121\", uri=\"/index.php/Wallet/ceksaldo\", algorithm=\"MD5\", qop=auth, nc=00000001, cnonce=\"A558jpRd\", response=\"7554b3b9965a4c35609dc56144f2b817\", opaque=\"15441c938348c30e0fe28369c669543d\"",
	)));

	$response = curl_exec($curl);
	//var_dump($response);die;
	curl_close($curl); 
	return (array)json_decode($response);
} 
public function loadppobAPI($function,$Data){ 

	$curl = curl_init();

	curl_setopt_array($curl, array(
	  CURLOPT_URL => $this->_ci->config->item('PPOB_SERVER')."/".$function,
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 0,
	  CURLOPT_FOLLOWLOCATION => true,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "POST",
	  CURLOPT_POSTFIELDS =>json_encode($Data),
	  CURLOPT_HTTPHEADER => array(
	    "Content-Type: application/json"
	  ),
	));

	$response = curl_exec($curl); 
	curl_close($curl);

	return json_decode($response);
} 
function createSign($option){

	return md5($this->_ci->config->item('PPOB_USER')."".$this->_ci->config->item('PPOB_KEY')."".$option);
}

function cekSaldo($user){
		 
	$dataItem = array(
		"user"=>$user,
		"action"=>'cek_saldo'
	);

	$dataPost =array(
		"user"=>$this->_ci->config->item('user_wallet'),
		"data"=>$this->_ci->incrypt->encrypt($dataItem)
	);

	$Data = $this->loadDataAPI(
				$this->_ci->config->item('server_wallet'),
				'integral',
				'1nt3gr4l', 
				'ceksaldo',
				$dataPost
				);
	//var_dump($Data);die;
	if(isset($Data['response']) && $Data['response'] ==true){
		$saldo = $this->_ci->incrypt->decrypt($Data['data']);
		return $saldo['saldo'];
	}else{
		return 'load saldo gagal';
	}
	
}
function paymentWallet($dataItem){
		 
	

	$dataPost =array(
		"user"=>$this->_ci->config->item('user_wallet'),
		"data"=>$this->_ci->incrypt->encrypt($dataItem)
	);

	$Data = $this->loadDataAPI(
				$this->_ci->config->item('server_wallet'),
				'integral',
				'1nt3gr4l', 
				'payment',
				$dataPost
				);
	//var_dump($this->_ci->incrypt->decrypt($Data['data']));die;
	if(isset($Data['response']) && $Data['response'] ==true){ 
		return $this->_ci->incrypt->decrypt($Data['data']);
	}else{
		return false;
	}
	
}

}