<div class="row "> 
	<div class="col-12 col-xl-8 offset-xl-2 mt-6 mb-2">
		 
		<div class="card">
		<div class="card-header">
			<div class="row">  
				<div class="col-12  col-md-6  text-left "> 
		            <h4><?php echo $title; ?></h4>
		        </div>
				<div class="col-6 col-md-6  text-right "> 
					 <a href="<?php echo site_url('home')?>">
		             <i class="fa fa-home "></i>Home
		            </a> 
		        </div> 
			</div>
		</div>
		<div class="card-body">	 
			<div class="table-responsive">
					<table class="table display nowra" style="width:100%">
						<thead>
							<tr>
							<td>No</td>
							<td></td> 
							<td>User</td>
							<td>Saldo</td>
							<td>Tgl</td> 
							</tr>
						</thead> 
						<tbody>
						<?php 
						$x=1;
						if(is_array($data)){
							foreach ($data as $key => $value) { 
							 	echo '
								<tr>
								<td>'.$x.'</td>
								<td>'; 
								echo '<a href="'.site_url('home/delete_saldo/'.$value['id_saldo']).'" class="btn btn-success  btn-sm"><i class="fa fa-trash"></i></a>';
								
								echo '</td>
								<td>'.$value['id_user'].'</td>
								<td>'.number_format($value['saldo']).'</td>
								<td>'.$value['datetime'].'</td>';  
								echo '</tr>'; 
								$x++;
							}
						 
						}
						?>
						</tbody>
						
					</table>
				</div>
		</div>
		</div>
	</div>
</div>

