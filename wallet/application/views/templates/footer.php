 </div>

</main>

<footer class="text-muted">
  <div class="container">
    <p class="float-right">
      <a href="#">Back to top</a>
    </p>
    <p><a href="#">&copy <?php echo $this->config->item('company_name').' </a> -  <strong>@'.date("Y")."</strong>" ?></p>
  </div>
</footer>
<!-- <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script> -->
<script src="https://getbootstrap.com/docs/4.4/dist/js/bootstrap.bundle.min.js"></script>



<script type="text/javascript">
$(document).ready(function(){
	$(".table").DataTable({
        dom: 'Bfrtip',
        buttons: [
            'excel', 'pdf', 'print'
        ]
    });
})
function popUp($title,message){ 
	Swal({
		title: $title,
		text: message, 
		showCancelButton: true,
		confirmButtonColor: '#3085d6', 
		confirmButtonText: 'Ok'
	}).then((result) => {
		 if (result.value) {
		 	return true
		 }else{
		 	return false
		 }
	});
	
}
$(document).ready(function(){
	
})
</script> 
</body>
</html>        