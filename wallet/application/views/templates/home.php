<?php if ($this->ion_auth->is_admin()){ ?>
<div class="row "> 
	<div class="col-12 col-xl-10 offset-xl-1 mt-6 mb-2"> 
		<a href="<?php echo $this->config->item('parent_module') ?>" class="btn btn-dark text-white"><i class="fa fa-home"></i> Kembali</a>
		<a href="<?php echo site_url('home/saldo_user') ?>" class="btn btn-success text-white"><i class="fa fa-money"></i> Saldo User</a>
		 
		<div class="card  bg-primary text-white">
		<div class="card-header">
			<div class="row">  
				<div class="col-12  col-md-6  text-left "> 
		            <h4><?php echo $title; ?></h4> 
		        </div>
				<div class="col-6 col-md-6  text-right "> 
					 <a href="<?php echo $this->config->item('parent_module') ?>">
		             <i class="fa fa-home "></i>Home
		            </a> 
		        </div> 
			</div>
		</div>
		<div class="card-body bg-white text-dark">	 
			<div class="table-responsive">
					<table class="table display nowrap " style="width:100%">
						<thead>
							<tr>
							<td>No</td>
							<td></td> 
							<td>Ref ID</td>
							<td>Aksi</td>
							<td>Nominal</td> 
							<td>User</td>  
							<td>Tanggal</td> 
							</tr>
						</thead> 
						<tbody>
						<?php 
						$x=1;
						if(is_array($data)){
							foreach ($data as $key => $value) { 
							 	echo '
								<tr>
								<td>'.$x.'</td>
								<td>';
								if($value['status']==1){
									echo '<a href="'.site_url('home/proses_topup/'.$value['id_topup']).'" class="btn btn-success  btn-sm"><i class="fa fa-check"></i></a>';
								}
								echo '</td>
								<td><small>'.$value['ref_id'].'</small></td>
								<td>'.$value['jenis_transaksi'].'</td>
								<td>'.number_format($value['nominal']).'</td>
								<td>'.$value['user'].'</td> 
								<td><small>'.$value['datetime'].'</small></td> ';
								
								echo '</tr>'; 
								$x++;
							}
						 
						}
						?>
						</tbody>
						
					</table>
				</div>
		</div>
		</div>
	</div>
</div>

<?php } ?>