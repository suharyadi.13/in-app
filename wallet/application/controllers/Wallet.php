<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

/**
 * Keys Controller
 * This is a basic Key Management REST controller to make and delete keys
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Wallet extends REST_Controller {

    protected $methods = [
            'index_put' => ['level' => 10, 'limit' => 10],
            'index_delete' => ['level' => 10],
            'level_post' => ['level' => 10],
            'regenerate_post' => ['level' => 10],
        ];
    private $klienID ="001203";
    private $secret = "8ccsswswcg8gg4oo88sw0ccgs0w8ows008og0oco"; 
    private $token = '$2a$08$RhnY4OennrqswNljoJJ.4ei/AS3TW1OldoXbGUaB3oFnSidveks6S';

    
    function __construct(){
        // Construct the parent class
        parent::__construct();
        //load the models 
        $this->load->model('custom_model');
        $this->load->library(array('session','curl','Custom_library','Incrypt','Bcrypt','ion_auth'));  
        $this->output->set_header('Access-Control-Allow-Origin: *');
    }
    public function ceksaldo_post()
    {
        
        $validate = $this->_validateAkses();
        //var_dump($validate);die;
        if($validate['response']==TRUE){
            $data = $this->db->select('*')->from('tbl_saldo')->where('id_user',$validate['data']['user'])
                ->get()->row_array();
            if(is_array($data) && count($data)>0){
                $this->response([
                'response' => TRUE,
                'message'=>"Load Saldo berhasil",
                'data' => $this->incrypt->encrypt($data),
                ], REST_Controller::HTTP_CREATED);
            }else{
                $this->response([
                'response' => TRUE,
                'message'=>"Data saldo masih kosong",
                'data' =>  $this->incrypt->encrypt(array('saldo'=>0))
                ], REST_Controller::HTTP_CREATED);
            }
            
        }else{
            $this->response(array(
                'response' => FALSE,
                'message'=>"Akses tidak dikenali",
                'data' =>"" 
                ), REST_Controller::HTTP_FORBIDDEN); 
        }
    }
    public function topup_post()
    {
        $validate = $this->_validateAkses();
        //var_dump($validate);die;
        if($validate['response']==TRUE){
            if(isset($validate['data']['nominal']) && $validate['data']['nominal'] >0){
                $dataMutasi = array(
                    'user'=>$validate['data']['user'],
                    'nominal'=>$validate['data']['nominal'],
                    'ref_id'=>$validate['data']['ref_id'],
                    'jenis_transaksi'=>$validate['data']['action'],
                    'status'=>'1', 
                    'datetime'=>date("Y-m-d H:i:s"),
                );
                $this->db->insert('tbl_mutasi',$dataMutasi);
                 $this->response([
                'response' => TRUE,
                'message'=>"Pengajuan Topup berhasil",
                'data' => $this->incrypt->encrypt($dataMutasi)
                ], REST_Controller::HTTP_CREATED); 
            }else{
                $this->response([
                    'response' => FALSE,
                    'message'=>"Nominal tidak boleh 0",
                    'data' => "",
                    ], REST_Controller::HTTP_CREATED); 
            }
        }else{
            $this->response([
                'response' => FALSE,
                'message'=>$validate['message'],
                'data' => ""
                ], REST_Controller::HTTP_FORBIDDEN);
        }

        
    }
    public function payment_post()
    {
        $validate = $this->_validateAkses();

        if($validate['response']==TRUE){
            $data = $this->db->select('id_user,saldo')
            ->from('tbl_saldo')->where('id_user',$validate['data']['user'])
            ->get()->row_array();
            $saldo = (isset($data['saldo'])?$data['saldo']:0);
            //validasi saldo 
            if(is_array($data) && count($data)>0 && $saldo >= $validate['data']['nominal']){
                $dataSaldo =  $this->db->query("update tbl_saldo set saldo=(saldo-".$validate['data']['nominal'].") WHERE id_user ='".$validate['data']['user']."' ");
                if($dataSaldo){
                    //masukan kedalam data mutasi
                    $dataMutasi = array(
                        'user'=>$validate['data']['user'],
                        'nominal'=>$validate['data']['nominal'],
                        'ref_id'=>$validate['data']['ref_id'],
                        'jenis_transaksi'=>$validate['data']['action'],
                        'status'=>'2', 
                        'datetime'=>date("Y-m-d H:i:s"),
                    );
                    $this->db->insert('tbl_mutasi',$dataMutasi);
                      

                    $this->response([
                    'response' => TRUE,
                    'message'=>"pembelian berhasil",
                    'data' => $this->incrypt->encrypt($data),
                    ], REST_Controller::HTTP_CREATED);
                }else{
                    $this->response([
                    'response' => FALSE,
                    'message'=>"Ada permasalahan pada proses payment, silahkan ulangi kembali ",
                    'data' => ""
                    ], REST_Controller::HTTP_CREATED); 
                }
            }else{
                $this->response([
                    'response' => FALSE,
                    'message'=>"Saldo anda tidak mencukupi",
                    'data' => ""
                    ], REST_Controller::HTTP_CREATED); 
            }
        }

        
    }
    public function transfer_post()
    {
        $this->_validateAkses();
        $data['message']="";


        $this->response([
            'response' => FALSE,
            'message'=>"",
            'data' => $data
            ], REST_Controller::HTTP_CREATED); 
    }

    public function mutasi_post(){
        $validate = $this->_validateAkses();
        //var_dump($validate);die;
        if($validate['response']==TRUE){
            if(isset($validate['data']['action']) && $validate['data']['action'] =="mutasi"){
                $data = $this->db->select('*')->from('tbl_mutasi')
                        ->where('user',$validate['data']['user'])->limit(100,0)
                        ->order_by('datetime','DESC')->get()
                        ->result_array();
                $dataMutasi = array(
                    'user'=>$validate['data']['user'],  
                    'jenis_transaksi'=>$validate['data']['action'], 
                    'data'=> $data,
                    'action'=> $validate['data']['action'],  
                    'datetime'=>date("Y-m-d H:i:s"),
                );
                
                 $this->response([
                'response' => TRUE,
                'message'=>"Load Mutasi Sukses",
                'data' => $this->incrypt->encrypt($dataMutasi)
                ], REST_Controller::HTTP_CREATED); 
            }else{
                $this->response([
                    'response' => FALSE,
                    'message'=>"Aksi tidak dikenali",
                    'data' => "",
                    ], REST_Controller::HTTP_CREATED); 
            }
        }else{
            $this->response([
                'response' => FALSE,
                'message'=>$validate['message'],
                'data' => ""
                ], REST_Controller::HTTP_FORBIDDEN);
        }
    }
    function inputPayment(){
        $dataMutasi = array(
            'user'=>$validate['data']['user'],
            'nominal'=>$validate['data']['nominal'],
            'ref_id'=>$validate['data']['ref_id'],
            'jenis_transaksi'=>$validate['data']['action'],
            'status'=>'1', 
            'datetime'=>date("Y-m-d H:i:s"),
        );
        $this->db->insert('tbl_mutasi',$dataMutasi);
         $this->response([
        'response' => TRUE,
        'message'=>"Pengajuan Topup berhasil",
        'data' => $this->incrypt->encrypt($dataMutasi)
        ], REST_Controller::HTTP_CREATED); 
    }
    function _validateAkses(){   

        $respon = array(
                    "response"=>FALSE,
                    "message"=>"");
        //$param = json_decode(base64_encode($this->post('param')));
        $this->custom_model->table = "tbl_akses";
        $condition = array('email'=>$this->post('user'),
                            'ip_registered'=>$_SERVER['REMOTE_ADDR'],
                            "status"=>"1") ;
        $user = $this->custom_model->get_all($condition);
        if($user->num_rows()>0){
            $user = $user->row_array();

            $this->incrypt->cid = $user['token'];
            $this->incrypt->secret =$user['key']; 
            $dataDecrypt = $this->incrypt->decrypt($this->post('data')); 
            //var_dump($dataDecrypt);
            $data = array(
                'ip_registered'=>$_SERVER['REMOTE_ADDR'], 
                'action'=>$dataDecrypt['action'],
                'user'=>$this->post('user'),
            );
           // var_dump($data);
            if($data['ip_registered']=="" || $data['action']=="" || $data['user']==""){
                $this->response([
                    'response' => FALSE,
                    'message'=>"IP / Aksi tidak dikenali, IP anda ( ".$_SERVER['REMOTE_ADDR']." )",
                    'data' =>  $this->incrypt->encrypt($data)
                    ], REST_Controller::HTTP_FORBIDDEN); 

            }else{ 
                $respon = array(
                    "response"=>TRUE,
                    "message"=>"validated",
                    "user"=>$this->post('user'), //user akses api
                    "token"=>$user['token'], //token akses api
                    "secret"=>$user['key'], //key akses api
                    "data" => $dataDecrypt); ////data post to api

                return $respon;
            }
        }else{
            $this->response([
            'response' => FALSE,
            'message'=>"Akses anda ditolak",
            'ip_anda'=>$_SERVER['REMOTE_ADDR'],
            'data' => "",
            ], REST_Controller::HTTP_FORBIDDEN); 
        }
        
        return $respon;

    }

    function requestToken_post(){
       /* $reqToken = array("token"=>"token2020","user"=>"admin@admin.com","secret"=>"123456789");
        
        var_dump(base64_encode(json_encode($reqToken)));die;*/
        
        $this->custom_model->table = 'tbl_akses';
        $condition = array(
            "ip_registered"=>$_SERVER['REMOTE_ADDR'],
            "email"=>$this->post('user'), 
            "status"=>"1",
        );
         
        
        $cek = $this->custom_model->get_all($condition);

        if($cek->num_rows()>0){
            $dataPost =(array) json_decode(base64_decode($this->post('data')));
            //var_dump($dataPost);die;
            if(isset($dataPost['secret'])){
                $token = $this->getToken($dataPost['token']);
                $data = array(
                    'token' => $token,
                    'ip_address'=>$_SERVER['REMOTE_ADDR']
                );
                $this->response([
                'response' => TRUE,
                'message'=>"",
                'data' => $data
                ], REST_Controller::HTTP_CREATED); 
            }else{
                $this->response([
                    'response' => FALSE,
                    'message'=>"mohon masukan secret code anda",
                    'data' => $dataPost
                    ], REST_Controller::HTTP_FORBIDDEN); 
            }
        }else{
                $this->response([
                    'response' => FALSE,
                    'message'=>"Akses sever anda ditolak",
                    'data' => ""    
                    ], REST_Controller::HTTP_FORBIDDEN); 
            }
    }
    function getToken($token){ 
       return  $this->bcrypt->hash_password($token);
    }


    function _decodeData($data,$klienID="",$secret=""){ 
        $decoded = $this->incrypt->decrypt($data,$this->klienID,$this->secret); 
         
        return $decoded;
    }
    function _encodeData($data,$klienID="",$secret=""){ 
        return $this->incrypt->encrypt($data,$this->klienID,$this->secret); 
    }


}
