<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library(array('form_validation','session'));
		$this->load->library(array('custom_library','Incrypt'));
	    if (!$this->ion_auth->logged_in()) {
            redirect('auth');
        }
        $this->user = $this->ion_auth->user()->row();
    }
    function loadMandatoryData($data=""){
        $user = $this->user;
        $data = [
            'user'      => $user,
            'judul'     => 'Presensi',
            'subjudul'  => 'Data Aplikasi',
            'parent'    => $this->config->item('parent_module'),
        ];
        $data['title'] = "INTEGRAL SMART SCHOOL";

        return $data;
    }


    function index(){
        $user = $this->user;
        $data = [
            'user'      => $user,
            'judul'     => 'Dashboard',
            'subjudul'  => 'Data Aplikasi',
            'parent'    => $this->config->item('parent_module'),
            'logData' =>array($this->session->userdata('userLogin'),$this->session->userdata('passLogin')),
        ];
        $enCryptData  = $this->incrypt->encrypt($data);
        $data['title'] = "Data History Topup"; 
        $data['sendData'] = urlencode(base64_encode($enCryptData)); 
        $data['modul'] =  $this->config->item('module');
        $data['data'] = $this->db->order_by('datetime','DESC')->get('tbl_mutasi')->result_array();
        
        //var_dump($data['mutasi']);die;
        $data['page'] = "home";
        $this->load->view('templates/index',$data);
    }

    function proses_topup($id){
       $cek = $this->db->where(array('id_topup'=>$id,'status'=>1))->get('tbl_mutasi');
       if ($cek->num_rows() <=0) {
            $this->session->set_flashdata("message","Data topup tidak ditemukan");
            //die(validation_errors());
            $this->index();
        }else{
            $dataMutasi= $cek->row_array();
            $ceksaldo = $this->db->select('*')->from('tbl_saldo')->where('id_user',$dataMutasi['user'])->get();
            if( $ceksaldo->num_rows()>0){
                $dataSaldo =  $this->db->query("update tbl_saldo set saldo=(saldo+".$dataMutasi['nominal'].") WHERE id_user ='".$dataMutasi['user']."' ");
                if($dataSaldo){
                    $this->db->update('tbl_mutasi',array('status'=>2),array('id_topup'=>$id));

                    $this->session->set_flashdata("message","Data topup berhasil ditambahkan");
                    redirect('home');
                }else{
                    $this->session->set_flashdata('message','Proses topup gagal '.$Data['message']);
                    $this->index();
                }
            }else{
                $insert = $this->db->insert('tbl_saldo',array(
                    'id_user'=>$dataMutasi['user'],
                    'saldo'=>$dataMutasi['nominal'],
                    'datetime'=>date('Y-m-d H:i:s'),
                ));
                $this->db->update('tbl_mutasi',array('status'=>2),array('id_topup'=>$id));
                if($insert){
                    $this->session->set_flashdata("message","Data topup berhasil ditambahkan");
                    redirect('home');
                }else{
                    $this->session->set_flashdata('message','Proses topup gagal '.$Data['message']);
                    $this->index();
                }
            }
            
        }
    }
    function saldo_user(){
        $user = $this->user;
        $data = [
            'user'      => $user,
            'judul'     => 'Dashboard',
            'subjudul'  => 'Data Aplikasi',
            'parent'    => $this->config->item('parent_module'),
            'logData' =>array($this->session->userdata('userLogin'),$this->session->userdata('passLogin')),
        ];
        $enCryptData  = $this->incrypt->encrypt($data);
        $data['sendData'] = urlencode(base64_encode($enCryptData)); 
        $data['modul'] =  $this->config->item('module');
        $data['title'] = "Saldo User ";   
        $data['data'] = $this->db->get('tbl_saldo')->result_array();
        //var_dump($data['mutasi']);die;
        $data['page'] = "saldo";
        $this->load->view('templates/index',$data);
    }
    function load_mutasi(){
         
            $dataItem = array(
                "user"=>$this->config->item('user_wallet'),  
                "action"=>'mutasi'
            );

            $dataPost =array(
                "user"=>$this->config->item('user_wallet'),
                "data"=>$this->incrypt->encrypt($dataItem)
            );

            $Data = $this->custom_library->loadDataAPI(
                        $this->config->item('server_wallet'),
                        'integral',
                        '1nt3gr4l', 
                        'mutasi',
                        $dataPost
                        );
            //var_dump($Data);die;
            if(isset($Data['response']) && $Data['response'] ==TRUE){
                $mutasi = $this->incrypt->decrypt($Data['data']);
                return $mutasi['data'];
            }else{

                return false;
            }
             
    }


    function chat(){
        $phone = str_replace("+","", $this->input->get('no'));
        $message = $this->input->get('message');
        // DO NOT EDIT BELOW
        $message = urlencode($message);
        $message = str_replace('+','%20',$message);
        $iphone = strpos($_SERVER['HTTP_USER_AGENT'],"iPhone");
        $android = strpos($_SERVER['HTTP_USER_AGENT'],"Android");
        $palmpre = strpos($_SERVER['HTTP_USER_AGENT'],"webOS");
        $berry = strpos($_SERVER['HTTP_USER_AGENT'],"BlackBerry");
        $ipod = strpos($_SERVER['HTTP_USER_AGENT'],"iPod");
        // check if is a mobile
        if ($iphone || $android || $palmpre || $ipod || $berry == true)
        {
        header('Location: whatsapp://send?phone='.$phone.'&text='.$message);
        //OR
        echo "<script>window.location='whatsapp://send?phone='.$phone.'&text='.$message</script>";
        }
        // all others
        else {
        header('Location: https://web.whatsapp.com/send?phone='.$phone.'&text='.$message);
        //OR
        echo "<script>window.location='https://web.whatsapp.com/send?phone='.$phone.'&text='.$message</script>";
        }
    }

         

}

/* End of file Wallet.php */
/* Location: ./application/controllers/Wallet.php */
/* Please DO NOT modify this information : */ 