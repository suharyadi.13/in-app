<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * Password hashing with PBKDF2.
 * Author: havoc AT defuse.ca
 * www: https://defuse.ca/php-pbkdf2.htm
 */

// These constants may be changed without breaking existing hashes.
define("PBKDF2_HASH_ALGORITHM", "sha256");
define("PBKDF2_ITERATIONS", 1000);
define("PBKDF2_SALT_BYTES", 24);
define("PBKDF2_HASH_BYTES", 24);

define("HASH_SECTIONS", 4);
define("HASH_ALGORITHM_INDEX", 0);
define("HASH_ITERATION_INDEX", 1);
define("HASH_SALT_INDEX", 2);
define("HASH_PBKDF2_INDEX", 3); 
/**
 * Format class
 * Help convert between various formats such as XML, JSON, CSV, etc.
 *
 * @author    Phil Sturgeon, Chris Kacerguis, @softwarespot
 * @license   http://www.dbad-license.org/
 */
class Custom_library {
	public $NamaBulan = array("Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
	public $ShortNamaBulan = array("Jan","Feb","Mar","Apr","Mei","Jun","Jul","Agus","Sept","Okt","Nov","Des");
    protected $_ci; 
function __construct(){
    $this->_ci = & get_instance();
}

function Filter_by_level($userlogin,$level){

		if($level == 1)
			$leveNew =  substr($userlogin,0,1);	
		else if($level == 2)
			$leveNew =  substr($userlogin,0,2);	
		else if($level == 3)
			$leveNew =  substr($userlogin,0,3);
		else if($level == 4)
			$leveNew =  substr($userlogin,0,4);
		else if($level == 5)
			$leveNew =  substr($userlogin,0,5);

		// var_dump($userlogin);
		// die();
		return $leveNew;
}

function GetJenisPelanggan(){
	$options = array(
				'FARMASI'=>'FARMASI',
				'COSMETIC'=>'COSMETIC',
				'F & B'=>'F & B',
				'INSTANSI'=>'INSTANSI',
				'TEXTILE'=>'TEXTILE',
				'KAPUR'=>'KAPUR',
				'LAB COFE'=>'LAB COFE',
				'LAB   '=>'LAB   ',
				'LAB'=>'LAB',
				'FIBER'=>'FIBER',
				'SUPPLIER'=>'SUPPLIER',

		);
	return $options;	
}
function GetStatus(){
	$options = array(
				'PROSPEK'         => 'PROSPEK',
				'DEAL'           => 'DEAL',
				'CANCEL'           => 'CANCEL'			
		);
	return $options;	
}

function GetJenisTransaksi(){
	$options = array(
				'Tender'       => 'Tender',
				'PL'           => 'PL',
				'Beli_Putus'   => 'Beli_Putus'
		);
	return $options;	
}	

function GetJenisOrder(){
	$options = array(
				'Bahan_Kimia'       => 'Bahan_Kimia',
				'Alat_Lab'           => 'Alat_Lab',
				'Lain-lain'           => 'Lain-lain',
		);
	return $options;	
}
function GetJenisArea(){
	$options = array(
				'BANDUNG'=>'BANDUNG',
				'NANJUNG'=>'NANJUNG',
				'CILAME'=>'CILAME',
				'GEDE BAGE'=>'GEDE BAGE',
				'PARAKAN SAAT'=>'PARAKAN SAAT',
				'CIWASTRA'=>'CIWASTRA',
				'PADALARANG'=>'PADALARANG',
				'CIHAMPELAS'=>'CIHAMPELAS',
				'JAKARTA'=>'JAKARTA',
				'SUMBER SARI'=>'SUMBER SARI',
				'BOGOR'=>'BOGOR',
				'CIKOPO, CIKAMPEK'=>'CIKOPO, CIKAMPEK',
				'INDOTASEI, CIKAMPEK'=>'INDOTASEI, CIKAMPEK',
				'SEMARANG'=>'SEMARANG',
				'MAJALENGKA'=>'MAJALENGKA',
				'RANCAEKEK'=>'RANCAEKEK',
				'LAMPUNG'=>'LAMPUNG',
				'CIJAMBE'=>'CIJAMBE',
				'CIMAHI'=>'CIMAHI',
				'PALASARI'=>'PALASARI',
				'SOEKARNO HATTA'=>'SOEKARNO HATTA',
				'CIPATAT'=>'CIPATAT',
				'PASTEUR'=>'PASTEUR',
				'BATUJAJAR'=>'BATUJAJAR',
		);
	return $options;	
}		
function cekCombo($val1,$val2){
	if($val1 == $val2)
		return "selected='selected' ";
	else
		return "";
}

function getNamaBulan($bln){
	if($bln >= 1 && $bln <= 12 ){
		return $this->NamaBulan[$bln-1];
	}else{
		return $bln;
	}


}



function encodeData($data)
{
    return $this->_ci->bcrypt(json_encode($data)); 
}

function decodeData($data, $key)
{
    $result = '';
        $strls = strlen($string);
        $strlk = strlen($key);
        for($i = 0; $i < $strls; $i++) {
            $char = substr($string, $i, 1);
            $keychar = substr($key, ($i % $strlk) - 1, 1);
            $char = $char."".$keychar;
            $result .= $char;
        }
    return $result;
}


/* Helper Methods */

    public function _generate_key()
    {
        do
        {
            // Generate a random salt
            $salt = base_convert(bin2hex($this->_ci->security->get_random_bytes(64)), 16, 36);

            // If an error occurred, then fall back to the previous method
            if ($salt === FALSE)
            {
                $salt = hash('sha256', time() . mt_rand());
            }

            $new_key = substr($salt, 0, config_item('rest_key_length'));
        }
        while ($this->_key_exists($new_key));

        return $new_key;
    }

    /* Private Data Methods */

    private function _get_key($key)
    {
        return $this->_ci->db
            ->where(config_item('rest_key_column'), $key)
            ->get(config_item('rest_keys_table'))
            ->row();
    }

    private function _key_exists($key)
    {
        return $this->_ci->db
            ->where(config_item('rest_key_column'), $key)
            ->count_all_results(config_item('rest_keys_table')) > 0;
    }

    private function _insert_key($key, $data)
    {
        $data[config_item('rest_key_column')] = $key;
        $data['date_created'] = function_exists('now') ? now() : time();

        return $this->_ci->db
            ->set($data)
            ->insert(config_item('rest_keys_table'));
    }

    private function _update_key($key, $data)
    {
        return $this->_ci->db
            ->where(config_item('rest_key_column'), $key)
            ->update(config_item('rest_keys_table'), $data);
    }

    private function _delete_key($key)
    {
        return $this->_ci->db
            ->where(config_item('rest_key_column'), $key)
            ->delete(config_item('rest_keys_table'));
    }



}